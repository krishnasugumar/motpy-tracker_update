import numpy
import pickle
from sklearn.preprocessing import LabelEncoder
from sklearn.pipeline import Pipeline
from keras.utils import np_utils


def preprocess_labels(labels, encoder=None, categorical=True, labelfile = "classes.npy"):
    if not encoder:
        encoder = LabelEncoder()
        encoder.fit(labels)
    y = encoder.transform(labels).astype(numpy.int32)
    numpy.save(labelfile, encoder.classes_)
    if categorical:
        y = np_utils.to_categorical(y)
    return y, encoder
    
def postprocess_labels(labelfile = "classes.npy"):
    encoder = LabelEncoder()
    encoder.classes_ = numpy.load(labelfile)
    # output = encoder.inverse_transform([numpy.argmax(dummy_y[0])])
    return encoder


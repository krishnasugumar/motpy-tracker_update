import os
import shutil
import time 
from glob import glob


def create_folder(path):
    if not os.path.isdir(path):
        os.makedirs(path)
    return path


def check_folder(path):
    if not os.path.exists(path):
        raise ValueError("images_dir_path:%s doesn't exist"%path)
    return path

def save_image_clusters(input_path_list, out_put_folder):
    for k, img_path  in enumerate(input_path_list):
        new_path = os.path.join(out_put_folder, "Face_" + str(k)+"_"+ time.strftime("%d%b%Y_%H%M%S", time.localtime())+'.jpg')
        shutil.copy(img_path, new_path)

def check_file(path):
    if not os.path.isfile(path):
        raise ValueError("Specified file: %s doesn't exist"%path)
    return path

def get_latest_pickle(path):
    list_of_files = glob(os.path.join(path, "*.pickle"))  
    latest_file = max(list_of_files, key=os.path.getctime)
    print(latest_file)
    return latest_file
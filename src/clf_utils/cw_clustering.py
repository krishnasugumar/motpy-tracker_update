"""
Owner : Shaik Ahmad. Recode Solurtions
Purpose: Face clustering algortihms
Created Date: 05-12-2019: 11:56:00
Update1: chinese wishpers algorithm added
Uodate2: db scan clustering algorithm added
Update3: do_kmens, EmbedClustering class object is added
Update4: Single function created to select clusters between db-scan and chinese whishpers

""" 

import dlib 
from sklearn.cluster import DBSCAN, KMeans, AffinityPropagation, AgglomerativeClustering, Birch, MeanShift
import numpy as np
from sklearn.metrics import silhouette_samples, silhouette_score

def do_clustering(embeddings, uniqueids, cluster_threshold = 0.30, cl_type="cw"):
    """
    Performs Chainese Whispers or DB scan Clustering algorithm based clustering
    Params  Input:   embeddings: a list 128 float64 numbers
                     uniqueids : reference paths
                     cluster_threshold: 0.30
                     cl_type = cw or db
            Out  :   clusterids
                     uniqueids
    """
    if cl_type == "db":
        clusterids, uniqueids = do_dbscan(embeddings, uniqueids, eps=cluster_threshold)
    else:
        clusterids, uniqueids = do_cwcluster(embeddings, uniqueids, cluster_threshold = cluster_threshold)

    return (clusterids, uniqueids)

def do_cwcluster(embeddings, uniqueids, cluster_threshold = 0.30):
    """
    Performs Chainese Whispers algorithm based clustering
    Params  Input:   embeddings: a list 128 float64 numbers
                     uniqueids : reference paths
            Out  :   clusterids
                     uniqueids
    """
    embeddings = [dlib.vector(embed) for embed in embeddings]
    clusterids = dlib.chinese_whispers_clustering(embeddings, cluster_threshold)
    return (clusterids, uniqueids)


def do_dbscan(embeddings, uniqueids, eps=0.28):
    clt = DBSCAN(metric="euclidean", eps=eps)
    clt.fit(embeddings)
    labelIDs = np.unique(clt.labels_)
    labels = list(clt.labels_)
    for i, j in enumerate(labels): 
        if j == -1:
            labels[i] = 5000+i 
    return (labels, uniqueids)


def do_kmens(embeddings, uniqueids):
    range_n_clusters = list(range(100, 200))
    for n_clusters in range_n_clusters:
        clusterer = KMeans(n_clusters=n_clusters, random_state=10)
        cluster_labels = clusterer.fit_predict(X)
    pass


class EmbedClustering(object):
    def __init__(self, cluster_threshold=0.30):
        self.cluster_threshold = cluster_threshold


############# Supporting Modules ####################    
    def outlier_processing(self, labels_, unique_numer = 5000, ):
        labelIDs = np.unique(labels_)
        labels = list(labels_)
        for i, j in enumerate(labels): 
            if j == -1:
                labels[i] = 5000+i
        return labels
    
    @staticmethod
    def silhouette_score(embeddings, cw_ids):
        return silhouette_score(embeddings, cw_ids)

    def get_cluster_number(self, X, estimated_cluster_number, search_range=7):
        range_n_clusters = list(range(estimated_cluster_number-search_range, estimated_cluster_number+search_range))
        silhouette_avg_lst = []
        for n_clusters in range_n_clusters:
            clusterer = KMeans(n_clusters=n_clusters, random_state=10)
            cluster_labels = clusterer.fit_predict(X)
            silhouette_avg = silhouette_score(X, cluster_labels)
            silhouette_avg_lst.append(silhouette_avg)
        good_cluster_number = range_n_clusters(np.argmax(silhouette_avg_lst))
        return good_cluster_number


############ Clustering Techniques ########################    
    def dbscan(self, embeddings, uniqueids):
        """
        DBscans takes inpputs and return cluster numbers. Cluster numbers above 5000 means outliers.
        """
        clt = DBSCAN(metric="euclidean", eps=self.cluster_threshold, min_samples=3)
        clt.fit(embeddings)
        labels = self.outlier_processing(clt.labels_) 
        return (labels, uniqueids)


    def KMeans(self, embeddings, uniqueids, estimated_samples_percluster=60, do_search=True):
        estimated_cluster_number = round(len(embeddings)/estimated_samples_percluster)
        if do_search:
            estimated_cluster_number = self.get_cluster_number(embeddings, estimated_cluster_number)
        clusterer = KMeans(n_clusters=estimated_cluster_number, random_state=10)
        cluster_labels = clusterer.fit_predict(embeddings)
        return (cluster_labels, uniqueids)
    
    def AffinityPropagation(self, embeddings, uniqueids):
        """Execution is speed is very slow"""
        aff_clusters = AffinityPropagation(damping=0.5).fit(embeddings)
        labels = aff_clusters.labels_
        return (labels, uniqueids)
    
    def optics(self, embeddings, uniqueids):
        opt_clusters = OPTICS(min_samples=3).fit(embeddings)
        labels = opt_clusters.labels_
        return (labels, uniqueids)

    def AgglomerativeClustering(self, embeddinds, uniqueids):
        agg = AgglomerativeClustering(n_clusters=None, distance_threshold=self.cluster_threshold).fit(embeddinds)
        labels = agg.labels_
        return (labels, uniqueids)

    def birch(self, embeddinds, uniqueids):
        birch = Birch(threshold=self.cluster_threshold).fit(embeddinds)
        labels = birch.labels_
        return (labels, uniqueids) 

    def meanshift(self, embeddinds, uniqueids):
        meanshift = MeanShift().fit(embeddinds)
        labels = meanshift.labels_
        return (labels, uniqueids) 


    
    
    

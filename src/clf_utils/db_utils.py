import base64
import datetime
import io
import os
import math
import time

import cv2
import numpy as np
from imutils import build_montages
from PIL import Image
import matplotlib.pyplot as plt
from collections import Counter

from clf_utils.save_load_data import load_json
from clf_utils.dataset_utils import get_numeric_embed
from clf_utils.folder_utils import create_folder

class DbUtils(object):
    def __init__(self, obj):
        self.obj = obj
        self.update_embed()
        # self.embedds = [item['embeded'] for item in self.obj]
        # self.track_ids = [item['trackdata_id'] for item in self.obj]
        self.update_ref_track_no()
        # self.track_ids = [item['ref_track_no'] for item in self.obj]
        # self.camids = [item['camera_id'] for item in self.obj]
        

    @staticmethod
    def stringToRGB(base64_string):
        try:
            imgdata = base64.b64decode(str(base64_string))
            image = Image.open(io.BytesIO(imgdata))
            return cv2.cvtColor(np.array(image), cv2.COLOR_BGR2RGB)
        except Exception as e:
            return cv2.cvtColor(np.array(image),cv2.COLOR_GRAY2RGB)
    # @staticmethod
    def remove_slected_keys(self, key_list=["ref_image"]):
        """removes selected keys from the dictionay and returns the remaining"""
        # obj = [{k: v for k, v in d.items() if k not in  ['ref_image', 'cameraId', 'clusterId', 'confidence']} for d in obj]
        self.obj = [{k: v for k, v in d.items() if k not in  key_list} for d in self.obj]
        # return obj

    def get_slected_keys(self, key_list=["ref_image"]):
        """keeps a selected keys from the dictionay """
        # obj = [{k: v for k, v in d.items() if k not in  ['ref_image', 'cameraId', 'clusterId', 'confidence']} for d in obj]
        self.obj = [{k: v for k, v in d.items() if k in  key_list} for d in self.obj]
    
    # @staticmethod
    def update_time(self, ref_key="created_date"):
        [d.update((k, self.get_time(v, format='string')) for k, v in d.items() if k == ref_key) for d in self.obj]
        # return obj
    
    @staticmethod
    def is_bw(img):
        return len(set(img[0][0])) ==1 and len(set(img[10][10])) == 1

    # @staticmethod
    def update_ref_image(self, ref_key = 'ref_image'):
        """Update the ref_image from string to image in obj""" 
        # self.obj = [dict(item, ref_key=self.stringToRGB(item[ref_key])) for i, item in enumerate(self.obj)]
        print("Updatng ref_image. Process may take some time")
        [d.update((k, self.stringToRGB(v)) for k, v in d.items() if k == ref_key) for d in self.obj]
        print("Updatation Ccmpleted")
        # return obj

    # @staticmethod
    def split_side_front_faces(self, ref_key = 'ref_image'):
        """gives front and side face records"""
        sideface_ids = self.get_side_face_records(ref_key=ref_key)
        refined_obj = self.get_front_face_records(ref_key=ref_key)
        return refined_obj, sideface_ids    

    # @staticmethod
    def get_front_face_records(self, ref_key = 'ref_image'):
        """gives front face records"""
        refined_obj = [d for d in self.obj if not(self.is_bw(d[ref_key]))]
        return refined_obj

    # @staticmethod
    def get_side_face_records(self, ref_key = 'ref_image'):
        """gives side face records"""
        sideface_ids = [d for d in self.obj if self.is_bw(d[ref_key])]
        return sideface_ids

    @staticmethod
    def get_time(date, format='string'):
        """converts a string timestamp/strptime to time object"""
        try:
            if format == 'timestamp':
                return datetime.datetime.fromtimestamp(date//1000)
            else:
                return datetime.datetime.strptime(date[:19], '%Y-%m-%d %H:%M:%S')
        except Exception as e:
            print("Plase select proper timestamp: %s"%e)
    
    # @staticmethod
    def update_ref_track_no(self, ref_key = 'ref_track_no'):
        """Update the ref track number from string to int in obj""" 
        [d.update((k, int(v)) for k, v in d.items() if k == ref_key) for d in self.obj]

    @staticmethod
    def mean(lst):
        return sum(lst)/len(lst)
    
    def update_embed(self, ref_key = 'embeded'):
        [d.update((k, get_numeric_embed(v)) for k, v in d.items() if k == ref_key) for d in self.obj]
        # self.obj = [dict(item, ref_key=get_numeric_embed(item[ref_key])) for i, item in enumerate(self.obj)]

    @staticmethod
    def plot_embed(embeds):
        x = range(0, 128)
        plt.plot(x, embeds, 'k')
        plt.show()

    @staticmethod
    def separate_small_tracks(track_based_data, T=3):
        short_tracks = {k:v for k, v in track_based_data.items() if len(v["embeded"]) < T}
        long_tracks = {k:v for k, v in track_based_data.items() if len(v["embeded"]) >= T}
        
        return short_tracks, long_tracks
    
    @staticmethod
    def save_track_images(tracks, path=None, show=False):
        if path is not None:
            path = create_folder(path)
        for k, v in tracks.items():
            # img_list = [stringToRGB(img_str) for img_str in v["refImage"]]
            img_list = v["ref_image"]
            montage = DbUtils.get_montage(img_list)
            if show:
                cv2.imshow("Montage", montage[0])
                cv2.waitKey(10)
            if path is not None:
                if len(Counter(v["ref_track_no"])) > 1:                    
                    cv2.imwrite(os.path.join(path, "%s_merged_%d_%s.jpg"%(k, len(Counter(v["ref_track_no"])),len(v["ref_track_no"]))), montage[0])            
                else:
                    cv2.imwrite(os.path.join(path, "%s_single_%s.jpg"%(k,len(v["ref_track_no"]))), montage[0])

    @staticmethod
    def get_montage(img_list):
        rows = math.ceil(math.sqrt(len(img_list)))
        columns = math.ceil(len(img_list)/rows)
        montage = build_montages(img_list, (125, 125), (rows, columns))
        return montage[0]



# path = "/home/ahmad/track_data_202002071201.json"
# # ref_key1 = 'select * from track_data td where cluster_id is null order by created_date'

# data_obj = load_json(path)

# data_obj = data_obj[ref_key1]

# dbu = DbUtils(data_obj)
# dbu.update_ref_track_no()
# dbu.update_ref_image()
# dbu.update_embed()





# data_obj = update_ref_track_no(data_obj)

print("Finished")



from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV
# from classifier_visualization import cm_analysis
import logging
lg = logging.getLogger(__name__)

def train_svc_with_gridsearch(X, y, nfolds=3):
    lg.debug("Svc gridsearch initiated")
    Cs = [0.1, 1, 10, 20, 40]
    gammas = [0.001, 0.01, 0.1, 1]
    param_grid = {'C': Cs, 'gamma' : gammas}
    lg.debug("Parameters Grid: %s"%param_grid)
    grid_search = GridSearchCV(SVC(kernel='rbf'), param_grid, cv=nfolds, verbose=10, n_jobs=1)
    grid_search.fit(X, y)

    # grid_search.best_params_
    lg.debug(grid_search.best_score_)
    lg.debug(grid_search.best_params_)
    # print(grid_search.best_estimator_)
    svc_best_estimator = grid_search.best_estimator_
    svc_best_estimator1 = svc_best_estimator.set_params(probability=True)
    svc_best_estimator1.fit(X, y)
    lg.debug(svc_best_estimator1)
    return svc_best_estimator1

def train_defualt_svc(X, y, C=10, cache_size=200, class_weight=None, coef0=0.0,
                decision_function_shape='ovr', degree=3, gamma=1, kernel='rbf',
                max_iter=-1, probability=True, random_state=None, shrinking=True,
                tol=0.001, verbose=False):

    svc_model = SVC(C=C, cache_size=cache_size, class_weight=class_weight, coef0=coef0,
                decision_function_shape=decision_function_shape, degree=degree, gamma=gamma, kernel=kernel,
                max_iter=max_iter, probability=probability, random_state=random_state, shrinking=shrinking,
                tol=tol, verbose=verbose)
    
    
    svc_model.fit(X, y)
    lg.debug(svc_model)
    return svc_model

import pandas as pd
from sklearn.metrics import confusion_matrix
import numpy as np
import os
from sklearn.manifold import TSNE
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn import metrics
from collections import Counter

def cm_analysis(y_true, y_pred, labels, prob_thr, ymap=None, figsize=(10,10)):
    """
    Generate matrix plot of confusion matrix with pretty annotations.
    The plot image is saved to disk.
    args: 
    y_true:    true label of the data, with shape (nsamples,)
    y_pred:    prediction of the data, with shape (nsamples,)
    filename:  filename of figure file to save
    labels:    string array, name the order of class labels in the confusion matrix.
                use `clf.classes_` if using scikit-learn models.
                with shape (nclass,).
    ymap:      dict: any -> string, length == nclass.
                if not None, map the labels & ys to more understandable strings.
                Caution: original y_true, y_pred and labels must align.
    figsize:   the size of the figure plotted.
    """
    if ymap is not None:
        y_pred = [ymap[yi] for yi in y_pred]
        y_true = [ymap[yi] for yi in y_true]
        labels = [ymap[yi] for yi in labels]
    cm = confusion_matrix(y_true, y_pred, labels=labels)
    cm_sum = np.sum(cm, axis=1, keepdims=True)
    cm_perc = cm / cm_sum.astype(float) * 100
    annot = np.empty_like(cm).astype(str)
    nrows, ncols = cm.shape
    for i in range(nrows):
        for j in range(ncols):
            c = cm[i, j]
            p = cm_perc[i, j]
            if i == j:
                s = cm_sum[i]
                annot[i, j] = '%.1f%%\n%d/%d' % (p, c, s)
            elif c == 0:
                annot[i, j] = ''
            else:
                annot[i, j] = '%.1f%%\n%d' % (p, c)
    cm = pd.DataFrame(cm, index=labels, columns=labels)
    cm.index.name = 'Actual'
    cm.columns.name = 'Predicted'
    fig, ax = plt.subplots(figsize=figsize)
    fig.suptitle("Threshold"+'%.2f' %prob_thr, fontsize=20)
    sns.heatmap(cm, annot=annot, fmt='', ax=ax,  cmap="rocket_r",linewidths=.5)
    filename = "heatmap_18_04/"+os.path.basename(os.path.dirname(valset_dir_path))+"-"+'%.2f' %prob_thr+"-"+time.strftime("%Y%m%d-%H%M%S")+".png"
    plt.savefig(filename)
    plt.show(block=False)

def get_metrics(y_pred, y_true):  
    # compute more than just one metrics

    chosen_metrics = {
        'conf_mat': metrics.confusion_matrix,
        'accuracy': metrics.accuracy_score,
        'classification_report' : metrics.classification_report
    }

    results = {}
    for metric_name, metric_func in chosen_metrics.items():
        try:
            if metric_name == 'classification_report':
                y_true_dict = Counter(y_true)
                inter_res = metric_func(y_pred, y_true, target_names=list(y_true_dict.keys()), digits=4)
            else:
                inter_res = metric_func(y_pred, y_true)

        except Exception as ex:
            inter_res = None
            print("Couldn't evaluate %s because of %s"%(metric_name, ex))
        results[metric_name] = inter_res

    results['conf_mat'] = results['conf_mat'].tolist()
    # results['clasf_report'] = results['clasf_report'].tolist()

    return results
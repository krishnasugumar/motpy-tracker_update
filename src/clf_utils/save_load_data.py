import json
import os
import pickle
from .folder_utils import create_folder, check_file
import numpy as np
def load_data(path = "db/images/dataset_ppk.pickle"):
    with open(path, 'rb') as f:
        X, y, paths_ = pickle.load(f)
    return (X, y, paths_)

def save_data(X, y, paths_, path = "db/images/dataset_ppk.pickle"):
    with open(path, 'wb') as f:
        pickle.dump((X, y, paths_), f)

def save_model(svc_model, path=None):
    path1 = create_folder(os.path.dirname(path))
    with open(path, 'wb') as f:
        pickle.dump(svc_model, f)

def save_classfile(encoder, path=None):
    path1 = create_folder(os.path.dirname(path))
    np.save(path, encoder.classes_)

def save_one_shot_learning(X, y, path=None):
    path1 = create_folder(os.path.dirname(path))
    with open(path, 'wb') as f:
        pickle.dump((X, y), f)

def load_oneshot(path=None):
    with open(path, 'rb') as f:
        X, y = pickle.load(f)
    return (X, y)

def load_json(path):
    path = check_file(path)
    with open(path, 'r') as myfile:
        data=myfile.read()
    data_obj = json.loads(data)
    return data_obj

def save_json(data, path = None):
    path1 = create_folder(os.path.dirname(path))
    with open(path, 'w') as f:
        json.dump(data, f)
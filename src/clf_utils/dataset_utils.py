"""
Ahmad
File Name: Dataset utils
Update1: Preproces dataset to get face recognition training data
Update2: Separating data based on the camera get_cambased_data(data_obj)
"""

import ast 
import logging 
import time 
from collections import Counter
from sklearn.model_selection import train_test_split
from clf_utils.cw_clustering import do_clustering

lg = logging.getLogger(__name__)
import numpy as np
def random_indexes(num):
    x = np.random.randint(num, size=num)
    return np.argsort(x)

def preprocess_dataset(X, y, count_threshold=30, test_size = 0.15, random_state=42):
    lg.debug("The minimum number of images each class should contains: %s"%count_threshold)
    lg.debug("The training data size %s and test data size is %s"%(1-test_size, test_size))
    class_dict = Counter(y)
    lg.debug(class_dict)
    # weak_class, weak_class_count = class_dict.most_common()[-1]

    strong_classes = {k:v for k,v in class_dict.items() if v > count_threshold}
    weak_classes = {k:v for k,v in class_dict.items() if v < count_threshold}
    if len(weak_classes) is 0:
        lg.debug("All %s class labels contains enough training and testing images"%len(strong_classes))
    else:
        lg.debug("{} class labels out of {} removed from training becuase of less training data".format(len(weak_classes), len(class_dict)))
        for class1, count in weak_classes.items():
            lg.debug("Class {} contains just {} images: Hense Removed in present training. Add atleat {} more images for next training".format(class1, count, (count_threshold-count)))
    
    count_threshold = max(min(strong_classes.values()), count_threshold)
    lg.debug("The Number of classes selected for training: %s"%len(strong_classes))
    lg.debug("The Number of images each class contains: %s"%count_threshold)    
    X_train = []
    y_train = []
    for key in strong_classes.keys():
        X_temp = X[y==key]
        y_temp = y[y==key]
        rand_indxs = random_indexes(len(X_temp))
        X_train.append(X_temp[rand_indxs[:count_threshold]])
        y_train.append(y_temp[rand_indxs[:count_threshold]])

        # X_train.append(X[y==key][:count_threshold])
        # y_train.append(y[y==key][:count_threshold])

    X_1 = [item for sublist in X_train for item in sublist]
    y_1 = [item for sublist in y_train for item in sublist]
    if test_size > 0.05:
        X_train, X_test, y_train, y_test = train_test_split(X_1, y_1, test_size=test_size, random_state=random_state)
    else:
        X_train, X_test, y_train, y_test = X_1, X_1[:5], y_1, y_1[:5]

    return (X_train, X_test, y_train, y_test)

def get_cambased_data(data_obj):
    #Update2: Separating data based on the camera get_cambased_data(data_obj)
    empdict_ppk0 = {}    
    for key, records in data_obj.items():
        for record in records:
            empdict_ppk0.setdefault(record["camera_id"], {}).setdefault("emp_id",[]).append(record["emp_id"])
            empdict_ppk0.setdefault(record["camera_id"], {}).setdefault("ref_track_no",[]).append(record["ref_track_no"])
            empdict_ppk0.setdefault(record["camera_id"], {}).setdefault("createddate",[]).append(record["createddate"])
            empdict_ppk0.setdefault(record["camera_id"], {}).setdefault("embeded",[]).append(record["embeded"])
    return empdict_ppk0
    


def get_empbased_data(data_obj):
    empdict_ppk1 = {}
    empdict_ppk1["emp_id"] = [record["emp_id"] for record in data_obj]
    empdict_ppk1["embeded"] = [record["embeded"] for record in data_obj]
    return empdict_ppk1

def get_separated_by_key(obj, ref_key="camera_id"):
    result_dict = {}
    a = time.time()
    for record in obj:
        for key, val in record.items():
            if ref_key is "camera_id":
                if record["camera_id"] in ["ppk-0", "ppk-1"]:
                    result_dict.setdefault(record[ref_key], {}).setdefault(key,[]).append(val)
            else:
                result_dict.setdefault(record[ref_key], {}).setdefault(key,[]).append(val)

    lg.debug("Time to get a specific key based data: %0.2f secs"%(time.time()-a))
    return result_dict

def get_numeric_embed(embed):
    try:
        return ast.literal_eval(embed)
    except:
        return embed

def get_outliers_and_strip_cluster(embeds, trkids, T_cl=4.0, cl_type="db"):
    clids, trkids = do_clustering(embeds, trkids, cluster_threshold=T_cl, cl_type=cl_type)

    outlier_ids = [trkid for trkid, clid in zip(trkids, clids) if clid > 4900]
    inlier_embeds = [embed for embed, clid in zip(embeds, clids) if clid <= 4900]
    inlier_ids =  [trkid for trkid, clid in zip(trkids, clids) if clid <= 4900]

    return outlier_ids, inlier_ids, inlier_embeds


def outlier_removal(cluster_points, trkids, iterations=5, T= 0.95, T_cl = 4.2, method = "clustering"):
    try:
        outlier = []
        outlier_ids = []
        inlier = cluster_points
        for i in range(iterations):
            if len(inlier) <5:
                break
            if i%2 == 0: # method is "clustering": #
                outlier_id, intlier_id, inlier = get_outliers_and_strip_cluster(inlier, trkids, T_cl=T_cl-0.1*i)
            else:
                centroid = get_centroid(inlier)
                outlier_id, intlier_id, inlier = get_outliers_and_strip_distance(inlier, trkids, centroid, T=T)
            trkids = intlier_id
            # outlier.extend(out)
            outlier_ids.extend(outlier_id)
    except Exception as e:
        print("Unknown Error", e)

    return outlier_ids, inlier

def get_centroid(input_list, key="mean"):
    if key is "kmeans":
        return get_kmeans_centroid(input_list)
    else:
        return get_mean_centroid(input_list)

def get_centroid_by_clustering(embeds, trkids=None, cl_type='db', cluster_threshold=0.42):
    if trkids == None:
        trkids = list(range(len(embeds)))
    clids, trkids = do_clustering(embeds, trkids, cluster_threshold=cluster_threshold, cl_type=cl_type)
    most_common_id = Counter(clids).most_common()[0][0]
    refined_embeds = [embed for embed, clid in zip(embeds, clids) if clid == most_common_id]
    refined_trkids = [trkids for trkid, clid in zip(trkids, clids) if clid == most_common_id]
    lg.debug("Number of removed samples: %d, Number of used samples: %d"%(len(embeds)-len(refined_embeds),len(refined_embeds)))
    centroid_embed = get_centroid(refined_embeds)

    return centroid_embed

def get_centroid_by_outlier_removal(embeds, trkids=None, iterations=5, T= 0.93):
    if trkids == None:
        trkids = list(range(len(embeds)))

    outlier_ids, inliers = outlier_removal(embeds, trkids, iterations=iterations, T= T)
    mean_centroid = get_centroid(inliers)
    lg.debug("Number of outliers: %d, Number of inliers: %d"%(len(outlier_ids), len(inliers)))
    return mean_centroid

def get_kmeans_centroid(input_list, random_state=0):
    kmeans = KMeans(n_clusters=1, random_state=random_state).fit(input_list)
    centroid1 = kmeans.cluster_centers_[0]
    return centroid1

def get_mean_centroid(input_list):
    try:
        return np.mean(input_list, axis=0)
    except Exception as e:
        print("Empty Embeddig Exception: ", e)
        return None

def get_outliers_and_strip_distance(cluster_points, ids, centroid, T= 0.7):

    d_vector = np.array([distance(point, centroid) for point in cluster_points])
    d_max = d_vector.max()
    # outliers = [row for row in cluster_points if distance(centroid, row) / d_max > T]
    outliers_ids = [id for id, row in zip(ids, cluster_points) if distance(centroid, row) / d_max > T]
    new_cluster = [row for row in cluster_points if distance(centroid, row) / d_max <= T]
    inliers_ids = [id for id, row in zip(ids, cluster_points) if distance(centroid, row) / d_max <= T]
    return outliers_ids, inliers_ids, new_cluster

def distance(a, b, axis=0):
    return np.linalg.norm(np.array(a)-np.array(b),axis=axis)

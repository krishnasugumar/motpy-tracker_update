import os
import time
from glob import glob

import cv2
import dlib
import numpy as np
from tqdm import tqdm

from clf_utils.folder_utils import create_folder
from clf_utils.save_load_data import save_data
from classes.face_recogniser import FaceRecogniser

face_rec = FaceRecogniser(ignore_clf=True)
face_det = dlib.cnn_face_detection_model_v1("assets/dlib_models/models/mmod_human_face_detector.dat")

def read_paths(images_dir_path1, train_ = True):
    classes_dir = os.listdir(images_dir_path1)   
    class_image_paths = {}
    for each_class in classes_dir:
        if train_ is True:
            class_image_paths[each_class] = glob(os.path.join(images_dir_path1, each_class, "*.jpg"))
        else:
            class_image_paths[each_class] = glob(os.path.join(images_dir_path1, each_class, "**/*.jpg"))
    return class_image_paths

def get_embedding(image):
    emb = np.squeeze(face_rec.get_embeddings(image, dont_detect=True))
    return emb


def prepare_embeddings(images_dir_path1, dont_show = True, save_ = True, out_path=None, train_ = True):
    X = []
    y = []
    paths_ = []

    class_image_paths = read_paths(images_dir_path1,  train_ =train_)
    used, removed = 0, 0
    for each_class, img_paths in class_image_paths.items():
        
        for img_path in tqdm(img_paths):
            img = cv2.imread(img_path)
            fimg = get_face_crop(img[:,:,::-1])
            if fimg is not None:
                if not dont_show: cv2.imshow("cropped", fimg)                    
                emb = np.squeeze(face_rec.get_embeddings(fimg, dont_detect=True))
                X.append(emb)
                y.append(each_class)
                paths_.append(img_path)       
                used += 1             
            else:
                if not dont_show: cv2.imshow("noface", img)
                removed += 1

            cv2.waitKey(1)
    # print("%d images removed, %d images processed"%(removed, used))
    X, y = np.array(X), np.array(y)
    if save_ is True:
        out_path = create_folder(out_path) + "dataset-embed"+"-"+time.strftime("%Y%m%d-%H%M%S")+".pickle"
        save_data(X, y, paths_, path=out_path)
    if train_:
        return (X, y)
    else:
        return (X, paths_)


def get_face_crop(rgbimg):
    try:
        rect = [x.rect for x in face_det(rgbimg, 1)][0]
        x1,y1,x2,y2 = rect.left(), rect.top(), rect.right(), rect.bottom()
        x1 = max(x1, 0)
        y1 = max(y1, 0)
        return rgbimg[y1:y2, x1:x2]
    except:
        return None


# X, y = prepare_embeddings("/data/work/face-rec/face-rec/data/testing/detected_faces/dataset_images")

# svc_best_estimator1 = train_svc_with_gridsearch(X, y,3)
# svc_best_estimator1.predict(X[0].reshape(1, 128))
# # print(X[0], y[0])
# svc_model = train_defualt_svc(X, y)
# y_pred = svc_model.predict(X[:150].reshape(150, 128))
# y_true = y[:150]
# save_data(X, y, path="sample.pickle")

# result = get_metrics(y_pred, y_true)
# print(len(X), len(y))
# X1, y1 = load_data(path="sample.pickle")
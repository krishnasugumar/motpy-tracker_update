
class DbNames(object):
    def __init__(self, record):
        self.record = record
        self.update()
    def naming_style(self):
        if "_" in "".join(list(self.record.keys())): 
            print("snake_case styled data: %s"%list(self.record.keys()))
            return "snake_case"
        else:
            print("cap_case styled data: %s"%list(self.record.keys()))
            return "cap_case"

    def update(self):
        if self.naming_style() is "snake_case":
            self.embeded = "embeded"
            self.camera_id = "camera_id"
            self.trackdata_id = "trackdata_id"
            self.ref_image = "ref_image"
            self.created_date = "created_date"
            self.emp_id = "emp_id"
            self.trackunique_id = "trackuniqid"
            self.cluster_id = "cluster_id"
            self.created_by = "created_by"
            self.first_name = "first_name"
            self.full_name = "full_name"
            self.last_name = "last_name"
            self.unique_identification = "unique_identification"
            self.updated_by = "updated_by"
            self.updated_date = "updated_date"
            self.employee_code = "employee_code"
            self.employee_name = "employee_name"
            self.work_location_code = "work_location_code"
            self.contact_number = "contact_number"
            self.emergency_contact_number = "emergency_contact_number"
            self.employee_contractor = "employee_contractor"
            self.verify_id = "verify_id"
            self.verification_status = "verification_status"
            self.verified_date = "verified_date"
            self.verifiedby = "verifiedby"
            self.version_num = "version_num"
            self.status = "status"
            self.del_reason_id = "del_reason_id"

        else:
            self.embeded = "embeded"
            self.camera_id = "cameraId"
            self.trackdata_id = "trackdataId"
            self.ref_image = "refImage"
            self.created_date = "createdDate"
            self.emp_id = "EmployeeId"
            self.trackunique_id = "trackuniqid"
            self.cluster_id = "clusterId"
            self.created_by = "createdBy"
            self.first_name = "firstName"
            self.full_name = "fullName"
            self.last_name = "lastName"
            self.unique_identification = "uniqueIdentification"
            self.updated_by = "updatedBy"
            self.updated_date = "updatedDate"
            self.employee_code = "employeeCode"
            self.employee_name = "employeeName"
            self.work_location_code = "workLocationCode"
            self.contact_number = "contactNumber"
            self.emergency_contact_number = "emergencyContactNumber"
            self.employee_contractor = "employeeContractor"
            self.verify_id = "verifyId"
            self.verification_status = "verificationStatus"
            self.verified_date = "verifiedDate"
            self.verifiedby = "verifiedby"
            self.version_num = "versionNum"
            self.status = "status"
            self.del_reason_id = "delReasonId"
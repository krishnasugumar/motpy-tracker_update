from clf_utils.db_utils import DbUtils
from clf_utils.save_load_data import load_json, save_data, save_json
from clf_utils.dataset_utils import get_separated_by_key, outlier_removal
from clf_utils.folder_utils import create_folder
import os
from collections import Counter
import cv2
import math
from imutils import build_montages
from ui.diagnostics import DBHandler
import json
import random
from classes.keras_facefilter import FaceFilter
from sklearn.metrics import precision_recall_fscore_support
import time
import pandas as pd
import traceback
from clf_utils.db_names import DbNames

TRAIN = False
FROM_DB = True
CLUSTER = not TRAIN
CAM_IDs = ["ppk-0"] #, "ppk-0"]
start_date = "2020-03-25"
end_date = "2020-04-06"
PATH = "/data/work/face-rec/face-data/defualt_images/train_data_analysis"
# PATH = "/mnt/extstorage/track_montage_view"
json_path = "/data/Downloads/traning_data/trainDump.json"

ff = FaceFilter()
dbh = DBHandler()

def load_json1(path):
    data = [json.loads(line) for line in open(path, 'r')]
    return data


def mark(image, msg=""):
    length, height, depth = image.shape
    org = (length//2-20, height//2-20)
    font = cv2.FONT_HERSHEY_SIMPLEX 
    fontScale = 2
    thickness = 3
    color = (0, 255, 0) 

    if msg.lower() == "sf":
        color = (0, 255, 0) 
    elif msg.lower() == "ol":
        color = (0, 255, 255)
    elif msg.lower() == 'ef':
        color = (255, 255, 0) 
    
    cv2.putText(image, msg, org, font, fontScale, color, thickness, cv2.LINE_AA)
    return image

query = """select *
from employee_info ei inner join employee_verification ev on ei.emp_id=ev.emp_id inner join track_data td on td.verify_id=ev.verify_id
where td.created_date between '2020-04-08 15:30:00'::timestamp and '2020-04-10 16:20:00'::timestamp"""

def get_query_dict(start_date=None,end_date=None):
    date_list = pd.date_range(start=start_date,end=end_date)
    query_dict = {}
    # shifts = ["shift"]
    for dat in date_list:
        date_str = str(dat.date())
        query_dict.setdefault(date_str, {})
        query_dict[date_str]["shift-1_start"] = "select * from track_data td where td.created_date between '" + date_str + " 06:00:00'::timestamp and '" + date_str + " 09:00:00'::timestamp"
        query_dict[date_str]["shift-2_start"] = "select * from track_data td where td.created_date between '" + date_str + " 15:00:00'::timestamp and '" + date_str + " 16:00:00'::timestamp"
        query_dict[date_str]["shift-1_end"] = "select * from track_data td where td.created_date between '" + date_str + " 15:00:00'::timestamp and '" + date_str + " 18:00:00'::timestamp"

    # print("Queries generated", query_dict)
    return query_dict

def get_montage(img_list):
    rows = math.ceil(math.sqrt(len(img_list)))
    columns = math.ceil(len(img_list)/rows)
    montage = build_montages(img_list, (125, 125), (rows, columns))
    return montage[0]

def get_id_based_dict(dbu, db_names, TRAIN = True):
    CLUSTER = not True
    if TRAIN:
        training_data_obj = get_separated_by_key(dbu.obj, ref_key=db_names.emp_id) #"emp_id"
        min_size = 40
    if CLUSTER:
        training_data_obj = get_separated_by_key(dbu.obj, ref_key=db_names.trackunique_id) #"emp_id"
        min_size = 10
    return training_data_obj, min_size

def remove_empty_embeds(trkids_all, embeds, img_list_all):
    trkids = [id for id, embed in zip(trkids_all, embeds) if len(embed) > 10]
    embeds = [embed for id, embed in zip(trkids_all, embeds) if len(embed) > 10]
    img_list = [img for img, embed in zip(img_list_all, embeds) if len(embed) > 10]
    empty_trkids = list(set(trkids_all)-set(trkids))

    return trkids, empty_trkids, embeds, img_list

def analysis(data_obj, db_names, img_fldr_path = None, show = False, TRAIN=True):
    dbu = DbUtils(data_obj)

    training_data_obj, min_size = get_id_based_dict(dbu, db_names, TRAIN=TRAIN)
    removable_trakdata_ids = []
    for id, emp_data in training_data_obj.items():
        try:
            embeds_all = emp_data[db_names.embeded] #100
            created_date_list = emp_data[db_names.created_date] #100
            if len(embeds_all) > min_size:
                trkids_all = emp_data[db_names.trackdata_id]     #100
                img_list_str = emp_data[db_names.ref_image] #100
                img_list_all = [dbu.stringToRGB(img_string) for img_string in img_list_str] #100
                
                r_trkids1, empt_trkids, r_embeds1, img_list = remove_empty_embeds(trkids_all, embeds_all, img_list_all) #90, 10, 90
                if len(r_trkids1) < 5:
                    mark_label(trkids_all, img_list_all, empt_trkids=empt_trkids)

                front_trk_ids = [id for id, face in zip(r_trkids1, img_list) if ff.is_front_face(face, thr=0.5) is True] #60
                side_trkids = [id for id in r_trkids1 if id not in front_trk_ids] #30
                r_embeds = [embd for id, embd in zip(r_trkids1, r_embeds1) if id in front_trk_ids]

                if len(front_trk_ids) < 5:
                    mark_label(trkids_all, img_list_all, side_trkids=side_trkids, empt_trkids=empt_trkids) 

                iterations = max(4, len(front_trk_ids)//100)
                outlier_ids, inlier_embeds = outlier_removal(r_embeds, front_trk_ids, iterations=iterations, T= 0.90, method="clustering", T_cl=2.8)
                slected_img_list = [img for img, trkid in zip(img_list, r_trkids1) if trkid not in outlier_ids+side_trkids]            
                # random.shuffle(slected_img_list)
                mark_label(trkids_all, img_list_all, side_trkids=side_trkids, outlier_ids=outlier_ids, empt_trkids=empt_trkids)

                montage = get_montage(img_list_all)
                removable_trakdata_ids.extend(empt_trkids+side_trkids+outlier_ids)
                if img_fldr_path is not None:
                    img_path = os.path.join(img_fldr_path, "montage_%d_%d_%s.jpg"%(len(img_list_all), len(Counter(created_date_list)), id))
                    cv2.imwrite(img_path, montage)
                    print("%s has total: %d side: %d, outlier: %d good: %d"%(id, len(trkids_all), len(side_trkids), len(outlier_ids), len(slected_img_list)))
                if show:
                    cv2.imshow("montage", montage)
                    cv2.waitKey(10)
        except Exception as e:
            print("Error Occurred", e)
            traceback.print_exc()
            time.sleep(0.001)
    return removable_trakdata_ids

def mark_label(trkids_all, img_list_all, side_trkids=[], outlier_ids=[], empt_trkids=[]):
    for i in range(len(trkids_all)):
        trkid, img = trkids_all[i], img_list_all[i]
        if trkid in side_trkids:
            img_list_all[i] = mark(img, msg="SF")
        elif trkid in outlier_ids:
            img_list_all[i] = mark(img, msg="OL")
        elif trkid in empt_trkids:
            img_list_all[i] = mark(img, msg="EF")

def single_query(query, img_path=None):
    data_obj = querydb(query)
    
    if data_obj is not None:
        db_names = DbNames(data_obj[0])
        for cam_id in CAM_IDs:
            data_obj_cam = [record for record in data_obj if record[db_names.camera_id] in [cam_id]]
            print("cam: %s, records: %d"%(cam_id, len(data_obj)))
            if img_path is not None:
                output_path = create_folder(os.path.join(img_path, cam_id))
                removable_trakdata_ids = analysis(data_obj_cam, db_names, img_fldr_path=output_path)
                print("Montages stored location:%s, Removable_ids: %d"%(output_path, len(removable_trakdata_ids)))
            else:
                removable_trakdata_ids = analysis(data_obj_cam, db_names)
                print("Montage Storage path not exists. No. of removable_ids: %d"%len(removable_trakdata_ids))

def querydb(query):
    fields, data_obj = dbh.querydb(query, asdict=True)
    if len(data_obj) is 0:
        print("No recods found in this shift")
        return None, None
    print("total records: %d"%(len(data_obj)))
    return data_obj

def multi_query(query_dict, img_path = None):
    print("INFO: DB handler intiated")
    for dat, shifts in query_dict.items():
        for shift, query in shifts.items():
            print("query", query)
            data_obj = querydb(query)
            
            if data_obj is not None:
                db_names = DbNames(data_obj[0])
                for cam_id in CAM_IDs:
                    data_obj_cam = [record for record in data_obj if record[db_names.camera_id] in [cam_id]]
                    print("date= %s, shift: %s, cam: %s, records: %d"%(dat, shift, cam_id, len(data_obj)))
                    if img_path is not None:
                        output_path = create_folder(os.path.join(img_path, dat, shift, cam_id))
                        removable_trakdata_ids = analysis(data_obj_cam, db_names, img_fldr_path=output_path)
                        print("Montages stored location:%s, Removable_ids: %d"%(output_path, len(removable_trakdata_ids)))
                    else:
                        removable_trakdata_ids = analysis(data_obj_cam, db_names)
                        print("Montage Storage path not exists. No. of removable_ids: %d"%len(removable_trakdata_ids))
                    

def main():
    try:
        img_fldr_path = PATH+"-"+time.strftime("%Y%m%d-%H%M%S")
        img_path = create_folder(img_fldr_path)
        print("INFO: Folder created")
        if 0:
            query_dict = get_query_dict(start_date=start_date, end_date=end_date)
            multi_query(query_dict, img_path =img_path)
        if 0:
            single_query(query, img_path =img_path)
        
        if 1:
            data_obj = load_json1(json_path)
            db_names = DbNames(data_obj[0])
            print("Selected camids: %s"%CAM_IDs)
            data_obj_cam = [record for record in data_obj if record[db_names.camera_id] in CAM_IDs]
            selctd_cam_rmvbl_trkdt_ids = analysis(data_obj_cam, db_names, img_fldr_path=img_path)
            print("NO.of removed trackids from %s is %s"%(CAM_IDs, len(selctd_cam_rmvbl_trkdt_ids)))
            unslctd_cam_rmvbl_trkdt_ids = [record[db_names.trackdata_id] for record in data_obj if record[db_names.camera_id] not in CAM_IDs]
            print("Unselected Camera trackids %s"%(len(selctd_cam_rmvbl_trkdt_ids)))
            total_removeable_trkids = unslctd_cam_rmvbl_trkdt_ids+selctd_cam_rmvbl_trkdt_ids
            print("Total number of removable track ids: %s"%len(total_removeable_trkids))
            rmvbl_ids_path = "/data/work/face-rec/face-data/removable_ids_%d_%s.json"%(len(total_removeable_trkids), time.strftime("%Y%m%d-%H%M%S")) 
            save_json(total_removeable_trkids, path=rmvbl_ids_path)
            print("Data saved: path: %s"%rmvbl_ids_path)
    except Exception as e:
        print("Main function error", e)
        traceback.print_exc()
        time.sleep(0.001)









if __name__ == "__main__":
    main()

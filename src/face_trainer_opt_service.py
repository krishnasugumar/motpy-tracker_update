import time
import numpy as np
from collections import Counter
import traceback
import schedule
import sys
from vap.config import AppConfig

from vap.framework.kai_process import KaiProcess
from clf_utils.save_load_data import save_model, save_classfile, save_one_shot_learning, load_json
from clf_utils.dataset_utils import preprocess_dataset, get_empbased_data
from clf_utils.svc_model_selection import train_svc_with_gridsearch, train_defualt_svc
from clf_utils.class_label_utils import preprocess_labels
from clf_utils.classifier_visualization import get_metrics
from clf_utils.dataset_utils import (get_cambased_data,
                                     get_centroid_by_clustering,
                                     get_centroid_by_outlier_removal,
                                     get_empbased_data, get_numeric_embed,
                                     get_separated_by_key, outlier_removal,
                                     preprocess_dataset)
from clf_utils.db_utils import DbUtils

from classes.backend_api_rest import BackendAPI
import logging
lg = logging.getLogger(__name__)
from face_clustering_opt_service import get_nonzero_embeddings

app_config = AppConfig("config.json")

# VARIATION_THRESHOLD = 2
EMBEDED = "embeded"
REF_IMAGE = "refImage"
CAM_ID = "camera_id"
EMP_ID = "emp_id"
SINGLE_EMBED = "single_embed"
MULTI_EMBED = "multi_embed"
CREATE_DATE = 'createddate' #'created_date' #'createddate'
# MODEL_PREFIX = config.recognition.training.svc.dlib_clf_path
# CLASSFILE_PREFIX = config.recognition.training.svc.classfile_prefix
# COUNT_THRESHOLD = config.recognition.training.count_threshold #10
# TEST_SIZE = 0. # config.recognition.training.test_size
# TRAIN_FILE_TYPE = config.recognition.recognition_type #"single_embed" or "multi_embed"
# ONE_SHOT = config.recognition.training.oneshot.db_path

# CAM_ID = ["ppk-1"]
# path = '/data/work/face-rec/face-data/clustering_input_data/raw_json/clustering-20200312-164934.json'
path = '/data/work/face-rec/face-data/ppk_track_data_table/Training_latest.json'
# path = "/data/Downloads/traning_data/trainDump.json"
# def load_data():
#     # with open('/data/work/face-rec/face-data/ppk_track_data_table/Training_latest.json', 'r') as myfile:
#     with open('/data/work/face-rec/face-data/clustering_input_data/raw_json/clustering-20200312-164934.json', 'r') as myfile:
#         data=myfile.read()
#     data_obj = json.loads(data)
#     return data_obj

class FaceTrainer(object):
    """
    FaceTrainer class creates a training data file. It recieves data from from DB and then selects the  
    """
    # def __init__(self, model_prefix=MODEL_PREFIX, classfile_prefix = CLASSFILE_PREFIX, one_shot=ONE_SHOT):
    def __init__(self, config):
        self.config = config
        self.model_prefix = self.config.recognition.training.svc.dlib_clf_path
        self.classfile_prefix = self.config.recognition.training.svc.classfile_prefix
        self.one_shot = self.config.recognition.training.oneshot.db_path
        self.train_file_type = self.config.recognition.recognition_type
        self.count_threshold = self.config.recognition.training.count_threshold
        self.camera_ids = self.config.recognition.training.camera_ids
        self.variation_threshold = self.config.recognition.training.variation_threshold

    def get_cam_based_embeds(self, data, cam_id = None, ref_key=CAM_ID):
        """ Takes embeddings specific to a perticular camera"""
        data_length = len(data[EMBEDED])
        embeds = [get_numeric_embed(data[EMBEDED][i]) for i in range(data_length) if data[ref_key][i] in cam_id]
        return embeds

    def get_time_based_data(self, data, cam_id = None, ref_key=CAM_ID):
        """ Takes embeddings specific to a perticular camera"""
        data_length = len(data[EMBEDED])
        times = [data[CREATE_DATE][i] for i in range(data_length) if data[ref_key][i] in cam_id]
        return times

    def get_center_embed(self, embeds, centroid_by_clustering=False):
        if centroid_by_clustering is True:
            centroid = get_centroid_by_clustering(embeds)
        else:
            centroid = get_centroid_by_outlier_removal(embeds)

        return centroid

    def remove_empty_embeds(self, embeds, cdates):
        embeds_ = embeds
        cdates_ = cdates
        cdates = [cdate for cdate, embed in zip(cdates_, embeds_) if len(embed) > 0]
        embeds = [embed for cdate, embed in zip(cdates_, embeds_) if len(embed) > 0]
        return embeds, cdates


    def train(self, do_grid_search = True):
        bk_api = BackendAPI(
                host=self.config.backend.host,
                port=self.config.backend.port,
                base_api=self.config.backend.base_api
            )
        # lg.debug(bk_api.url)
        lg.debug("Data Loading is initiated")
        lg.debug("Training data threshold: %d"%self.count_threshold)
        a = time.time()
        data_obj = bk_api.get_training_data()
        # data_obj = load_json(path)
        b = time.time()-a
        lg.debug("Data Loading is completed: Recieved data lenght: %d, time: %0.3fsec"%(len(data_obj), b))
        # self.db_data = DbUtils(data_obj)
        # self.db_data.update_ref_image(ref_key=REF_IMAGE)
        cam_ids = [item[CAM_ID] for item in data_obj]
        lg.debug("Available camids: %s"%str(Counter(cam_ids)))
        lg.debug("Selected camids : %s"%self.camera_ids)
        
        training_data_obj = get_separated_by_key(data_obj, ref_key=EMP_ID) #"emp_id"
        lg.debug("Total number of recieved classes: %d"%len(training_data_obj))
        
        if self.train_file_type == SINGLE_EMBED:
            lg.debug("Single Embed based approach is selected")
        else:
            lg.debug("Multi Embed based approach is selected")
            self.train_file_type = MULTI_EMBED

        try:
            X_train, y_train = [], []
            insuf_training_emp, insuf_variation_emp, satisfied_emp = 0, 0, 0
            used, not_used, insuf_var, insuf_train, no_embeds = 0, 0, 0, 0, 0
            
            for each_emp_id, each_emp_data in training_data_obj.items():
                try:
                    embeds = self.get_cam_based_embeds(each_emp_data, cam_id=self.camera_ids)
                    cdates = self.get_time_based_data(each_emp_data, cam_id=self.camera_ids)
                    recieved_count = len(cdates)
                    embeds, cdates = self.remove_empty_embeds(embeds, cdates)
                    valid_count = len(cdates)
                    cdates = [date[:13] for date in cdates] # removes minutes and seconds
                    variation_count = len(np.unique(cdates))
                    no_embeds += recieved_count-valid_count

                    if valid_count < self.count_threshold:
                        insuf_training_emp += 1
                        insuf_train += valid_count
                        lg.debug("emp_id: %s -%d has insufficient clean data (%d < %d) "%
                            (each_emp_id, recieved_count, valid_count, self.count_threshold))
                        continue
                    
                    if variation_count < self.variation_threshold:
                        insuf_variation_emp += 1
                        insuf_var += valid_count
                        lg.debug("emp_id: %s -%d has insufficient clean data variations (%d < %d)"%
                            (each_emp_id, recieved_count, variation_count , self.variation_threshold))
                        continue
                    
                    satisfied_emp += 1
                    # lg.debug("emp_id: %s -%d has sufficient clean data (%d > %d) and variations (%d < %d)"%
                    #         (each_emp_id, recieved_count, valid_count, self.count_threshold, variation_count , self.variation_threshold))
                    if self.train_file_type == SINGLE_EMBED:
                        centroid = self.get_center_embed(embeds, centroid_by_clustering=False)
                        X_train.append(centroid)
                        y_train.append(each_emp_id)

                    elif self.train_file_type == MULTI_EMBED:
                        ids = [each_emp_id]*valid_count
                        # outlier_ids, inlier_embeds = outlier_removal(embeds, ids, iterations=5, T= 0.93)
                        # X_train += inlier_embeds[:self.count_threshold]
                        # y_train += [each_emp_id]*len(inlier_embeds[:self.count_threshold])

                        # Different Approach
                        time_ranges = Counter(cdates)
                        time_ranges = Counter({k: c for k, c in time_ranges.items() if c >= 4})
                        total = sum(time_ranges.values())
                        minor_date_count = valid_count-total
                        inliers = []
                        major_date_count = 0
                        for each_time, count in time_ranges.items():
                            time_embds = [emb for emb, cdate in zip(embeds, cdates) if cdate==each_time]
                            contrib = int(np.round(len(time_embds)/total*self.count_threshold))
                            ids = list(range(count))
                            outlier_ids, inlier_embeds = outlier_removal(time_embds, ids, iterations=3, T= 0.93)
                            
                            if contrib < len(inlier_embeds):
                                selected_embeds = inlier_embeds[:contrib]
                            else:
                                outlier_embeds = [embed for embed, id in zip(time_embds, ids) if id in outlier_ids]
                                selected_embeds = inlier_embeds + outlier_embeds[:contrib-len(inlier_embeds)]

                            
                            # limit = min((self.count_threshold//len(time_ranges)+1, len(inlier_embeds)))

                            inliers += selected_embeds
                            major_date_count += count-len(selected_embeds)

                        if len(inliers) > self.count_threshold:
                            inliers = inliers[:self.count_threshold]
                        else:
                            inliers += inliers[:self.count_threshold-len(inliers)]
                        X_train += inliers
                        y_train += [each_emp_id]*len(inliers)
                        used +=  len(inliers)
                        not_used += valid_count - len(inliers)
                        lg.debug("emp_id: %s -%d \t embed: %d \t used %d removed %d"%
                            (each_emp_id, recieved_count, valid_count, len(inliers) , major_date_count+minor_date_count))
                except Exception as e:
                    lg.debug("emp_id %s, has an issue"%each_emp_id)
                    lg.debug(e)

            
            lg.debug("Total employees: %d, all satisfied: %d, insufficient data: %d, insuffient varations: %d"%
                    (len(training_data_obj), satisfied_emp, insuf_training_emp, insuf_variation_emp))
            lg.debug("Total records: %d, no embeds: %d, used: %d, not used: %d insufficient data: %d, insuffient varations: %d"%
                    (len(data_obj), no_embeds, used, not_used, insuf_train, insuf_var))
            
            if len(X_train) < 1:
                raise ValueError("Number of samples for training is 0")

            lg.debug("The selected traning file type: %s" % self.train_file_type)
            one_shot_path = self.one_shot + "-" + str(len(Counter(y_train))) + "-" + time.strftime(
                "%Y%m%d-%H%M%S") + ".pickle"
            save_one_shot_learning(X_train, y_train, path=one_shot_path)
            lg.debug("one shot saved successfully: %s" % (one_shot_path))
            lg.debug("Saved training data classes: %d, samples: %d" % (len(np.unique(y_train)), len(y_train)))
        except Exception as e:
            lg.error(e)
            traceback.print_exc()
            time.sleep(0.001)


class TrainingService(KaiProcess):

    def __init__(self, evt_start, evt_finish):
        self.config = AppConfig("config.json")
        self.schedule_times = self.config.recognition.training.schedule
        super().__init__(
            health_status_port=self.config.management.training_service_health_status_port,
            daemon=True
        )
        self.training_start_evt = evt_start
        self.training_finish_evt = evt_finish

    def unique_name(self) -> str:
        return "TrainingService"

    def monitored_status_params(self) -> dict:
        return {"Training": self.training_start_evt.is_set(),
                "schedule": self.schedule_times}

    def signal_training_start(self):
        self.training_finish_evt.clear()
        self.training_start_evt.set()

    def signal_training_finish(self):
        self.training_finish_evt.set()
        self.training_start_evt.clear()

    def job(self):
        lg.info("##### Preparing to Initiate Training Sequence #####")
        self.signal_training_start()
        time.sleep(5)
        lg.info("##### Training Started #####")
        try:
            a = time.time()
            train()
            b = time.time() - a
            lg.debug("Traing process execution time: %0.3f"%b)
        except Exception as e:
            lg.error(e)
            traceback.print_exc()
            lg.info("Training Failed")

        self.signal_training_finish()

    def run(self):
        super().run()
        lg.info("Scheduling Training Everyday at %s" % self.schedule_times)
        for schedule_time in self.schedule_times:
            schedule.every().day.at(schedule_time).do(self.job)

        while not self.shutdown_event.is_set():
            schedule.run_pending()
            time.sleep(1)

        lg.info("Shutting down")
        sys.exit()



def train():
    ft = FaceTrainer(config=app_config)
    ft.train(do_grid_search=True)


if __name__ == "__main__":
    from vap.modules import logging

    lg = logging.get_root_logger(log_name="ayeAccess.FR")
    train()



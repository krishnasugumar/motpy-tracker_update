"""
Owner : Shaik Ahmad. Recode Solurtions
Purpose: Face c;ustering service
Created Date: 05-12-2019: 11:56:00
Update1: code optimized 06-12-2019: 07:29:00
Uodate2: Adding camera ID based cluster separation
Update3: Processing bigclusters
Update4: Performing second round of clustering
Update4: Adding dbscan clustering
Update5: Reimplementing the whole logic using pandas
Update6: Track Merging
Update7: Unrecognised Cluster count increased from 16 to 45 and bug fix
"""
import ast
import numpy as np
import time
from vap.config import AppConfig
from flask import Flask, request, Response
from clf_utils.cw_clustering import do_clustering
import json
import logging
import time

import numpy as np
from flask import Flask, Response, request

from clf_utils.cw_clustering import do_clustering
from vap.config import AppConfig
from classes.backend_api_rest import BackendAPI
lg = logging.getLogger(__name__)
import uuid
from collections import Counter
from classes.one_shot_learning import OneShotRecognition
from vap.framework.kai_process import KaiProcess, run_and_wait
from sklearn.metrics import silhouette_score
import pandas as pd

import cv2
import math
import os
from imutils import build_montages
import base64

from face_registration_service import registration_matching, get_response_json, load_data
from classes.face_recogniser import FaceRecogniser

extensive_log = True
app_config = AppConfig("config.json")

recognition_engine = OneShotRecognition(app_config)
backend_api = BackendAPI()
# INPUT_PATH = app_config.clustering.input_path
# OUTPUT_PATH = app_config.clustering.output_path
COUNT_THRESHOLD = app_config.clustering.min_cluster_size
CLUSTER_THRESHOLD = app_config.clustering.cluster_threshold
BIG_CLUSTER_THRESHOLD = 190
CLUSTERING_ALG = "db"  # "db" #"cw"
UNCLUSTERED = "UnClustered"
INVALID_CLUSTERID = ""
UNKOWN = "UNK"
VALID = 'valid'
MIN_UNREC_COUNT = 50

STATUS = 'status'
CLUSTER_NO = "cluster_no"
CAMERA_ID = "camera_id" #"cameraid"
CLUSTER_ID = "cluster_id" #'clusterId'
REF_IMAGE = 'ref_image'
EMBEDED = 'embeded'
REF_TRACK_NO = "ref_track_no" #"refTrackNo"
TRACK_DATA_ID = "trackdataid" #'trackdataId'
CONFIDENCE = 'confidence'
EMP_ID = 'prediction'
TRACK_UNIQUE_ID = 'trackuniqid'

# STATUS = 'status'
# CLUSTER_NO = "cluster_no"
# CAMERA_ID =  "camera_id" #"cameraid"
# CLUSTER_ID = "cluster_id" #'cluster_id'
# REF_IMAGE = 'ref_image'
# EMBEDED = 'embeded'
# REF_TRACK_NO = "ref_track_no"
# TRACK_DATA_ID = 'trackdataid'
# EMP_ID = 'prediction'
# CONFIDENCE = 'confidence'
# STATUS = 'status'

JS_KEY_CLUSTER_ID = "clusterid"
JS_KEY_TRACK_DATA_ID = "trackdataId"
JS_KEY_CAMERA_ID = "cameraid"
JS_KEY_EMP_ID = "employeeId"
JS_KEY_CONFIRM_EMP_ID = "confirmemployeeId"

def get_nonzero_embeddings(embeddings, uniqueids):
    refined_embeddings = []
    refined_uniqueids = []
    emptyembedding_ids = []
    for i, x in enumerate(embeddings):
        try:
            if len(ast.literal_eval(x)) > 0:
                refined_embeddings.append(ast.literal_eval(embeddings[i]))
                refined_uniqueids.append(uniqueids[i])
            else:
                emptyembedding_ids.append(uniqueids[i])
        except Exception as e:
            emptyembedding_ids.append(uniqueids[i])
            lg.debug("zero clustering error: %s - %s" % (e, x))
    refined_embeddings = np.array(refined_embeddings).reshape(len(refined_embeddings), 128)

    return (refined_embeddings, refined_uniqueids, emptyembedding_ids)

def process_big_clusters(clusterids, uniqueids, refined_embeddings, cid):
    big_clusters = Counter(dict(filter(lambda x: x[1] >= BIG_CLUSTER_THRESHOLD, Counter(clusterids).items())))
    if len(big_clusters) == 0:
        clusterids = np.array(clusterids).reshape(len(clusterids), 1)
        return clusterids

    lg.debug("Number of big clusters (>%s) camera %s: %s" % (BIG_CLUSTER_THRESHOLD, cid, len(big_clusters)))
    clusterids_array = np.array(clusterids).reshape(len(clusterids), 1)
    uniqueids_array = np.array(uniqueids).reshape(len(clusterids), 1)
    for id, val in big_clusters.items():
        big_embeds = refined_embeddings[np.where(clusterids_array == id)[0]]
        big_uniqueids = uniqueids_array[np.where(clusterids_array == id)[0]]
        new_clusterids, new_uniqueids = do_clustering(big_embeds, big_uniqueids,
                                                      cluster_threshold=CLUSTER_THRESHOLD - 0.03,
                                                      cl_type=CLUSTERING_ALG)
        # new_clusterids, new_uniqueids = do_dbscan(big_embeds, big_uniqueids, eps=CLUSTER_THRESHOLD-0.02)
        lg.debug("Old Cluster id: %s embeddings: %d Newly Culsters Count: %d" % (
            id, len(big_uniqueids), len(np.unique(new_clusterids))))
        new_clusterids_array = np.array(new_clusterids).reshape(len(new_clusterids), 1) + 10000
        clusterids_array[np.where(clusterids_array == id)[0]] = new_clusterids_array
    clusterids = clusterids_array
    return clusterids

def save_images(clst, savedir):
    if clst.name == '':
        return

    savepath = os.path.join(savedir, 'clst-0')

    os.makedirs(savepath, exist_ok=True)

    img_list = clst[REF_IMAGE].tolist()
    img_list = [stringToRGB(s) for s in img_list]
    img_list = [img for img in img_list if not is_bw(img)]
    # print(clst.name, len(img_list))

    clst_id = clst.name
    trackNoCount = clst[REF_TRACK_NO].nunique()
    refTrackNo = clst[REF_TRACK_NO].iloc[0]

    rows = math.ceil(math.sqrt(len(img_list)))
    if rows == 0:
        return
    columns = math.ceil(len(img_list) / rows)
    montage = build_montages(img_list, (125, 125), (rows, columns))
    if 0:
        cv2.imshow("Montage", montage[0])
        cv2.waitKey(10)
    if trackNoCount > 1:
        # print("merged_montage_%s_%d_%s.jpg" % (clst_id, trackNoCount, len(img_list)))
        cv2.imwrite(
            os.path.join(savepath,
                         "%d_merged_montage_%s_%d_%s.jpg" % (refTrackNo, clst_id, trackNoCount, len(img_list))),
            montage[0])
    else:
        # print("montage_img_%s_%s.jpg" % (clst_id, len(img_list)))
        cv2.imwrite(os.path.join(savepath, "%d_montage_img_%s_%s.jpg" % (refTrackNo, clst_id, len(img_list))),
                    montage[0])

def save_track_images(trk, savedir):
    if trk.name == '':
        return

    refTrackNo = trk.name

    savepath = os.path.join(savedir, 'tracks')
    os.makedirs(savepath, exist_ok=True)

    img_list = trk[REF_IMAGE].tolist()
    img_list = [stringToRGB(s) for s in img_list]
    img_list = [img for img in img_list if not is_bw(img)]
    rows = math.ceil(math.sqrt(len(img_list)))
    if rows == 0:
        return
    columns = math.ceil(len(img_list) / rows)
    montage = build_montages(img_list, (125, 125), (rows, columns))

    cv2.imwrite(os.path.join(savepath, "trackNo_%s_%s.jpg" % (refTrackNo, len(img_list))), montage[0])


def resolve_clusters_with_tracks(trk):
    # inside every track, Assign the big cluster number as a final cluster
    major_cluster_no = trk[CLUSTER_NO].value_counts().idxmax()
    trk[CLUSTER_NO] = major_cluster_no
    return trk

def final_emp_id(preds, confids):
    confids = np.array(confids)
    preds = np.array(preds)
    rslt = {}
    confids[preds == 'UNK'] = 0.1    
    for emp_id, confid in zip(preds, confids):
        if emp_id not in rslt.keys():
            rslt[emp_id] = 0
        rslt[emp_id] += confid

    result = list(sorted(rslt.items(), key=lambda kv: kv[1]))
    if len(result) > 1:
        if result[-1][0] == 'UNK': 
            final = result[-2]
        else:
            final = result[-1]
    else:
        final = result[-1]

    if final[1] > 1.0:
        return final
    else:
        return ("UNK", 0.)

    # if result[1]/len(preds) > 0.2 and len(preds) > 10:
    #     return result
    # else:
    #     return ("UNK", 0.)

def set_cluster_id(clst):
    # throw away clusters smaller than COUNT_THRESHOLD
    count = len(clst.index)
    clst_name = clst.name
    status_list = list(clst[STATUS].unique())
    track_list = clst[REF_TRACK_NO].unique()
    # lg.debug("Cluster: ", clst_name, "Count: ", count, "Status: ", status_list, "Tracks: ", track_list)
    # If the Cluster is Smaller than the threshold 
    try:
        if count <= COUNT_THRESHOLD or clst.name == -1:
            clst[CLUSTER_ID] = ""
            status_list = "small"
        elif len(status_list) == 1 and status_list[0] == "Recognized":
            pass
        elif len(status_list) == 1 and status_list[0] == "Un Recognized":
            clst = add_uuid(clst, count)
        
        elif clst[STATUS].nunique() > 1:
            rclst = clst[clst[STATUS]=="Recognized"]
            urclst = clst[clst[STATUS]=="Un Recognized"]
            urclst[CLUSTER_ID] = ""
            clst = pd.concat([rclst, urclst])

            # cluster_ids = clst.loc[clst[STATUS]=="Recognized"][CLUSTER_ID].unique()
            # if len(cluster_ids) == 1:
            #     known_cluster_id = cluster_ids[0]
            #     clst[CLUSTER_ID] = known_cluster_id
                
            # else:
            #     rclst = clst[clst[STATUS]=="Recognized"]
            #     urclst = clst[clst[STATUS]=="Un Recognized"]
            #     urclst = add_uuid(urclst, count)
            #     clst = pd.concat([rclst, urclst])

        else:
            lg.debug("Last Condition: cluster: %s, count: %d, Tracks: %s"%(clst.name, count, track_list))

        if extensive_log:
            if len(track_list) > 1:
                lg.debug("ClstNo: %s Count: %d MergedTrackId: %s status: %s"%
                    (clst.name, count, str(track_list), str(status_list)))
            else:         
                lg.debug("ClstNo: %s Count: %d TrackId: %s status: %s"%
                    (clst.name, count, str(track_list), str(status_list)))


    except Exception as e:
        lg.debug(e)


    


    # # If the cluster is larger than the threshold



    # else:
    #     if clst[STATUS].nunique() == 1 and clst[STATUS].unique()[0] == "Recognized":
    #         lg.debug("Cluster No: %s Count: %d Tracks: %s status: %s"%
    #             (clst.name, count, track_list, str(clst[STATUS].unique())))
        
    #     elif clst[STATUS].nunique() == 1 and clst[STATUS].unique()[0] == "Un Recognized":
    #         # if clst[REF_TRACK_NO].nunique() == 1:
    #         lg.debug("Cluster No: %s Count: %d Tracks: %s status: %s"%
    #             (clst.name, count, track_list, str(clst[STATUS].unique())))
    #         clst = add_uuid(clst, count)

    #     elif clst[STATUS].nunique() > 1:
    #         if "Un Clustered" in clst[STATUS].unique():
    #             lg.warning("cluster: %s, count: %d, Tracks: %s contains Un Clustered samples"%(clst.name, count, track_list))
    #         elif "Recognized" in clst[STATUS].unique():
    #             lg.warning("cluster: %s, count: %d, Tracks: %s  contains Recognised samples"%(clst.name, count, track_list))
    #             known_cluster_id = clst.loc[clst[STATUS]=="Recognized"][CLUSTER_ID].unique()[0]
    #             clst[CLUSTER_ID] = known_cluster_id
    #         else:
    #             lg.warning("cluster: %s, count: %d, Tracks: %s facing a undefined condition"%(clst.name, count, track_list))
    #             clst = add_uuid(clst, count)
    #     else:
    #         lg.warning("cluster: %s, count: %d Track Count: %s facing a undefined condition"%(clst.name, count, track_list))
    #         clst = add_uuid(clst, count)

    # lg.debug(Counter(clst[CLUSTER_ID]))
    return clst

def add_uuid(clst, count):
    clst[CLUSTER_ID] = ""
    cluster_samples = clst[CLUSTER_ID].sample(min(MIN_UNREC_COUNT,count))
    clst[CLUSTER_ID].loc[clst[CLUSTER_ID].index.isin(cluster_samples.index)] = uuid.uuid1()
    return clst


def is_bw(img):
    return len(set(img[0][0])) == 1 and len(set(img[10][10])) == 1

def filter_bw(img):
    return not is_bw(stringToRGB(img))

def stringToRGB(base64_string):
    nparr = np.fromstring(base64.b64decode(base64_string), np.uint8)
    img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
    return img

def log_each_track(trk_df):
    """Logs Track based data. Cam_id, Track No, Status, Total Images, Valid Images and Invalid Images"""
    img_count = trk_df.shape[0]
    valid_count = trk_df[trk_df['valid'] == True].shape[0]
    invalid_count = img_count - valid_count
    track_status = trk_df[STATUS].unique()[0]
    cam_id = trk_df[CAMERA_ID].unique()[0]
    lg.debug("Cam id: %s, TrkId: %s, Status: %s, Total Images: %d, Valid: %d, Invalid: %d"%
        (cam_id, trk_df.name, track_status, img_count, valid_count, invalid_count))

def log_recieved_data(df):
    """Logs all the Total Cams, Tracks, Total Images, Recognized Tracks, Unrecognised Tracks, Recognised People"""
    total_tracks = df[REF_TRACK_NO].nunique()
    total_images = df.shape[0]
    recognised_tracks = df.loc[df[STATUS] == "Recognized"][REF_TRACK_NO].nunique()
    unrecognised_tracks = df.loc[df[STATUS] == "Un Recognized"][REF_TRACK_NO].nunique()
    total_cameras = df[CAMERA_ID].unique()
    unique_recognitions = df[CLUSTER_ID].nunique()

    lg.debug("Cam(s): %s, TrkId: %d, Recognized Tracks: %d, UnRecognized Tracks: %d, Recognized People: %d"%
        (str(total_cameras), total_tracks, recognised_tracks, unrecognised_tracks, unique_recognitions))

def log_img_count(df):
    total_count = df.shape[0]
    valid_count = df.loc[df[VALID]==True].shape[0]
    invlid_count = df.loc[df[VALID]==False].shape[0]
    cam_id = df[CAMERA_ID].unique()[0]
    lg.debug("Cam(s): %s, Total Face crops: %d, Valid: %d, Invalid: %d"%
        (cam_id, total_count, valid_count, invlid_count))
    
    lg.debug("Cam(s): %s, Total Face crops: %d, Recognised: %d, Unrecognised: %d"%
        (cam_id, total_count, df[df[STATUS]=="Recognized"].shape[0], df[df[STATUS]=="Un Recognized"].shape[0]))
    

def log_cluster_wise_analysis(clst, caption="Before"):
    track_ids = Counter(clst[REF_TRACK_NO].tolist())
    lg.debug("%s:: Cluster: %s, TrkId: %s"%(caption, str(clst.name), str(track_ids.most_common())))

def log_track_wise_analysis(trk, caption="Before"):
    clst_nos = Counter(trk[CLUSTER_NO].tolist())
    lg.debug("%s:: TrkId: %s, Cluster: %s"%(caption, str(trk.name), str(clst_nos.most_common())))

def cluster_tracks(
        data_as_json,
        img=False,
        cluster_debug_output_path="/data/work/face-rec/face-rec/data/clusteringTest/outputs/17_2_1/"
):
    bf_timestamp = time.time()
    df = pd.DataFrame(data_as_json)
    lg.debug("Total Recieved data")
    log_recieved_data(df)

    df[EMBEDED] = df[EMBEDED].apply(lambda r: np.array(ast.literal_eval(r)))

    df[VALID] = df[EMBEDED].apply(lambda r: r.shape[0] > 1)

    log_img_count(df)
    # df.groupby(REF_TRACK_NO).apply(log_each_track)
            
    df['confidence'] = df['confidence'].apply(lambda r: float(r))

    def perform_clustering(grp, img):
        cam_id = grp.name
        lg.debug("Cam: %s perform_clustering_called"%cam_id)
        
        # Perform Recieved Data Logging
        log_recieved_data(grp)
        log_img_count(grp)
        if extensive_log:
            grp.groupby(REF_TRACK_NO).apply(log_each_track)
        
        if img:
            vdf = grp[grp['valid']][[EMBEDED, TRACK_DATA_ID, REF_TRACK_NO, REF_IMAGE, CAMERA_ID, CLUSTER_ID, EMP_ID, CONFIDENCE, STATUS]]
            ivdf = grp[~grp['valid']][[EMBEDED, TRACK_DATA_ID, REF_TRACK_NO, REF_IMAGE, CAMERA_ID, CLUSTER_ID, EMP_ID, CONFIDENCE, STATUS]]
        else:
            vdf = grp[grp['valid']][[EMBEDED, TRACK_DATA_ID, REF_TRACK_NO, CAMERA_ID, CLUSTER_ID, EMP_ID, CONFIDENCE, STATUS]]
            ivdf = grp[~grp['valid']][[EMBEDED, TRACK_DATA_ID, REF_TRACK_NO, CAMERA_ID, CLUSTER_ID, EMP_ID, CONFIDENCE, STATUS]]

        #Assign Invalid ClusterID tag to all Unrecognised Tracks
        ivdf[CLUSTER_ID] = INVALID_CLUSTERID
        # vdf.loc[vdf[CLUSTER_ID].isnull(), CLUSTER_ID] = INVALID_CLUSTERID

        tracks_data= get_tack_list(vdf, ivdf, grp)
        if extensive_log:
            try:
                lg.debug("TrkIds: %s"%str(tracks_data))
            except Exception as e:
                print(e)

        if img:
            vdf = vdf[vdf[REF_IMAGE].apply(filter_bw)]

        # Prepare data for clsutering
        refined_embeddings = vdf[EMBEDED].tolist()
        refined_embeddings = np.array(refined_embeddings).reshape(len(refined_embeddings), 128)
        uniqueids = vdf[TRACK_DATA_ID].tolist()

        try:
            lg.debug("Cam ID :  %s Clsutering Size Embeddings Count: %d, Employee Ids Count: %d"%(cam_id, len(refined_embeddings), len(uniqueids)))
        except Exception as e:
            lg.debug(e)

        # Perform clustering
        try:
            cluster_ids, uids = do_clustering(refined_embeddings, uniqueids, cluster_threshold=CLUSTER_THRESHOLD,
                                              cl_type=CLUSTERING_ALG)
            try:
                lg.debug("Cam ID :  %s Stage one clustering: silhouette_score: %s" %(cam_id, silhouette_score(refined_embeddings, cluster_ids)))
            except Exception as e:
                lg.debug(e)

            cluster_ids = process_big_clusters(cluster_ids, uids, refined_embeddings, cam_id)

            # try:
            #     lg.debug("Stage two clusterings: silhouette_score: %s" % silhouette_score(refined_embeddings, cluster_ids))
            # except Exception as e:
            #     lg.debug(e)

            # Update vdf with new cluster numbers
            vdf[CLUSTER_NO] = cluster_ids

            unique_tracks = vdf[REF_TRACK_NO].nunique()
            unique_clusters = vdf[CLUSTER_NO].nunique()

            lg.debug("Cam ID :%s BEFORE CLUSTER RESOLVE: Processed TrkId:%d  Generated Clusters:%d"%(cam_id, unique_tracks, unique_clusters))
            # vdf.groupby(REF_TRACK_NO).apply(log_track_wise_analysis, caption="BEFORE CLUSTER RESOLVE")

            if img:
                vdf.groupby(REF_TRACK_NO).apply(save_track_images, (os.path.join(cluster_debug_output_path, cam_id)))
            
            vdf = vdf.groupby(REF_TRACK_NO).apply(resolve_clusters_with_tracks)

            new_clsuter_count = vdf[CLUSTER_NO].nunique()
            lg.debug("Cam ID: %s Merged TrkId: %d "%(cam_id, unique_tracks-new_clsuter_count))
            lg.debug("Cam ID :  %s AFTER CLUSTER RESOLVE: Fianl Clusters: %d Removed Clsuters: %d"%
                (cam_id, new_clsuter_count, unique_clusters-new_clsuter_count))
            # vdf.groupby(REF_TRACK_NO).apply(log_track_wise_analysis, caption="AFTER CLUSTER RESOLVE")

            # vdf.groupby(CLUSTER_NO).apply(log_cluster_wise_analysis, caption="AFTER CLUSTER RESOLVE")
            vdf = vdf.groupby(CLUSTER_NO).apply(set_cluster_id)

            # set_cluster_id_count = vdf[vdf[CLUSTER_ID] != ""][CLUSTER_ID].nunique()
            try:
                try:
                    unrec_vdf = vdf[vdf[STATUS]== 'Un Recognized']
                    small_count = (unrec_vdf[CLUSTER_NO].value_counts() <= COUNT_THRESHOLD).value_counts()[True]
                except:
                    small_count = 0
                unrec_cluster_count = unrec_vdf[CLUSTER_NO].nunique()-small_count
                rec_count = vdf[vdf[STATUS]== 'Recognized'][CLUSTER_NO].nunique()
                lg.debug("Cam ID :  %s Unrec clusters :%d Rec clusters :%d, Small clucters: %d"%
                    (cam_id, unrec_cluster_count, rec_count, small_count))
            except Exception as e:
                lg.debug("Exception at small clusters", e)

            if img:
                vdf.groupby(CLUSTER_ID).apply(save_images, (os.path.join(cluster_debug_output_path, cam_id)))

        except Exception as e:
            lg.warning(e)

        return pd.concat([vdf, ivdf], sort=True) 

    def get_tack_list(vdf, ivdf, grp):
        """Lists all the tracks that are Valid, Invalid, Pratially Valid, Recognised and Recognised"""
        total = grp[REF_TRACK_NO].nunique()
        vdf_track_list = vdf[REF_TRACK_NO].unique()
        ivdf_track_list = ivdf[REF_TRACK_NO].unique()

        # invalid_tracks = list(set(ivdf_track_list)- set(vdf_track_list))
        # partial_tracks = list(set(ivdf_track_list) - set(invalid_tracks))
        # valid_tracks = list(set(vdf_track_list)- set(ivdf_track_list))
        tracks_data = {}
        tracks_data['valid'] = list(set(vdf_track_list)- set(ivdf_track_list))
        tracks_data['invalid'] = list(set(ivdf_track_list)- set(vdf_track_list))
        tracks_data['partial'] = list(set(ivdf_track_list) & set(vdf_track_list))
        tracks_data['rec'] = list(grp.loc[grp[STATUS] == "Recognized"][REF_TRACK_NO].unique())
        tracks_data['unrec'] = list(grp.loc[grp[STATUS] == "Un Recognized"][REF_TRACK_NO].unique())
        try:
            lg.debug("Tracks Count: %d, Valid: %d, Invlid: %d, Partial: %d, Recgonised: %d, Unrecognised: %d"%
                    (total, len(tracks_data['valid']), len(tracks_data['invalid']), len(tracks_data['partial']),
                    len(tracks_data['rec']), len(tracks_data['unrec'])))
        except Exception as e:
            lg.debug(e)

        return tracks_data


    # df = df[(df['updated_by'].notnull()) & (df['updated_by'] != 'SYSTEM')]

    # print("len dfindex", len(df.index))
    if len(df.index) > 0:
        df = df.groupby(CAMERA_ID).apply(perform_clustering, img=img)
    else:
        lg.warning("Nothing to process")

    af_timestamp = time.time()
    lg.debug("TimeTaken: %0.3f" % (af_timestamp - bf_timestamp))
    return df

def get_trkdatids(df, each_cam, each_cluster_id):
    cam_df = df[df[CAMERA_ID]==each_cam]
    clsdf = cam_df[cam_df[CLUSTER_ID]==each_cluster_id]    
    trkdatids = clsdf[clsdf[STATUS]=='Un Recognized'][TRACK_DATA_ID].tolist()
    
    return trkdatids


def df_to_json(df, empid_clsid_list):
    lg.debug("json preparation initiated")
    js = []
    # emp_list = ['7db8d736-8d15-40f2-886e-4961688665c7', '0962e544-1e47-11ea-b610-cd5239d9129b', 
    #             '60dcdc98-f8c1-4cf8-85ad-022e250201a2','0dce41c3-ca9d-4afe-8312-777fea7511fe']
    cluster_id_list = []
    employee_id_list = []
    emp_cluster_ids_map_exists = False

    try:
        cluster_id_list = empid_clsid_list['cluster_ids']
        employee_id_list = empid_clsid_list['emp_ids']
        if len(cluster_id_list) == len(employee_id_list):
            emp_cluster_ids_map_exists = True
            # lg.debug("Emp and Cluster ids are exist")
        else:
            lg.debug("Emp and Cluster ids don't exist")
    except Exception as e:
        lg.debug(e)
        lg.debug("Emp and Cluster ids don't exist")

    unique_cams = df[CAMERA_ID].unique().tolist()
    zz, clst_cnt = 0, 0 
    for each_cam in unique_cams:        
        cluster_ids = df[(df[CAMERA_ID] == each_cam)][CLUSTER_ID].unique().tolist()
        for each_cluster in cluster_ids:
            zz  += 1
            # print(each_cluster)
            confirm_emp_id = None
            cluster_nos = 0
            trackdata_ids = get_trkdatids(df, each_cam, each_cluster)
            # cluster_nos = df[df[CLUSTER_ID] == each_cluster][CLUSTER_NO].unique()[0]
            # print("Len of old trk list: ", len(trackdata_ids), "new list: ", len(new_trackids))
            if len(trackdata_ids) > 0:
                clst_cnt += 1
                if len(str(each_cluster)) == 0:
                    each_cluster = UNCLUSTERED
                    FINAL_EMP_ID = UNKOWN

                elif each_cluster in cluster_id_list:
                    FINAL_EMP_ID = UNKOWN #employee_id_list[cluster_id_list==str(each_cluster)]
                    each_cluster =  UNCLUSTERED

                else:
                    try:
                        cluster_nos = df[df[CLUSTER_ID] == each_cluster][CLUSTER_NO].unique()[0]
                        preds = df[df[CLUSTER_NO]==cluster_nos][EMP_ID].tolist()
                        confids = df[df[CLUSTER_NO]==cluster_nos][CONFIDENCE].tolist()
                        emp_id, confid = final_emp_id(preds, confids)

                        if emp_id != 'UNK':
                            confirm_emp_id = emp_id

                    except Exception as e:
                        lg.debug(e)
                    
                    FINAL_EMP_ID = UNKOWN

                cluster_obj = {
                    JS_KEY_CLUSTER_ID: str(each_cluster),
                    JS_KEY_TRACK_DATA_ID: trackdata_ids,
                    JS_KEY_CAMERA_ID: each_cam,
                    JS_KEY_EMP_ID: FINAL_EMP_ID,
                    JS_KEY_CONFIRM_EMP_ID: confirm_emp_id
                }

                lg.debug("Sno: %d, Cam ID: %s, clsuter_no: %s, FINAL EMP_ID: %s, Confirm EMP_id: %s"%
                    (zz, each_cam, str(cluster_nos), FINAL_EMP_ID, confirm_emp_id))

                js.append(cluster_obj)
        # lg.debug(js)
    lg.debug("Final: Total Unique Clusters: %d, Unrec cluster count: %d"
        %(zz, clst_cnt))
    lg.debug("Returned Trackdataids: %d"%(sum([len(obj[JS_KEY_TRACK_DATA_ID]) for obj in js])))
    lg.debug("Total Trackdataids: %d"%(df[df[STATUS]=="Un Recognized"].shape[0]))
    return js


class ClusteringService(KaiProcess):

    def __init__(self):
        self.config = AppConfig("config.json")

        super().__init__(
             health_status_port=self.config.management.clustering_service_health_status_port
        )

    def unique_name(self) -> str:
        return "ClusteringService"

    def monitored_status_params(self) -> dict:
        return {}

    def run(self):
        super().run()
        self.fr = FaceRecogniser(app_config)
        app = Flask(__name__)

        @app.route('/cluster', methods=["POST"])
        def cluster_with_tracks():
            data_as_json = request.json
            try:
                empid_clsid_list = backend_api.get_employee_id_cluster_id()
                lg.debug("empid_clsid_list exist")
            except Exception as e:
                lg.debug("empid_clsid_list not exist")
                empid_clsid_list = {}
                lg.debug(e)

            # try:
            #     lg.debug("Json file saved")
            #     with open("data_file9.json", "w") as write_file:
            #         json.dump(data_as_json, write_file)
            # except Exception as e:
            #     print(e)

            df= cluster_tracks(data_as_json)
            tracks_data = {}
            response_json = df_to_json(df, empid_clsid_list)
            # print(response_json)
            return Response(json.dumps(response_json), headers={'Content-type': "application/json"})

        
        @app.route('/matching', methods=["POST"])
        # @app.route('/matching')
        def reg_matching():
            # path = "/media/ahamad/work/projects/tvs/face-rec/face-data/matching-smapleip/Input.txt"
            lg.debug("Matching Process Initiated")
            a = time.time()
            data = request.json
            # data = load_data(path)

            lg.debug("Registration Matching Initiated")
            match_list = registration_matching(data, self.fr)

            lg.debug("Response Generation Initiated")
            response_json = get_response_json(match_list)
            lg.debug("Registration task completed %0.4f sec"%(time.time() - a))
            # print(response_json)
            return Response(json.dumps(response_json), headers={'Content-type': "application/json"})

        self.run_in_and_wait_for_subthread(app.run, host="0.0.0.0", debug=False)

        
        

if __name__ == "__main__":
    from vap.modules import logging

    lg = logging.get_root_logger(log_name="ayeAccess.FR")
    cs = ClusteringService()
    run_and_wait(cs)

    if 0:
        # with open("clustering-20200313-150747.json") as jsdata:
        cluster_input_path = "data_file17_X.json"
        emp_clst_path = "emp_clst_1.json"
        cluster_debug_output_path = "/data/work/face-rec/face-data/clustering_input_data/outputs/17_2_4/"

        with open(emp_clst_path) as jsdata:
            empid_clsid_list = json.load(jsdata)
        
        with open(cluster_input_path) as jsdata:
            data_as_json = json.load(jsdata)
            # data_as_json = json.dumps(next(iter(data_as_json.values())))
        # data_as_json = data_as_json[list(data_as_json.keys())[0]]
        df = cluster_tracks(data_as_json, cluster_debug_output_path = cluster_debug_output_path)
        # df.to_pickle("savedf7.pkl")
        tracks_data = {}
        response_json = df_to_json(df, empid_clsid_list)

        print("Completed Sucessfully")

    # if 0:
    #     df = pd.read_pickle("savedf.pkl")
    #     response_json = df_to_json(df)
    #     print(response_json)
    # else:
    #     pass
        # cluster()

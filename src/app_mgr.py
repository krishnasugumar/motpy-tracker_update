import multiprocessing as mp
import time

import vap
from face_clustering_opt_service import ClusteringService
from face_recognition_core_service import VisionService
from face_trainer_opt_service import TrainingService
from vap.framework.kai_process import run_and_wait, KaiProcess
from vap.modules import logging

lg = logging.get_root_logger(log_name="ayeAccess.FR")


class AppManager(KaiProcess):

    def __init__(self):
        self.config = vap.config.AppConfig("config.json")
        super().__init__(
            health_status_port=self.config.management.manager_health_status_port,
            force_terminate_on_stop=True,
            stop_subprocs_on_stop=True,
            join_subprocs_on_sigstop=True
        )

    def unique_name(self) -> str:
        return "FaceRecognitionVisionServiceManager"

    def monitored_status_params(self) -> dict:
        return {}

    def run(self):

        event_training_start = mp.Event()
        event_training_finished = mp.Event()

        vision_service = VisionService()
        clusering_service = ClusteringService()
        trainer_service = TrainingService(event_training_start, event_training_finished)

        self.subproc_handles = [vision_service, clusering_service, trainer_service]
        self.run_subprocs()

        super().run()

        try:
            while not self.shutdown_event.is_set():
                try:
                    while not event_training_start.wait(timeout=0.1):
                        if self.shutdown_event.is_set():
                            raise KeyboardInterrupt

                    lg.info("Stopping Main Application")
                    vision_service.stop()
                    vision_service.join()

                    while not event_training_finished.wait(timeout=0.1):
                        if self.shutdown_event.is_set():
                            raise KeyboardInterrupt

                    lg.info("Resuming Main Application")
                    vision_service = VisionService()
                    vision_service.start()

                except KeyboardInterrupt:
                    # need to catch signal
                    break
        except Exception as e:
            lg.error(e)

        lg.info("Exiting AppMgr")


if __name__ == "__main__":
    app = AppManager()
    run_and_wait(app)
    print("exited app_mgr")

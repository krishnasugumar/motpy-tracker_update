import sys
sys.path.append("/data/work/face-rec/face-rec/src/")

import psycopg2
import uuid 
import random
from datetime import datetime
from glob import glob 
import os
from classes.face_recogniser import FaceRecogniser
from vap.config import AppConfig
import cv2 
import numpy as np 
import base64
import time
from tqdm import tqdm 

NULL="null"

def uuid_or_def(defval):
    return str(uuid.uuid1()) if defval is NULL else defval 

def time_or_def(defval):
    return datetime.now() if defval is NULL else defval

class DBHandler(object):

    def __init__(self):
        self.con = psycopg2.connect(database="testdb", user="kamerai", password="kamerai", host="127.0.0.1", port="5432")
        print("Database opened successfully")

    def querydb(self, query):
        cur = self.con.cursor()
        cur.execute(query)
        return cur.fetchall()

    def commit(self, query):
        # print(query)
        # return None 

        cur = self.con.cursor()
        cur.execute(query)
        return self.con.commit()

    def fetch_from(self, table, columns):
        query = "select %s from %s"%(",".join(columns), table)
        return self.querydb(query) 

    def fetch_unique_from(self, table, column):
        query = "select distinct(%s) from %s"%(column, table)
        return self.querydb(query)

    def insert_into(self, table, headers, rows):
        rowsq =   ",\n".join(["(%s)"%(",".join(['\'%s\''%str(col) for col in row])) for row in rows]  ) 
        #rowsq = ",\n".join(["(%s"%(",".join(col)) for row in rows for col in row ])
        query = "insert into {table} ({headers}) values {rows}".format(
            table=table,
            headers="{headers}".format(headers=",".join(headers)),
            rows=rowsq 
        )
        print(self.commit(query))

    def insert_single_into(self, table, headers, row):
        query = "insert into {table} ({headers}) values ({row})".format(
            table=table,
            headers="{h}".format(h=",".join(headers)),
            row=row 
        )
        self.commit(query)

    def insert_multi_into(self, table, headers, rowsstr):
        query = "insert into {table} ({headers}) values {rowsstr}".format(
            table=table,
            headers="{h}".format(h=",".join(headers)),
            rowsstr=rowsstr
        )
        self.commit(query)

    def insert_obj(self, table, obj):
        self.insert_single_into(table, obj.headers(), str(obj))

    def insert_multi_obj(self, table, multiobj):
        self.insert_multi_into(table, multiobj.headers(), str(multiobj))




class TrackData(object):

    def __init__(
        self,
        embeded,
        ref_image,
        ref_track_n,        
        trackdata_id=NULL,
        camera_id="0",
        cluster_id=NULL,
        created_by=NULL,
        created_date=NULL,
        status="Recognized",
        trackuniqid=NULL,
        updated_by="Admin",
        updated_date=NULL,
        verify_id=NULL 
    ):
        self.trackdata_id=uuid.uuid1() if trackdata_id is NULL else trackdata_id 
        self.camera_id=camera_id
        self.cluster_id=uuid_or_def(cluster_id)
        self.created_by="Admin"
        self.created_date=time_or_def(created_date)
        self.embeded=embeded
        self.ref_image=ref_image
        self.ref_track_no=ref_track_n
        self.status=status
        self.trackuniqid=uuid_or_def(trackuniqid)
        self.updated_by=updated_by
        self.updated_date=time_or_def(updated_date)
        self.verify_id=verify_id

    def __repr__(self):
        return ",".join([
            "'%s'"%self.trackdata_id,
            "'%s'"%self.camera_id,
            "'%s'"%self.cluster_id,
            "'%s'"%self.created_by,
            "'%s'"%self.created_date,
            "'%s'"%self.embeded,
            "'%s'"%self.ref_image,
            "'%s'"%self.ref_track_no,
            "'%s'"%self.status,
            "'%s'"%self.trackuniqid,
            "'%s'"%self.updated_by,
            "'%s'"%self.updated_date,
            "'%s'"%self.verify_id
        ])

    def headers(self):
        return [
            "trackdata_id",
            "camera_id",
            "cluster_id",
            "created_by",
            "created_date",
            "embeded",
            "ref_image",
            "ref_track_no",
            "status",
            "trackuniqid",
            "updated_by",
            "updated_date",
            "verify_id"
        ]

class Track(object):

    def __init__(
        self,
        imgList,
        embedList,
        cameraId,
        trackNo,
        verificationId
    ):

        self.TrackDatas = []

        ref_track_n=trackNo        
        camera_id=cameraId
        created_by="SYSTEM"
        status="Recognized"
        trackuniqid=str(uuid.uuid1())
        updated_by="Admin"
        verify_id=NULL 
        cluster_id=str(uuid.uuid1())
        created_date=datetime.now(),
        updated_date=datetime.now(),
        verify_id=verificationId 

        for img,emb in zip(imgList, embedList):
            img64 = base64.b64encode(cv2.imencode('.jpg', cv2.imread(img))[1].tostring()).decode()
            self.TrackDatas.append(TrackData(
            camera_id=camera_id,
            cluster_id=cluster_id,
            created_by=created_by,
            created_date=created_date,
            embeded=emb,
            ref_image=img64,
            ref_track_n=ref_track_n,
            status=status,
            trackuniqid=trackuniqid,
            updated_by=updated_by,
            updated_date=updated_date,
            verify_id=verify_id
            ))

    def headers(self):
        return self.TrackDatas[0].headers()

    def __repr__(self):
        return ",".join([
            "(%s)"%trackdata for trackdata in self.TrackDatas
        ])


class EmployeeInfo(object):

    def __init__(
        self,
        first_name,
        last_name,        
        full_name=NULL,
        emp_id=NULL,
        contact_number="123",
        created_by="Admin",
        created_date=NULL,
        emergency_contact_number="123",
        ttype="Casual",
        unique_identification=NULL,
        updated_by="SYSTEM",
        updated_date=NULL
    ):
        self.emp_id=str(uuid.uuid1()) if emp_id is NULL else emp_id
        self.contact_number=contact_number
        self.created_by=created_by
        self.created_date=datetime.now() if created_date is NULL else created_date
        self.emergency_contact_number=emergency_contact_number
        self.first_name=first_name
        self.full_name="%s %s"%(first_name, last_name) if full_name is NULL else full_name
        self.last_name=last_name
        self.type=ttype
        self.unique_identification=str(uuid.uuid1()) if unique_identification is NULL else unique_identification
        self.updated_by=updated_by
        self.updated_date=datetime.now() if updated_date is NULL else updated_date

    def __repr__(self):
        return ",".join([
            "'%s'"%self.emp_id,
            "'%s'"%self.contact_number,
            "'%s'"%self.created_by,
            "'%s'"%self.created_date,
            "'%s'"%self.emergency_contact_number,
            "'%s'"%self.first_name,
            "'%s'"%self.full_name,
            "'%s'"%self.last_name,
            "'%s'"%self.type,
            "'%s'"%self.unique_identification,
            "'%s'"%self.updated_by,
            "'%s'"%self.updated_date
        ])
        
    def headers(self):
        return [
            "emp_id",
            "contact_number",
            "created_by",
            "created_date",
            "emergency_contact_number",
            "first_name",
            "full_name",
            "last_name",
            "type",
            "unique_identification",
            "updated_by",
            "updated_date"
        ]

class VerificationData(object):

    def __init__(
        self,
        verify_id,
        verification_status,
        verified_date,
        verifiedby,
        version_num,
        emp_id
        ):

        self.verify_id=verify_id
        self.verification_status=verification_status
        self.verified_date=verified_date
        self.verifiedby=verifiedby
        self.version_num=version_num
        self.emp_id=emp_id

    def __repr__(self):
        return ",".join([
            "'%s'"%self.verify_id,
            "'%s'"%self.verification_status,
            "'%s'"%self.verified_date,
            "'%s'"%self.verifiedby,
            "'%s'"%self.version_num,
            "'%s'"%self.emp_id
        ])

    def headers(self):
        return [
            "verify_id",
            "verification_status",
            "verified_date",
            "verifiedby",
            "version_num",
            "emp_id"
        ]


        
if __name__ == "__main__":
    pgdb = DBHandler()
    
    track_data_table = "track_data"
    emp_info_table = "employee_info"
    emp_verification_table = "employee_verification"

    def db_create_new_employee(firstname, lastname, unique_identification):
        new_emp = EmployeeInfo(first_name=firstname, last_name=lastname, unique_identification=unique_identification)
        pgdb.insert_obj(emp_info, new_emp)

    def scan_for_people(scan_path):
        people = glob(scan_path+"/*/")
        return people 

       
    
    scan_path = "/data/work/face-rec/face-rec/data/hosurVisit2/All200/rearranged"
    print("Scanning for people in %s"%scan_path)
    people = scan_for_people(scan_path)
    fr = FaceRecogniser(AppConfig("config.json"))

    start_all = time.time()

    for idx,person in tqdm(enumerate(people), total=len(people)):
        start_person = time.time()
        person = person[:-1]
        fullname = os.path.basename(person)
        
        firstname = fullname.split('_')[0]
        lastname = fullname.split('_')[-1]


        imglist = sorted(glob(person+"/*.jpg"))
        print("Processing %s %s with %d images"%(firstname, lastname, len(imglist)))

        emblist = []
        for imgpath in imglist:
            img = cv2.imread(imgpath)
            emb = np.array(fr.get_embedding(img)).tolist()
            emblist.append(emb)
        

        camera_id="0"
        track_no =str(idx) 
        created_date = datetime.now()
        verified_date = datetime.now()

        #first insert employee in employee_info
        emp_id = str(uuid.uuid1())
        verify_id = str(uuid.uuid1())

        employeeInfoObj = EmployeeInfo(
            first_name=firstname,
            last_name=lastname,
            full_name="%s %s"%(firstname, lastname),
            emp_id=emp_id,
            contact_number="123",
            created_by="Admin",
            created_date=created_date,
            emergency_contact_number="123",
            ttype="Casual" if "scs" not in firstname else "Permanent",
            unique_identification=str(idx),
            updated_by="Admin",
            updated_date=created_date
        )

        trackInfoObj = Track(        
            imgList=imglist, 
            embedList=emblist,
            cameraId=camera_id,
            trackNo=track_no,
            verificationId=verify_id
        )

        verificationInfoObj = VerificationData(
            verify_id=verify_id,
            verification_status="true",
            verified_date=verified_date,
            verifiedby="Admin",
            version_num="0",
            emp_id=emp_id 
        )
        
        print("inserting employee_info table")
        pgdb.insert_obj(emp_info_table, employeeInfoObj)

        print("inserting employee_verification table")
        pgdb.insert_obj(emp_verification_table, verificationInfoObj)

        print("inserting track_data table")
        pgdb.insert_multi_obj(track_data_table, trackInfoObj)

        end_person = time.time()
        print("Time taken: %0.2f seconds"%(end_person-start_person))

    end_all = time.time()
    print("Total Time Taken for %d People :: %0.2f seconds"%(len(people), end_all-start_all))
         
        # print(emblist)

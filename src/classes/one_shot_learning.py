import numpy as np
import logging
import time
from collections import Counter

import numpy as np

from classes import utils
from clf_utils.classifier_visualization import get_metrics
from clf_utils.dataset_utils import preprocess_dataset
from clf_utils.save_load_data import load_oneshot

lg = logging.getLogger(__name__)
COUNT_THRESHOLD = 30


# DISTANCE_THRESHOLD = config.recognition.training.oneshot.distance_threshold
# DATASET_PATH = "/data/work/face-rec/face-rec/src/poc_3cam_test/dataset_ppk.pickle"
# DATASET_PATH = "/data/work/face-rec/face-rec/src/db/images/traindataset-20191128-172515.pickle"
# X, y, path_ = load_data(path=DATASET_PATH)

def get_oneshot_training_data(file_path):
    X, y = load_oneshot(file_path)
    # with open(file_path, 'rb') as f:
    #     X, y = pickle.load(f)    
    # X_train, X_test, y_train, y_test = preprocess_dataset(X, y, count_threshold=COUNT_THRESHOLD, test_size=0)
    X_train = np.array(X_train).reshape(len(X_train), 128)
    y_train = np.array(y_train).reshape(len(y_train), 1)
    return (X_train, y_train, X_test, y_test)


class OneShotRecognition(object):
    def __init__(self, config, dist_threshold=0.38):

        train_data_file_path = config.recognition.training.oneshot.db_path
        # self.train_data_file_path = train_data_file_path # check_file(train_data_file_path)
        self.train_data_file_path = utils.most_recent_file(config.recognition.training.oneshot.db_path, ".pickle")
        if self.train_data_file_path is not None:
            X, y = load_oneshot(self.train_data_file_path)
            self.X_train = np.array(X).reshape(len(X), 128)  # X
            self.y_train = np.array(y).reshape(len(y), 1)  # y
            self.X_test = None
            self.y_test = None
        else:
            pass
            # raise Warning('Training file does not exist: %s'%config.recognition.training.oneshot.db_path)

        self.dist_threshold = config.recognition.training.oneshot.dist_threshold
        lg.debug("OneShotRecognition distance threshold %f" % self.dist_threshold)
        self.config = config

    def predict(self, test):
        # param: test is a list of 128 dimentional embeddings
        if self.train_data_file_path is None:
            return "UNK", 0.0
        dists = [np.linalg.norm(self.X_train - embd, axis=1) for embd in test]
        names = [self.y_train[dist1 < self.dist_threshold] for dist1 in dists]
        names_list = [item for sublist in names for item in sublist]
        if len(names_list) > 0:
            names_list = [item[0] for item in names_list]
            final_name, score = Counter(names_list).most_common()[0]
            strng_name = final_name + " - " + str(score)
            lg.debug("One shot recognition %s" % strng_name)
            # lg.debug(final_name)
            # lg.debug(score)
            return final_name, score / ((self.config.recognition.training.count_threshold /1.5) * len(test))
        else:
            lg.debug("One shot recognition UNK - 0.0")
            return "UNK", 0.0

    def result_analysis(self):

        result = {}
        time_list = []
        y_test1 = np.array(self.y_test).reshape(len(self.y_test), 1)
        y_pred = []
        for i in range(len(self.X_test)):
            ground_truth = self.y_test[i:i + 1][0]
            a = time.time()
            pred_name, pred_confidence = self.predict(self.X_test[i:i + 1])
            y_pred.append(pred_name)
            time_list.append(time.time() - a)

            if pred_name is not None:
                result.setdefault(ground_truth, []).append((pred_name, pred_confidence))
            else:
                result.setdefault(ground_truth, []).append(('UNK', None))

        print(
            "Average Time to process %s images of 40 at a time %s" % (len(time_list), sum(time_list) / len(time_list)))
        y_pred = np.array(y_pred).reshape(len(y_pred), 1)
        # y_pred = y_pred.tolist()
        result_metrics = get_metrics(self.y_test, y_pred)
        return result_metrics


def main():
    X_train, X_test, y_train, y_test = preprocess_dataset(X, y, count_threshold=COUNT_THRESHOLD)
    recognizer = OneShotRecognition(train_data_file_path=DATASET_PATH)
    result = recognizer.result_analysis()

    for i in range(len(X_test)):
        name, count = recognizer.predict(X[i:i + 2])
        if name is not None:
            print(name, count, y[i:i + 2])


if __name__ == '__main__':
    main()

# from logging_init import lg
import queue
import threading
import time
import traceback
import pandas as pd

from classes.backend_api_rest import BackendAPI as BackendAPIRest
from classes.dtypes import *
from classes.face_recogniser import UNKNOWN, UNCLEAR, FaceType
from classes.sliceable_deque import sliceable_deque
from vap.framework.kai_process import KaiProcess
from vap.modules import logging

lg = logging.getLogger(__name__)

EMP_VAR_LIMIT = 5

class RecognitionResultHandlerProcess(KaiProcess):

    def __init__(self, app_config, in_q, track_state_update_q, save_q, recognition_state_update_qs):
        super().__init__(force_terminate_on_stop=True)
        self.in_q = in_q
        self.save_q = save_q
        self.track_state_update_q = track_state_update_q
        self.track_state_table = {}
        self.app_config = app_config

        self.RECOGNITION_MAX_BATCHES = app_config.recognition.batches_to_process_per_track
        self.RECOGNITION_IGNORE_EMP_IDS = app_config.recognition.ignore_emp_ids
        self.recognition_state_update_qs = recognition_state_update_qs

        self.q_handles = [self.in_q, self.save_q, self.track_state_update_q] + [q for q in
                                                                                self.recognition_state_update_qs]

    def unique_name(self):
        return "RecognitionResultHandler"

    def monitored_status_params(self):
        return {
            "save_q": self.save_q.qsize(),
            "recognized_crops_q": self.in_q.qsize(),
        }

    def track_state_monitor(self):
        while not self.shutdown_event.is_set():
            try:
                state_event = self.track_state_update_q.get_nowait()
                track_id = state_event.track_id
                event = state_event.event
                if track_id not in self.track_state_table.keys():
                    self.register_new_track(track_id)

                if event == TRACK_DELETED:
                    lg.debug("TrkId-%d Deleted from Feed" % track_id)
                    self.track_state_table[track_id]["deleted"] = True
                elif event == TRACK_QUEUED:
                    # lg.debug("TrkId-%d %d == %d" % (track_id, self.lookup_table[track_id]["slots_queued"],
                    #                                 self.lookup_table[track_id]["slots_processed"]))
                    self.track_state_table[track_id]["slots_queued"] += 1
                    self.track_state_table[track_id]["camId"] = state_event.camId
            except queue.Empty:
                time.sleep(0.1)
            except Exception as e:
                lg.error(e)

        lg.info("Exiting Track State Monitor")

    def is_invalid_face_crop(self, emp_id):
        return emp_id == UNCLEAR or emp_id == FaceType.NoFace.name or emp_id == FaceType.SideFace.name

    def weighted_recognition_result(self, results_list, track_id, log=True):
        consecutive_wt = 1.2
        sparse_wt = 1.
        unk_wt = 0.05
        conf_threshold = 0.40  # 0.5  # 0.8/100
        total_conf_threshold = 6.5  # 1.1  # 2.5/100
        emp_freq_T = 0.67
        unk_freq_T = 0.7
        possible_empids_wts = {}
        prev_emp_id = ""
        org_length = len(results_list)
        lg.debug("TrkId-%d Length-%d"%(track_id, org_length))
        # lg.debug(results_list)
        # for item in results_list:
        #     print(item, len(item[0]))
        results_list = [item for item in results_list if len(item[0])>= 10 or len(item[0])<4]
        side_faces_len = org_length-len(results_list)
        lg.debug("TrkId-%d Length-%d Side-%d Useful-%d"%(track_id, org_length, side_faces_len, len(results_list)))
        # lg.debug(results_list)
        for emp_id, confidence in results_list:
            if emp_id is not prev_emp_id:
                if confidence < conf_threshold or emp_id in self.RECOGNITION_IGNORE_EMP_IDS or self.is_invalid_face_crop(emp_id):
                    emp_id = UNKNOWN
            if emp_id not in possible_empids_wts.keys():
                possible_empids_wts[emp_id] = 0.

                # lg.debug("Confidence < %0.2f; Recognition rejected"%conf_threshold)

            wt = 1.0 * confidence if emp_id != UNKNOWN else unk_wt

            if emp_id != UNKNOWN and emp_id == prev_emp_id:
                wt *= consecutive_wt
            else:
                wt *= sparse_wt

            possible_empids_wts[emp_id] += wt
            prev_emp_id = emp_id

        if log:
            lg.debug("TrkId-%d LocalResult-%s" % (track_id, possible_empids_wts))

        result = list(sorted(possible_empids_wts.items(), key=lambda kv: kv[1])[-1])
        if log:
            lg.debug("TrkId-%d LocalMaxima-%s"%(track_id, str(result)))
        
        emp_occurance = [i for i, record in enumerate(results_list) if record[0]==result[0]]
        emp_freq = len(emp_occurance)/len(results_list)

        unk_occurance = [i for i, record in enumerate(results_list) if record[0]==UNKNOWN]
        unk_freq = len(unk_occurance)/len(results_list)
        id_variations = len(possible_empids_wts)

        # # lg.debug("TrkId-%d IDVariation-%d Threshold-%d"%(track_id, id_variations, EMP_VAR_LIMIT))
        # if log:
        #     lg.debug("TrkId-%d Empid-%s confid-%0.2f(%0.2f) freq-%0.2f(%0.2f)" %
        #         (track_id, result[0], result[1], total_conf_threshold, emp_freq, emp_freq_T))
        #     # lg.debug("TrkId-%d Empid-%s freq-%0.2f, Threshold-%0.2f" % (result[0], emp_freq, emp_freq_T))
        #     lg.debug("TrkId-%d Unkfreq-%0.2f, Threshold-%0.2f" % (track_id, unk_freq, unk_freq_T))

        old_emp = result[0]
        if result[1] < total_conf_threshold:
            result[0] = UNKNOWN
            score_ = 0
            # if log:
            #     lg.debug("TrkId-%d Score-0 Final_Empid-UNK old-%s Confid-%0.2f" %(track_id, old_emp, result[1]))

        elif emp_freq < emp_freq_T:
            result[0] = UNKNOWN
            score_ = 1
            # if log:
            #     lg.debug("TrkId-%d Score-1 Final_Empid-UNK old-%s Confid-%0.2f(%0.2f), freq-%0.2f(%0.2f)" %
            #         (track_id, old_emp, result[1], total_conf_threshold, emp_freq, emp_freq_T))

        elif unk_freq > unk_freq_T:
            result[0] = UNKNOWN
            score_ = 2
            # if log:
            #     lg.debug("TrkId-%d Score-2 Final_Empid-UNK old-%s Confid-%0.2f(%0.2f), freq-%0.2f(%0.2f) unk_freq-%0.2f(%0.2f) " %
            #         (track_id, old_emp, result[1], total_conf_threshold, emp_freq, emp_freq_T, unk_freq, unk_freq_T))
        
        elif id_variations > EMP_VAR_LIMIT:
            result[0] = UNKNOWN
            score_ = 3
        else:
            score_ = 4

        if log:
            lg.debug("TrkId-%d Length-%d Score-%d Side-%d Empid-%s Old-%s Confid-%0.2f(>%0.2f), freq-%0.2f(>%0.2f) unk_freq-%0.2f(<%0.2f) IDvariations-%d(<%d)" %
                (track_id, org_length, score_, side_faces_len, result[0], old_emp, result[1], total_conf_threshold, emp_freq, emp_freq_T, unk_freq, unk_freq_T, id_variations, EMP_VAR_LIMIT))


        return result

    def report_track_recognition_status(self, track_id):
        try:
            local_track_recognition_status = self.track_state_table[track_id]
            if local_track_recognition_status['slots_processed'] > 0:
                slots = local_track_recognition_status['slots_processed']
                emp_id, confidence = local_track_recognition_status['recognition_result']
                invalid = local_track_recognition_status['invalid_crops_count']

                emp_name = self.resolve_emp_name(emp_id)

                lg.debug("TrkId-%d Preparing to report" % track_id)
                lg.info("TrkId-%d finally recognized as %s (%s) after processing valid %d/%d total crops; Wt=%0.2f" % (
                    track_id, emp_id, emp_name, slots - invalid, slots, confidence))
                # lg.debug(local_track_recognition_status)
                if not local_track_recognition_status['reported']:
                    lg.debug("TrkId-%d setting reported to true" % track_id)
                    self.track_state_table[track_id]["reported"] = True
                    self.enqueue_for_saving(emp_id, local_track_recognition_status, track_id)

                    lg.debug("TrkId-%d Deleting from state_table" % track_id)
                    self.broadcast_recognition_state_update(track_id, 'delete', ())
                    del self.track_state_table[track_id]
                    # gc.collect()
        except Exception as e:
            lg.error("TrkId-%d Reporting error - %s"%str(e))

    def resolve_emp_name(self, emp_id):
        if emp_id != UNKNOWN:
            try:
                emp_name = self.backend.get_name(emp_id)
            except Exception as e:
                lg.error(e)
                emp_name = UNKNOWN
        else:
            emp_name = UNKNOWN
        return emp_name

    def get_sorted_crops_embeddings_preds_confidence_quality(self, track_status, limit=70, by="confidence"):
        pred_conf = track_status["results"]
        predictions, confidences = [], []
        for p, c in pred_conf:
            predictions.append(p)
            confidences.append(c)

        df = pd.DataFrame(
            zip(
                track_status["crops"],
                track_status["embeddings"],
                predictions,
                confidences,
                track_status["face_quality"]
                ), columns=["crops", "embeddings", "predictions", "confidence", "face_quality"]
        )
        df = df.sort_values(by=by, ascending=False)

        crops, embeddings, predictions, confidence, face_quality = \
            (df[x].tolist()[:limit] for x in ["crops", "embeddings", "predictions", "confidence", "face_quality"])

        lg.debug("sort_result: %s"% confidence)
        return crops, embeddings, predictions, confidence, face_quality


    def enqueue_for_saving(self, emp_id, local_track_recognition_status, track_id):
        crops, embeddings, predictions, confidence, face_quality = self.get_sorted_crops_embeddings_preds_confidence_quality(
            local_track_recognition_status, limit=70
        )

        save_crops_count = len(crops)
        if emp_id != UNKNOWN:
            lg.debug("TrkId-%d Saving Recognized %d Crops" % (track_id, save_crops_count))
            try:
                self.save_q.put(SaveData('save_recognized_crops',
                                         uniqueId=emp_id,
                                         imgList=crops,
                                         embeddingList=embeddings,
                                         camId=local_track_recognition_status["camId"],
                                         trackNo=track_id,
                                         predictions=list(zip(predictions, confidence))
                                         ), timeout=5)
                lg.debug("TrkId-%d Queued for Saving; SaveQSize: %d" % (track_id, self.save_q.qsize()))

            except Exception as e:
                lg.critical(
                    "TrkId-%d data lost (%d crops)  as SaveQueue is Full!" % (track_id, save_crops_count))
        else:
            lg.debug("TrkId-%d Saving %d Crops" % (track_id, save_crops_count))
            try:
                self.save_q.put(SaveData('save_crops',
                                         imgList=crops,
                                         embeddingList=embeddings,
                                         camId=local_track_recognition_status["camId"],
                                         trackNo=track_id,
                                         predictions=list(zip(predictions, confidence))
                                         ), timeout=5)
                lg.debug("TrkId-%d Queued for Saving; SaveQSize: %d" % (track_id, self.save_q.qsize()))
            except Exception as e:
                lg.critical(
                    "TrkId-%d data lost (%d crops) as SaveQueue is Full!" % (track_id, save_crops_count))

    def broadcast_recognition_state_update(self, track_id, cmd, data):
        msg = (track_id, cmd, data)
        for q in self.recognition_state_update_qs:
            try:
                q.put(msg, timeout=1)
            except:
                lg.warning("For TrkId-%d Broadcast Q full; Skipping")

    def register_new_track(self, trk_id):
        # lg.debug("Registering Local track Trk-Id %s"%trk_id)
        self.track_state_table[trk_id] = {
            "slots_processed": 0,
            "slots_queued": 0,
            "reported": False,
            "results": [],
            "crops": sliceable_deque(maxlen=100),
            "embeddings": [],
            "deleted": False,
            "deleted_time": 0,
            "recognition_result": (UNKNOWN, 0),
            "name": UNKNOWN,
            "saved": False,
            "invalid_crops_count": 0,
            "face_quality": []
        }

    def run(self):
        super().run()
        self.main()
        # cProfile.runctx('self.main()', globals(), locals(), 'prof-rec_result_handler.prof')

    def main(self):

        lg.info("Starting %s" % self.__class__.__name__)

        self.init_backend_api()
        self.init_track_state_monitor()

        while not self.shutdown_event.is_set():
            try:
                recognition_result = self.in_q.get_nowait()
                if recognition_result is None:
                    lg.debug("Process detected poison pill; Exiting")
                    break
                else:
                    track_id = recognition_result.track_id
                    emp_id = recognition_result.emp_id
                    confidence = recognition_result.confidence
                    embedding = recognition_result.embedding
                    crop = recognition_result.crop
                    face_quality = recognition_result.face_quality

                    self.update_track_state_table(crop, track_id, emp_id, confidence, embedding, face_quality)
                    batch = self.track_state_table[track_id]['slots_processed']
                    lg.debug("TrkId-%s B%d :: %s @ %0.2f%%" % (track_id, batch, emp_id, confidence))

                    if not self.is_invalid_face_crop(emp_id):
                        weighted_recognition_result = self.weighted_recognition_result(
                            self.track_state_table[track_id]['results'], track_id, log=True
                        )
                        lg.debug("Weighted Recognition Result (Proposed): %s" % weighted_recognition_result)
                        # weighted_recognition_result = self.weighted_recognition_result(
                        #     self.track_state_table[track_id]['results'][-40:], track_id, log=True
                        # )
                        # lg.debug("Weighted Recognition Result: %s" % weighted_recognition_result)
                        uid, uid_conf = weighted_recognition_result
                        if self.track_state_table[track_id]["recognition_result"][0] != uid:
                            self.track_state_table[track_id]["recognition_result"] = weighted_recognition_result
                            if uid != UNKNOWN:
                                self.track_state_table[track_id]["name"] = self.backend.get_name(uid)
                                self.broadcast_recognition_state_update(track_id, 'update',
                                                                    (uid, self.track_state_table[track_id]["name"]))
                            else:
                                self.track_state_table[track_id]["name"] = UNKNOWN
                    else:
                        self.track_state_table[track_id]["invalid_crops_count"] += 1

            except queue.Empty:
                time.sleep(0.001)

            except Exception as e:
                lg.error(e)
                traceback.print_exc()
                time.sleep(0.001)

            self.report_tracks_recognition_status()

        lg.info("Exiting %s" % self.__class__.__name__)

    def report_tracks_recognition_status(self):
        reportable_track_ids = []
        for track_id in list(self.track_state_table.keys()): 
            # list(dict.keys) to avoid runtimeerror: dictionary size changed during iteration
            try:
                track_state = self.track_state_table[track_id]
                if ((track_state["slots_processed"] > self.RECOGNITION_MAX_BATCHES) \
                    or (track_state["deleted"] and track_state["slots_processed"] == track_state[
                            "slots_queued"])) \
                        and track_state["reported"] is False:
                    reportable_track_ids.append(track_id)

            except KeyError as e:
                lg.error("TrkId-%s Temp Key error due to sync fail - %s" % (track_id, e))
                traceback.print_exc()
        for track_id in reportable_track_ids:
            self.report_track_recognition_status(track_id)

    def update_track_state_table(self, crop, track_id, emp_id, confidence, embedding, face_quality):
        if track_id not in self.track_state_table.keys():  # must be new
            self.register_new_track(track_id)
        self.track_state_table[track_id]['queued'] = False
        self.track_state_table[track_id]['slots_processed'] += 1
        self.track_state_table[track_id]['crops'].append(crop)
        self.track_state_table[track_id]['results'].append([emp_id, confidence])
        self.track_state_table[track_id]['embeddings'].append(embedding)
        self.track_state_table[track_id]['face_quality'].append(face_quality)

    def init_track_state_monitor(self):
        lg.info("Starting TrackState Monitor")
        threading.Thread(target=self.track_state_monitor, args=(), daemon=True).start()

    def init_backend_api(self):
        lg.debug("Starting DB API Service")
        self.backend = BackendAPIRest(
            host=self.app_config.backend.host,
            port=self.app_config.backend.port,
            base_api=self.app_config.backend.base_api
        )
        if not self.backend.is_alive():
            lg.warning("No DB Service Found on %s:%s" % (self.app_config.backend.url, self.app_config.backend.port))

import cProfile
import queue
import threading
import time
import traceback

import cv2
import os
import classes.utils
from classes.dtypes import *
from classes.face_detector import FaceBoxesDetector
from classes.hybrid_queue import HybridQueue
from vap.framework.kai_process import KaiProcess
from vap.modules import logging
from vap.modules.deepsort_tracker import DeepSortTracker
from vap.modules.deepsort_tracker import MotpyTracker
from vap.sources import RealtimeVideoFeed
from vap.utils import PredictionUtil, ImageUtil

lg = logging.getLogger(__name__)


class CameraProcess(KaiProcess):

    def __init__(self, app_config, camIdx, camId, video_src, cam_buffer_q, track_state_update_q, out_q, feed_display_q, **kwargs):
        super().__init__(
            daemon=True
        )
        self.app_config = app_config
        self.track_state_update_q = track_state_update_q
        self.out_q = out_q
        self.feed_display_q = feed_display_q
        self.video_src = video_src
        self.camIdx = camIdx
        self.camId = camId
        self.min_valid_bbox_area = self.video_src.detection.min_bbox_area
        self.max_valid_bbox_area = self.video_src.detection.max_bbox_area
        self.buffer = cam_buffer_q #HybridQueue(ram_maxsize=2000, disk_maxsize=0)
        self.trackCount = 0
        self.q_handles = [self.out_q, self.track_state_update_q, self.feed_display_q, self.buffer]
        self.preview_skip_counter = 0

    def unique_name(self):
        return "CameraProcess-%s"%self.camId

    def monitored_status_params(self):
        return {
            "crops_to_recognize_q": self.out_q.qsize(),
            "camId": self.camId,
            "tracksCount": self.trackCount
        }

    def is_valid_detection(self, bbox):
        return (
                self.min_valid_bbox_area < bbox.area < self.max_valid_bbox_area
        )

    def run(self):
        super().run()
        if 1: # classes.utils.is_env_production():
            self.main()
        # else:
        #     cProfile.runctx('self.main()', globals(), locals(), 'prof%s.prof' % self.camId)

    def cap_thread(self, src, roi):
        x1, y1, x2, y2 = [roi[k] for k in ["x1", "y1", "x2", "y2"]]
        while not self.shutdown_event.is_set():
            camera_feed = self.get_camera_feed()
            lg.debug("Opening Feed %s" % self.video_src.src)
            stat_interval = 500
            for frame in camera_feed:
                if self.shutdown_event.is_set():
                    break

                stat_interval -= 1
                if stat_interval < 0:
                    stat_interval = 500
                    try:
                        lg.debug("So far RealtimeVideoFeed skipped %d Frames" % camera_feed.tick_count)
                    except:
                        pass

                try:
                    self.extract_roi_and_buffer(frame, x1, x2, y1, y2)
                except queue.Full:
                    time.sleep(0.05)                
                    lg.warning("Skipped Frame as Q Full %d"%self.buffer.qsize())
                except Exception as e:
                    lg.error(e)

            # break
        lg.info("Exiting CaptureThread for Source: %s" % (self.video_src.src))

    def extract_roi_and_buffer(self, frame, x1, x2, y1, y2):
        # roiframe = frame
        roiframe = frame[y1:y2, x1:x2]
        _, roiframe = cv2.imencode(".jpg", roiframe)
        self.buffer.put_nowait(roiframe)

    def get_camera_feed(self):
        if classes.utils.is_env_production():
            lg.info("Running in PRODUCTION")
            camera_feed = RealtimeVideoFeed(self.video_src.src)
        else:
            camera_feed = RealtimeVideoFeed(self.video_src.src, max_fps=25)
        return camera_feed

    def main(self):
        lg.info("Starting CameraProcess for Source: %s" % self.video_src.src)

        face_detector, tracker = self.init_detector_tracker()
        self.start_capture_thread()

        while not self.shutdown_event.is_set():
            try:
                fullframe_jpg = self.buffer.get_nowait()
                fullframe = cv2.imdecode(fullframe_jpg, 1)
            except queue.Empty as e:
                # lg.debug("buffer is empty! %d"%self.buffer.qsize())
                time.sleep(0.1)
                continue
            except Exception as e:
                lg.error(e)
                time.sleep(0.1)
                continue

            tracked_faces = self.detect_and_track(face_detector, fullframe, tracker)
            self.process_tracks_for_recognition(tracked_faces)
            self.queue_for_preview(fullframe_jpg, tracked_faces)

        lg.info("Exiting %s for Source: %s" % (self.__class__.__name__, self.video_src.src))
        lg.debug("Buffer Qsize: %d"%self.buffer.qsize())
        # self.inject_poison_pill()
        # lg.info("Exit")

    def process_tracks_for_recognition(self, tracked_faces):
        for tracked_face in tracked_faces:
            track = tracked_face.info.track
            # if not track.is_confirmed or track.time_since_update > 1:
            #     continue
            if not track.is_confirmed or track.time_since_update > 1:
                continue
            if track.confirmed_age > 0:
                self.queue_for_recognition(tracked_face)

    def inject_poison_pill(self):
        try:
            lg.info("Injecting Poison Pill")
            self.out_q.put_nowait(None)
        except Exception as e:
            lg.warning("Failed to inject poison pill: %s" % str(e))
            pass

    def queue_for_preview(self, frame_jpg, tracked_faces):
        try:
            # trimmed_tracks = self.trim_tracking_callbacks(tracked_faces)
            timestamp_string = time.strftime("%b %d %Y %H:%M:%S")
            self.feed_display_q.put_nowait((frame_jpg, tracked_faces, timestamp_string))
        except queue.Full:
            self.preview_skip_counter += 1
            if self.preview_skip_counter % 300 == 0:
                lg.warning("Skipped %d frames for preview as Preview-Q full"%self.preview_skip_counter)
        except Exception as e:
            lg.error(e)
            traceback.print_exc()

    def queue_for_recognition(self, tracked_face):
        lg.debug("crops to recognize q: %d" % self.out_q.qsize())
        self.out_q.put(
            DataToRecognize(
                tracked_face.info.track.track_id, tracked_face.info.crop
            )
        )
        self.track_state_update_q.put(
            KwargsObject(track_id=tracked_face.info.track.track_id, event=TRACK_QUEUED, camId=self.camId)
        )

    def detect_and_track(self, face_detector, fullframe, tracker):
        frame = ImageUtil.resize_keep_aspratio(fullframe, width=self.video_src.detection.downsize_width,
                                               height=None)
        faces = face_detector.predict(frame, scale=1.0, score_threshold=0.5)
        faces_copy = faces
        faces = PredictionUtil.expand_bbox(faces, margin=0.1)
        faces = [face for face in faces if self.is_valid_detection(face.bbox)]
        tracked_faces = tracker.update(frame, faces)
        tracked_faces_copy = tracked_faces
        tracked_faces = PredictionUtil.extract_crops_to_info(tracked_faces, (200, 200), cropimage=fullframe)
        print(frame.shape)
        print(tracked_faces)
        for face_list in tracked_faces:
            confidence = face_list.score
            # det = {}
            # det['box'] = []
            # print(det)
            bbox_det = face_list.bbox_asis
            crop = face_list.info.crop
            fullframe_copy = frame.copy()
            print(bbox_det)
            # det.box = [bbox_det.xmin, bbox_det.ymin, bbox_det.xmax, bbox_det.ymax]
            cv2.rectangle(fullframe_copy, (bbox_det.xmin, bbox_det.ymin), (bbox_det.xmax, bbox_det.ymax), (255, 0, 0), 2)
            det_face = fullframe_copy[int(bbox_det.ymin):int(bbox_det.ymax),
                       int(bbox_det.xmin):int(bbox_det.xmax)]
            print(det_face.shape)
            # cv2.imshow(fullframe_copy)
            # cv2.waitkey(0)
            # if  det_face.shape[0] > 0 and  det_face.shape[1] > 0:
                # detections.append(det)
            det_folder = '/tmp' + os.sep + "DETECTIONS"
            if not os.path.exists(det_folder):
                os.makedirs(det_folder)
                cv2.imwrite(det_folder + "/1.jpg", crop)
            else:
                no_of_files = len(os.listdir(det_folder)) + 1
                cv2.imwrite(det_folder + os.sep + str(no_of_files) + ".jpg", crop)
        return tracked_faces

    def start_capture_thread(self):
        roi = self.video_src.roi
        threading.Thread(target=self.cap_thread, args=(self.video_src.src, roi,), daemon=True).start()

    def init_detector_tracker(self):
        face_detector = self.get_new_detector()
        tracker = self.get_new_tracker()

        empty = cv2.imread("assets/initface.jpg")
        initfaces = face_detector.predict(empty)
        tracker.update(empty, initfaces)
        tracker.update(empty, initfaces)
        tracker.clear()
        del empty

        return face_detector, tracker

    def get_new_detector(self):
        lg.debug("Creating Detector")
        face_detector = FaceBoxesDetector()
        return face_detector

    def get_new_tracker(self):
        def track_deletion_callback(track):
            try:
                k = time.time()
                self.track_state_update_q.put(
                    KwargsObject(track_id=track.track_id, event=TRACK_DELETED)
                )
                lg.debug("Trk-Id %s Deleted; took %f sec" % (track.track_id, time.time() - k))
                self.trackCount += 1
                # lg.debug("Time taken by del tracker process %0f" % (time.time() - k))
            except Exception as e:
                lg.error(e)
            #
            # track.crops.clear()
            # del track.crops
            # gc.collect()

        def track_confirmation_tick_callback(track):
            pass

        lg.debug("Creating New Tracker")
        # tracker = DeepSortTracker(
        #     "assets/mars-small128.pb",
        #     track_deletion_callback,
        #     track_confirmation_tick_callback,
        #     start_id=(self.camIdx * 100000) + 1,
        #     metric="cosine",
        #     max_age=6,
        #     max_match_distance=0.19
        # )
        var_pos_q = 5000
        var_pos = 0.1
        ord_pos = 1
        model_spec = {'order_pos': ord_pos, 'dim_pos': 2,
                      'order_size': 0, 'dim_size': 2,
                      'q_var_pos': float(var_pos_q), 'r_var_pos': var_pos}
        tracker = MotpyTracker( dt= 1/25,
            start_id=0,
            model_spec=model_spec)
        return tracker

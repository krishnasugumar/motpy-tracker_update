import cProfile
import multiprocessing as mp
import queue
import threading
import time
import traceback
import imutils

import cv2
from flask import Flask, Response

from classes.face_recogniser import UNKNOWN, UNCLEAR, UNRESOLVED
from classes.video_recorder import VideoRecorder
from vap.framework.kai_process import KaiProcess
from vap.framework import bottle
from vap.modules import logging
# from logging_init import lg
from vap.utils import VisualizationUtil, ImageUtil
from vap.utils.classes import DisplayText, TextAlign, STANDARD_COLORS

lg = logging.getLogger(__name__)



class FeedDisplayProcess(KaiProcess):

    def __init__(self, app_config, camIdx, camId, src, in_q, shared_lookup_table, recognition_state_update_q):
        self.port_no = src['port_no']
        super().__init__(
            health_status_port=self.port_no,
            enable_video_on_status_port=True
        )
        self.shutdown_event = mp.Event()
        self.app_config = app_config
        self.camId = camId
        self.camIdx = str(camIdx)
        self.in_q = in_q
        self.video_src = src
        self.title = str(camIdx) + ": .." + str(src.src)[-8:]
        self.shared_lookup_table = shared_lookup_table
        self.RECOGNITION_CONFIRMATION_CONFIDENCE = app_config.recognition.confirmation_confidence
        self.input_video_recorder = VideoRecorder(prefix="input-rec-%s" % self.camIdx,
                                                  record_dir=app_config.recording.input.save_path,
                                                  config=app_config.recording.input)
        self.output_video_recorder = VideoRecorder(prefix="output-rec-%s" % self.camIdx,
                                                   record_dir=app_config.recording.output.save_path,
                                                   config=app_config.recording.output)
        self.recognition_state_update_q = recognition_state_update_q
        self.lookup_table = {}
        self.cv2_show = True

        self.q_handles = [self.in_q, self.recognition_state_update_q]

    def unique_name(self):
        return "FeedDisplayProcess-%s"%self.camId

    def monitored_status_params(self):
        return {
            "camId": self.camId,
            "title": self.title
        }

    def recognition_state_update_monitor(self):
        while not self.shutdown_event.is_set():
            try:
                (track_id, cmd, data) = self.recognition_state_update_q.get_nowait()
                if track_id in self.lookup_table.keys():
                    if cmd == 'update':
                        emp_id, name = data
                        lg.info("Recognition State Update: %s" % (','.join((str(track_id), str(emp_id), str(name)))))
                        self.lookup_table[track_id] = {'emp_id': emp_id, 'name': name}
                    elif cmd == 'delete':
                        del self.lookup_table[track_id]
            except queue.Empty:
                time.sleep(0.01)

        lg.info("Exiting RecognitionStateUpdateThread for Source: %s" % (self.video_src.src))

    def run(self):
        super().run()
        cProfile.runctx('self.run2()', globals(), locals(), 'prof-feed-%s.prof' % self.camIdx)

    # def health_status_handler_additional_routes(self):
    #     @bottle.get('/video')
    #     def feed():
    #         bottle.response.content_type='multipart/x-mixed-replace; boundary=frame'
    #         return iter(self.live_video_preview_frame_gen())

    # def video_stream(self):
    #     lg.debug("Video stream is activated for camid: %s" % self.camId)
    #     app = Flask(__name__)
    #
    #     @app.route('/')
    #     def index():
    #         return "goto /video_feed"
    #
    #     @app.route('/video_feed')
    #     def feed():
    #         return Response(self.gen(),
    #                         mimetype='multipart/x-mixed-replace; boundary=frame')
    #
    #     app.run(host='0.0.0.0', port=self.port_no, debug=False, threaded=True)
    #
    # def live_video_preview_frame_gen(self):
    #     """Video streaming generator function."""
    #     lg.debug("gen function is called camid: %s" % self.camId)
    #     # threading.Thread(target=self.feed_port, daemon=True).start()
    #     while True:
    #         frame_b = cv2.imencode('.jpg', current_frame)[1].tobytes()
    #
    #         yield (b'--frame\r\n'
    #                b'Content-Type: image/jpeg\r\n\r\n' + frame_b + b'\r\n')

    def run2(self):
        lg.info("Starting FeedDisplayProcess for Source: %s" % self.video_src.src)
        threading.Thread(target=self.recognition_state_update_monitor, args=(), daemon=True).start()
        # threading.Thread(target=self.video_stream, daemon=True).start()
        while not self.shutdown_event.is_set():
            try:
                frame_jpg, tracked_faces, timestamp_string = self.in_q.get_nowait()
                try:
                    if frame_jpg is None:
                        lg.warning("Process detected poison pill; Exiting")
                        break
                    else:
                        # rec_t = time.time()
                        frame = cv2.imdecode(frame_jpg, 1)
                        frame = imutils.resize(frame, width=320)
                        pilframe = ImageUtil.cv_to_pil(frame)
                        timestamp = DisplayText(
                            timestamp_string,
                            TextAlign.TopRight,
                            color=STANDARD_COLORS.Red,
                            bgcolor=STANDARD_COLORS.Black
                        )
                        pilframe = VisualizationUtil.draw_text(pilframe, timestamp)
                        iframe = ImageUtil.pil_to_cv(pilframe)
                        self.input_video_recorder.record(iframe)
                        # lg.debug("recording took %0f" % (time.time() - rec_t))

                        if len(tracked_faces) > 0:
                            self.input_video_recorder.activity_detected()
                            self.output_video_recorder.activity_detected()

                        for tracked_face in tracked_faces:
                            track_id = tracked_face.info.track.track_id
                            try:
                                unique_id = self.lookup_table[track_id]['emp_id']
                                uname = self.lookup_table[track_id]["name"]
                            except Exception as e:
                                lg.error(e)
                                unique_id = UNKNOWN
                                uname = UNKNOWN
                                self.lookup_table[track_id] = {"emp_id": unique_id, "name": uname}
                            tracked_face.info.hidden = False
                            tracked_face.info.boxcolor = 'yellow'
                            if unique_id != UNKNOWN and unique_id != UNCLEAR and unique_id != UNRESOLVED:
                                tracked_face.info.boxcolor = 'LightGreen'
                                tracked_face.info.hidden = False
                            tracked_face.info.captions['Name'] = uname
                            tracked_face.info.captions['EmpID'] = unique_id
                        pilframe = VisualizationUtil.draw_bbox_detections(pilframe, tracked_faces)

                        frame = ImageUtil.pil_to_cv(pilframe)

                        self.push_to_preview_buffer(frame.copy())
                        if self.cv2_show:
                            cv2.imshow(self.title, frame)
                            cv2.waitKey(1)
                        self.output_video_recorder.record(frame)

                except Exception as e:
                    lg.error(e)
                    traceback.print_exc()
                    time.sleep(0.001)
            except:
                time.sleep(0.001)
        if self.cv2_show:
            cv2.destroyAllWindows()
        self.input_video_recorder.kill()
        self.output_video_recorder.kill()
        lg.info("Exiting %s for Source: %s" % (self.__class__.__name__, self.video_src.src))

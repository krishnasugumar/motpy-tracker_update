import queue
import time

import cv2

from classes.dtypes import *
from classes.face_recogniser import FaceRecogniser, UNCLEAR, NOEMBEDDING, InvalidFaceException
from vap.framework.kai_process import KaiProcess
from vap.modules import logging

lg = logging.getLogger(__name__)


class RecognitionProcess(KaiProcess):

    def __init__(self, app_config, in_q, out_q):
        super().__init__()
        self.in_q = in_q
        self.out_q = out_q
        self.app_config = app_config
        self.q_handles = [self.in_q, self.out_q]

    def unique_name(self):
        return "RecognitionProcess-%d"%self.pid

    def monitored_status_params(self):
        return {
            "crops_to_recognize_q": self.in_q.qsize(),
            "recognized_q": self.out_q.qsize(),
        }

    def area_of(self, np_array):
        h, w, _ = np_array.shape
        return h * w

    def is_eligible_for_recognition(self, crop):
        return self.area_of(crop) > 0  # TODO: fix this

    def run(self):
        super().run()
        lg.info("Starting %s" % self.__class__.__name__)

        face_recogniser = self.init_face_recogniser()

        while not self.shutdown_event.is_set():
            try:
                data_to_recognize = self.in_q.get_nowait()
                if data_to_recognize is None:
                    lg.debug("Process detected poison pill; Exiting")
                    break
                else:
                    track_id = data_to_recognize.track_id
                    face_crop = data_to_recognize.crops
                    a = time.time()
                    confidence, embedding, emp_id, face_crop, face_quality = self.process_crop_for_recognition(face_crop,
                                                                                                 face_recogniser)
                    b = time.time() - a
                    lg.debug("TrkId-%s %s  Time taken %0f " % (track_id, emp_id, b))
                    self.enqueue_recognition_result(confidence, embedding, emp_id, face_crop, track_id, face_quality)

            except queue.Empty:
                # lg.debug("crops Q empty")
                time.sleep(0.1)
            except Exception as e:
                lg.error(e)

        lg.info("Exiting %s" % self.__class__.__name__)

    def enqueue_recognition_result(self, confidence, embedding, emp_id, face_crop, track_id, face_quality):
        self.out_q.put(RecognizedData(
            emp_id=emp_id,
            confidence=confidence,
            track_id=track_id,
            eligible_crops_count=1,
            embedding=embedding,
            crop=face_crop,
            face_quality=face_quality
        ))

    def process_crop_for_recognition(self, face_crop, face_recogniser):
        if self.is_eligible_for_recognition(face_crop):
            # TODO: expose probability thresh to config?
            confidence, embedding, emp_id, face_crop, face_quality = self.predict_emp_id(face_recogniser, face_crop)
        else:
            lg.warning("No eligible crops")
            emp_id, confidence, embedding, face_quality = UNCLEAR, 0.0, NOEMBEDDING, 0.0
        return confidence, embedding, emp_id, face_crop, face_quality

    def predict_emp_id(self, face_recogniser, face_crop):
        try:
            face_crop, emp_id, confidence, embedding, face_quality = face_recogniser.recognize_face(face_crop)
        except InvalidFaceException as invalid_ex:
            emp_id, confidence, embedding, face_quality = invalid_ex.facetype, 0.0, NOEMBEDDING, invalid_ex.confid
            face_crop = cv2.cvtColor(face_crop, cv2.COLOR_BGR2GRAY)
        except Exception as e:
            lg.warning(e)
            emp_id, confidence, embedding, face_quality = UNCLEAR, 0.0, NOEMBEDDING, 0.0
            face_crop = cv2.cvtColor(face_crop, cv2.COLOR_BGR2GRAY)

        return confidence, embedding, emp_id, face_crop, face_quality

    def init_face_recogniser(self):
        face_recogniser = FaceRecogniser(self.app_config)
        # just to initialize the cnn
        empty = cv2.imread("assets/initface.jpg")
        try:
            face_recogniser.recognize_face(empty, detect_face=False)
        except Exception as e:
            lg.error(e)
        return face_recogniser

import queue
import time
import traceback

from classes.backend_api_rest import BackendAPI as BackendAPIRest
from vap.framework.kai_process import KaiProcess
from vap.modules import logging

lg = logging.getLogger(__name__)


class SaveDataProcess(KaiProcess):

    def __init__(self, app_config, in_q):
        super().__init__(force_terminate_on_stop=True)
        self.in_q = in_q
        self.app_config = app_config
        self.q_handles = [self.in_q]

    def unique_name(self):
        return "SaveProcess"

    def monitored_status_params(self):
        return {
            "save_q": self.in_q.qsize()
        }

    def run(self):
        super().run()
        self.main()
        # cProfile.runctx('self.run2()', globals(), locals(), 'prof-save-data.prof')

    def main(self):
        lg.info("Starting %s" % self.__class__.__name__)

        lg.debug("Starting DB")
        self.backend = BackendAPIRest(
            host=self.app_config.backend.host,
            port=self.app_config.backend.port,
            base_api=self.app_config.backend.base_api
        )
        if not self.backend.is_alive():
            lg.warning("No DB Service Found on %s:%s" % (self.app_config.backend.url, self.app_config.backend.port))

        while not self.shutdown_event.is_set():
            try:
                save_data = self.in_q.get_nowait()
                lg.debug("Save q size: %d" % self.in_q.qsize())
                if save_data is None:
                    lg.debug("Process detected poison pill; Exiting")
                    break
                else:
                    save_body = save_data.todict()
                    save_type = save_body.pop('save_type', 'save_crops')

                    track_id = save_body["trackNo"]
                    lg.debug("TrkId-%d Pushing to DB" % track_id)

                    if save_type == 'save_crops':
                        self.backend.save_crops(**save_body)
                    elif save_type == 'save_recognized_crops':
                        self.backend.save_recognized_crops(**save_body)
            except queue.Empty:
                time.sleep(0.01)
            except Exception as e:
                lg.error(e)
                traceback.print_exc()
                time.sleep(0.01)

        lg.info("Exiting %s" % (self.__class__.__name__))

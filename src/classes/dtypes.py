TRACK_DELETED = "deleted"
TRACK_QUEUED = "queued"
TRACK_CREATED = "created"


class DataToRecognize(object):

    def __init__(self, track_id, crops):
        self.track_id = track_id
        self.crops = crops


class RecognizedData(object):

    def __init__(self, emp_id, confidence, track_id, eligible_crops_count, embedding, crop, face_quality):
        self.emp_id = emp_id
        self.confidence = confidence
        self.track_id = track_id
        self.eligible_crops_count = eligible_crops_count
        self.embedding = embedding
        self.crop = crop
        self.face_quality = face_quality


class SaveData(object):

    def __init__(self, save_type, **kwargs):
        self.save_type = save_type
        for key, value in kwargs.items():
            setattr(self, key, value)

    def todict(self):
        return self.__dict__


class KwargsObject(object):

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

    def todict(self):
        return self.__dict__

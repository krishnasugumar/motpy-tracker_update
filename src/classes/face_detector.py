from vap import models, config


class FaceBoxesDetector(models.TFSingleClassBBoxDetector):
    """ FaceBoxes detector """

    def __init__(self, model_path="assets/faceboxes-model.pb", gpu_memory_fraction=0.1, visible_device_list='0'):
        tfconfig = config.TFModelConfig(
            model_path=model_path,
            gpu_memory_fraction=gpu_memory_fraction,
            cuda_visible_device_list=visible_device_list,
            input_tensors=["import/image_tensor:0"],
            output_tensors=["import/boxes:0", "import/scores:0", "import/num_boxes:0"]
        )
        super(FaceBoxesDetector, self).__init__(config=tfconfig, classname="Face")

    def predict(self, input, scale=1., score_threshold=0.5):
        cvimage = input[:, :, ::-1]

        faces = super().predict(
            cvimage,
            scale=scale,
            score_threshold=score_threshold
        )

        return faces


class FaceEmbeddingsExtractor(models.TFFaceEmbeddingsExtractor):
    """Gives Face Embeddings :AD:"""

    def __init__(self, model_path="assets/insightface.pb", gpu_memory_fraction=0.25, visible_device_list='0'):
        tfconfig = config.TFModelConfig(
            model_path=model_path,
            gpu_memory_fraction=gpu_memory_fraction,
            cuda_visible_device_list=visible_device_list,
            input_tensors=['img_inputs:0', 'dropout_rate:0'],
            output_tensors=['resnet_v1_50/E_BN2/Identity:0'],
            import_name=""
        )
        super(FaceEmbeddingsExtractor, self).__init__(config=tfconfig)

    def predict(self, image, face_describer_tensor_shape=(112, 112)):
        embeddings = super().predict(image, face_describer_tensor_shape=face_describer_tensor_shape)

        return embeddings[0][0]

import logging
import os

import dlib
import numpy as np

from classes.keras_facefilter import FaceFilter
from classes.one_shot_learning import OneShotRecognition
from enum import Enum

lg = logging.getLogger(__name__)
UNKNOWN = "UNK"
UNCLEAR = "BAD"
UNRESOLVED = "..."
NOEMBEDDING = []


class FaceType(str, Enum):
    Frontal = 'Frontal'
    SideFace = 'SideFace'
    NoFace = 'NoFace'


class InvalidFaceException(Exception):
    def __init__(self, facetype, confid):
        self.facetype = facetype
        self.confid = confid
        self.msg = "Invalid FaceType: %s"%facetype.name

class FaceRecogniser(object):
    def __init__(self, appconfig):

        landmarks_model_path = "assets/dlib_models/models/shape_predictor_5_face_landmarks.dat"
        embeddings_model_path = "assets/dlib_models/models/dlib_face_recognition_resnet_model_v1.dat"
        mmod_model_path = "assets/dlib_models/models/mmod_human_face_detector.dat"

        if not os.path.exists(landmarks_model_path):
            raise ValueError("Alignment Model Path doesn't exist")

        if not os.path.exists(embeddings_model_path):
            raise ValueError("Embeddings Model Path doesn't exist")

        self.landmarks_model = dlib.shape_predictor(landmarks_model_path)
        self.embeddings_model = dlib.face_recognition_model_v1(embeddings_model_path)
        self.face_det = dlib.cnn_face_detection_model_v1(mmod_model_path)
        self.ff = FaceFilter()
        try:
            # self.recognition_engine = SvcClassifier(
            #     appconfig
            # )
            self.recognition_engine = OneShotRecognition(
                appconfig
            )
        except Exception as e:
            lg.error("Failed to load recognition engine: %s" % e)
            self.recognition_engine = None

    def recognize_face(self, face_crop, detect_face=True):
        frontal_confidence, is_frontal = self.ff.how_probable_is_front_face(face_crop)
        if not is_frontal:
            # face_crop = cv2.cvtColor(face_crop, cv2.COLOR_BGR2GRAY)
            raise InvalidFaceException(FaceType.SideFace, frontal_confidence)

        face_embedding = self.get_embedding(face_crop, detect_face)

        if self.is_empty(face_embedding):
            raise InvalidFaceException(FaceType.NoFace, 0.0)

        if self.recognition_engine is None:
            face_uid, confidence = UNKNOWN, 0.0
        else:
            face_uid, confidence = self.recognition_engine.predict([face_embedding])

        return face_crop, face_uid, confidence, face_embedding, frontal_confidence

    def get_embedding(self, face_crop, num_jitters=10, detect_face=True):
        try:
            face_landmarks = self.get_landmarks(face_crop, detect_face)
            face_embeddings_np = np.squeeze(np.array(
                self.embeddings_model.compute_face_descriptor(
                    face_crop, face_landmarks, num_jitters
                )))
        except Exception as e:
            lg.error(e)
            face_embeddings_np = np.array([])

        return face_embeddings_np

    @staticmethod
    def is_empty(embedding_np):
        return embedding_np.size != 128

    def get_landmarks(self, face_crop, detect_face=True):
        face_location = self.get_face_location(face_crop, detect_face)
        face_landmark = self.landmarks_model(face_crop, face_location)
        face_landmarks = dlib.full_object_detections()
        face_landmarks.append(face_landmark)
        return face_landmarks

    def get_face_location(self, face_crop, detect_face=True):
        if not detect_face:
            face_location = dlib.rectangle(0, 0, face_crop.shape[1], face_crop.shape[0])
        else:
            valid_faces = self.face_det(face_crop, 1)
            if len(valid_faces) > 1:
                lg.warning(len(valid_faces))
                raise RuntimeError("More than one face found in recognition crop")
            elif len(valid_faces) == 0:
                raise RuntimeError("No face found in recognition crop")

            face_location = valid_faces[0].rect
        return face_location




if __name__ == "__main__":
    test()
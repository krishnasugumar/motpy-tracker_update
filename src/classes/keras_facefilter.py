"""
Owner : Shaik Ahmad. Recode Solutions
Purpose: Face Filtering using keras classifier
Created Date: 08-02-2020: 20:56:00
Update1: Adding KerasCLF object to perform classification problem in a generalised way
Update2: Adding FaceFilter to detect side faces
 
"""

import logging
import os
import time

import cv2
import imutils
import numpy as np
from keras.models import load_model
from keras.preprocessing.image import img_to_array

lg = logging.getLogger(__name__)
from keras.backend.tensorflow_backend import set_session
import tensorflow as tf


def get_prediction_ready_img(img, img_size=(32, 32)):
    img = cv2.resize(img, img_size)
    img = img.astype("float") / 255.0
    img = img_to_array(img)
    img = np.expand_dims(img, axis=0)
    return img


class KerasCLF:
    def __init__(self, model_path=None,
                 lable_encoder_path=None):
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = False  # dynamically grow the memory used on the GPU
        config.gpu_options.per_process_gpu_memory_fraction = 0.08
        config.log_device_placement = False  # to log device placement (on which device the operation ran)
        sess = tf.Session(config=config)
        set_session(sess)  # set this TensorFlow session as the default session for Keras

        if not os.path.exists(model_path):
            raise ValueError("Classifier Model Path doesn't exist")

        # if not os.path.exists(lable_encoder_path):
        #     raise ValueError("Classifier Model class file Path doesn't exist")

        self.model_path = model_path
        self.lable_encoder_path = lable_encoder_path
        try:
            self.model = load_model(self.model_path)
            self.lable_encoder = np.array(['frontal', 'side'])
            # self.lable_encoder = pickle.loads(open(self.lable_encoder_path, "rb").read())
        except Exception as e:
            lg.debug("Failed to load the model: %s" % e)
            self.model = None

    def predict(self, img):
        if self.model is None:
            return ("Invalid", 0.)
        predictions = self.model.predict(img)[0]
        max_class_indx = np.argmax(predictions)
        # class_label = self.lable_encoder.classes_[max_class_indx]
        class_label = self.lable_encoder[max_class_indx]
        return (class_label, predictions[max_class_indx])


class FaceFilter:
    def __init__(self, model_path="assets/front_side_700_v3.model",
                 lable_encoder_path="assets/le_front_side_700_v1.pickle"):

        self.model_path = model_path
        self.lable_encoder_path = lable_encoder_path
        self.kerasclf = KerasCLF(model_path=self.model_path, lable_encoder_path=self.lable_encoder_path)
        self.thr = 0.

    def is_front_face(self, face, thr = 0.):
        self.thr = thr
        a = time.time()
        face = get_prediction_ready_img(face)
        class_label, confid = self.kerasclf.predict(face)
        lg.debug("Face-filter prediction: %s, confidance: %s, time: %s sec" % (class_label, confid, time.time() - a))
        if class_label == "frontal" and confid > thr:
            return True
        else:
            return False

    def how_probable_is_front_face(self, face, thr = 0.):
        self.thr = thr
        a = time.time()
        face = get_prediction_ready_img(face)
        class_label, confid = self.kerasclf.predict(face)
        lg.debug("Face-filter prediction: %s, confidance: %s, time: %s sec" % (class_label, confid, time.time() - a))
        if class_label == "frontal":
            if confid > thr:
                return confid, True
            else:
                return confid, False
        else:
            return 1 - confid, False


def run():
    SOURCE = 0  # "/data/work/face-rec/face-data/input_videos/input-rec-20Dec2019_003942.mp4"
    modelPath = "/data/work/face-rec/anti-spoofing1/liveness-detection-pyimagesearch/face_detector/res10_300x300_ssd_iter_140000.caffemodel"
    protoPath = "/data/work/face-rec/anti-spoofing1/liveness-detection-pyimagesearch/face_detector/deploy.prototxt"
    ff = FaceFilter()
    lg.debug("[INFO] starting video stream...")
    net = cv2.dnn.readNetFromCaffe(protoPath, modelPath)
    vs = cv2.VideoCapture(SOURCE)
    grabbed, frame = vs.read()
    frame = imutils.resize(frame, width=1080)
    while True:
        grabbed, frame = vs.read()
        try:
            frame = imutils.resize(frame, width=1080)
        except AttributeError:
            time.sleep(0.01)
            break

        (h, w) = frame.shape[:2]
        blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 1.0,
                                     (300, 300), (104.0, 177.0, 123.0))

        net.setInput(blob)
        detections = net.forward()

        for i in range(0, detections.shape[2]):
            try:
                confidence = detections[0, 0, i, 2]
                if confidence > 0.5:
                    box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                    (startX, startY, endX, endY) = box.astype("int")

                    startX = max(0, startX - 10)
                    startY = max(0, startY - 10)
                    endX = min(w, endX + 10)
                    endY = min(h, endY + 10)

                    face = frame[startY:endY, startX:endX]
                    if ff.is_front_face(face):
                        label = "Front_Face"
                        cv2.putText(frame, label, (startX, startY - 10),
                                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 255), 2)
                        cv2.rectangle(frame, (startX, startY), (endX, endY),
                                      (0, 255, 255), 2)
                    else:
                        label = "Side_Face"
                        cv2.putText(frame, label, (startX, startY - 10),
                                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                        cv2.rectangle(frame, (startX, startY), (endX, endY),
                                      (255, 0, 0), 2)
            except Exception as e:
                print("Prediction Error : %s" % e)
                time.sleep(0.01)
        cv2.imshow("Frame", frame)
        key = cv2.waitKey(1) & 0xFF

        if key == ord("q"):
            break

    cv2.destroyAllWindows()
    vs.release()


def main():
    run()


if __name__ == '__main__':
    main()

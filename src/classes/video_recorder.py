import logging
import os
import threading
import time
from datetime import time as dtime

import cv2

import classes.utils

lg = logging.getLogger(__name__)


class VideoRecorder(object):

    def __init__(self, prefix, record_dir, config):

        self.record_dir = record_dir
        FOURCC = cv2.VideoWriter_fourcc(*"mp4v")
        self.RECORD_MAX_FRAME_COUNT = config.frames_per_file
        self.RECORD_ADJACENT_FRAME_COUNT = config.post_activity_record_frame_count
        self.record_timeout = config.duration_per_file
        self.record_timeout_reset = config.duration_per_file
        self.record_remaining_adjacent_frame_count = self.RECORD_ADJACENT_FRAME_COUNT
        self.recorded_frame_count = 0
        self.shutdown = False
        self.ticker = None
        self.timeout_event = threading.Event()

        if not os.path.exists(self.record_dir):
            os.makedirs(self.record_dir)

        self.prefix = prefix

        self.recording_save_path = os.path.join(self.record_dir,
                                                self.prefix + time.strftime("-%d%b%Y_%H%M%S.avi", time.localtime()))
        self.four_cc = FOURCC
        self.video_writer = None
        self.enabled = config.enabled

    def is_critical_time(self):
        # critical_times = [
        #     ( dtime(7,00), dtime(8,00) ),
        #     ( dtime(15,00), dtime(16,00)),
        #     ( dtime(23,50), dtime(1,00))
        # ]
        critical_times = [
            (dtime(6, 00), dtime(9, 00)),
            (dtime(15, 00), dtime(16, 30)),
            (dtime(19, 00), dtime(20, 00)),
            (dtime(23, 50), dtime(1, 00)),
        ]

        for ct in critical_times:
            if classes.utils.is_time_between(ct[0], ct[1]):
                return True

        return False

    def activity_detected(self):
        self.record_remaining_adjacent_frame_count = self.RECORD_ADJACENT_FRAME_COUNT

    def kill(self):
        self.shutdown = True
        if self.video_writer is not None:
            self.video_writer.release()
            self.video_writer = None

    def recording_timedout(self):
        if self.is_critical_time():
            lg.debug("recording file duration hit; Critical time detected; will NOT restart until next timeout")
        else:
            self.timeout_event.set()

    def stop_current_recording(self):
        if self.video_writer is not None:
            lg.debug("Releasing video writer")
            self.video_writer.release()
            lg.debug("Deleting video writer")
            del self.video_writer
            lg.debug("Setting to None")
            self.video_writer = None

    def start_new_recording(self, H, W):
        self.recording_save_path = os.path.join(self.record_dir,
                                                self.prefix + time.strftime("-%d%b%Y_%H%M%S.mp4", time.localtime()))
        lg.debug("Recording Video\nDEST:%s\nFOURCC:%s\nFPS:%d\nWtxHt:%dx%d" % (
            self.recording_save_path, self.four_cc, 25, W, H))
        self.video_writer = cv2.VideoWriter(self.recording_save_path, self.four_cc, 25, (W, H), True)
        self.recorded_frame_count = 0
        self.record_timeout = self.record_timeout_reset
        if self.ticker:
            self.ticker.cancel()
        self.ticker = classes.utils.RepeatTimer(self.record_timeout, self.recording_timedout)
        self.ticker.start()

    def record(self, frame):

        if self.enabled:

            if self.recorded_frame_count >= self.RECORD_MAX_FRAME_COUNT:
                lg.debug("Closing input recording as max frames hit; will restart")
                self.stop_current_recording()

            if self.timeout_event.is_set():
                lg.debug("Closing input recording as file duration hit; will restart")
                self.stop_current_recording()
                self.timeout_event.clear()

            if self.video_writer is None:
                H, W = frame.shape[:2]
                self.start_new_recording(H, W)

            if self.record_remaining_adjacent_frame_count > 0 and not self.timeout_event.is_set():
                self.video_writer.write(frame)
                self.recorded_frame_count += 1
                self.record_remaining_adjacent_frame_count -= 1

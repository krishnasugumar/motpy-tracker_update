import logging
import pickle

import numpy as np

from classes import utils
from clf_utils.class_label_utils import postprocess_labels

lg = logging.getLogger(__name__)


class SvcClassifier(object):

    def __init__(self, config):
        # lg.info("Loading SVC Classifier from %s and %s"%(clf_model_path, clf_labels_path))

        clf_model_path = utils.most_recent_file(config.recognition.training.svc.model_path_prefix, ".clf")
        clf_labels_path = utils.most_recent_file(config.recognition.training.svc.classfile_prefix, ".npy")

        lg.info("Loading SVC Classifier from %s and %s" % (clf_model_path, clf_labels_path))

        with open(clf_model_path, 'rb') as f:
            self.clf_model = pickle.load(f)

        self.encoder = postprocess_labels(clf_labels_path)

    def predict(self, embedding):
        pred_probs = self.clf_model.predict_proba([embedding])
        out_prob, out_class_num = np.max(pred_probs, axis=1), np.argmax(pred_probs, axis=1)
        out_class_name = self.encoder.inverse_transform(out_class_num)

        return (out_class_name[0], out_prob[0])

import itertools
from collections import deque


class sliceable_deque(deque):
    def __getitem__(self, index):
        if isinstance(index, slice):
            if index.start < 0:
                idx = max(0, self.__len__() - abs(index.start))
            else:
                idx = index.start

            return type(self)(itertools.islice(self, idx,
                                               index.stop, index.step))
        return deque.__getitem__(self, index)

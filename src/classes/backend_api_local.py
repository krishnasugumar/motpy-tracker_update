import datetime
import logging
import os
import shutil
import time
import uuid
from glob import glob

import cv2
import numpy as np
import pandas as pd

from classes.face_recogniser import UNKNOWN

pd.options.mode.chained_assignment = None

lg = logging.getLogger(__name__)


class DBEntry(object):
    def __init__(
            self,
            clientId,
            cameraId,
            uniqueId,
            employeeId,
            timestamp=datetime.datetime.now(),
            firstName="",
            lastName="",
            role="",
            approved="N",
            comments="",
            imagesPathPrefix="",
    ):
        self.clientId = clientId
        self.cameraId = cameraId
        self.timestamp = timestamp
        self.uniqueId = uniqueId
        self.employeeId = employeeId
        self.firstName = firstName
        self.lastName = lastName
        self.role = role
        self.approved = approved
        self.comments = comments
        self.imagesPath = "%s/%s" % (imagesPathPrefix, self.uniqueId)

    @property
    def as_dict(self):
        return {
            'timestamp': self.timestamp,
            'clientId': self.clientId,
            'cameraId': self.cameraId,
            'uniqueId': self.uniqueId,
            'employeeId': self.employeeId,
            'firstName': self.firstName,
            'lastName': self.lastName,
            'role': self.role,
            'approved': self.approved,
            'comments': self.comments,
            'imagesPath': self.imagesPath
        }


class BackendAPI(object):
    def __init__(
            self,
            enrollment_csv_path="db.csv",
            lookup_csv_path="lookup.csv",
            logging_csv_path="log.csv",
            detected_faces_img_path_prefix=None,
            recognized_faces_img_path_prefix=None,
            img_path_prefix=None
    ):
        super().__init__()

        self.schema = {
            'timestamp': [],
            'clientId': [],
            'uniqueId': [],
            'employeeId': [],
            'firstName': [],
            'lastName': [],
            'role': [],
            'approved': [],
            'comments': [],
            'imagesPath': []
        }

        self.enrollment_df = pd.DataFrame(data=self.schema)
        self.lookup_df = pd.DataFrame(data=self.schema)
        self.logging_df = pd.DataFrame(data=self.schema)
        self.enrollment_csv_path = enrollment_csv_path
        self.logging_csv_path = logging_csv_path
        self.lookup_csv_path = lookup_csv_path

        self.cam_id = "0"

        self.client_id = "tvs-puzhal"

        self._img_path_prefix = "image_database/%s/" % (self.client_id) if img_path_prefix is None else img_path_prefix
        self._detected_faces_img_path_prefix = "detected_faces/%s/" % (
            self.client_id) if detected_faces_img_path_prefix is None else detected_faces_img_path_prefix
        self._recognized_faces_img_path_prefix = "recognized_faces/%s/" % (
            self.client_id) if recognized_faces_img_path_prefix is None else recognized_faces_img_path_prefix
        if not os.path.exists(self._img_path_prefix):
            os.makedirs(self._img_path_prefix)
        if not os.path.exists(self._detected_faces_img_path_prefix):
            os.makedirs(self._detected_faces_img_path_prefix)

    @property
    def img_path_prefix(self):
        return self._img_path_prefix

    @property
    def detected_faces_img_path_prefix(self):
        return self._detected_faces_img_path_prefix

    @property
    def recognized_faces_img_path_prefix(self):
        return self._recognized_faces_img_path_prefix

    def log_face(self, empId, saveDB=True):
        row = self.enrollment_df.loc[self.enrollment_df['uniqueId'] == empId]
        row.ix[:, 'comments'] = 'logged_in'
        row.ix[:, 'timestamp'] = datetime.datetime.now()
        # dbentry = DBEntry(
        #     clientId=self.client_id,
        #     uniqueId=empId,
        #     employeeId="",
        #     camera_id=self.cam_id,
        #     approved='Y',
        #     comments='logged_in'
        # )

        self.logging_df = self.logging_df.append(row)

        if saveDB:
            self.save_db()

    def delete_uniqueIds(self, uniqueIds):
        self.enrollment_df = self.enrollment_df[~self.enrollment_df['uniqueId'].isin(uniqueIds)]
        self.save_db()
        for uid in uniqueIds:
            uid_dir = os.path.join(self.img_path_prefix, uid)
            if os.path.exists(uid_dir):
                shutil.rmtree(uid_dir)

    def merge_uniqueIds(self, uniqueIds):
        merged_uid = uniqueIds[0]
        merged_uid_dir = os.path.join(self.img_path_prefix, merged_uid)
        files_count = 0
        for uid in uniqueIds[1:]:
            uid_dir = os.path.join(self.img_path_prefix, uid)
            uid_files = os.listdir(uid_dir)
            files_count += len(uid_files)
            for i, uid_file in enumerate(uid_files):
                src_filename = os.path.join(uid_dir, uid_file)
                dest_filename = os.path.join(merged_uid_dir, "%d_%s.jpg" % (i, uid))
                shutil.move(src_filename, dest_filename)
        lg.debug("Moved %d files to merge" % files_count)
        self.delete_uniqueIds(uniqueIds[1:])

    def update_cell(self, uniqueId, colIndex, newValue):
        colname = self.enrollment_df.columns[colIndex]
        rowindex = self.enrollment_df.index[self.enrollment_df['uniqueId'] == uniqueId].tolist()[0]
        self.enrollment_df.ix[rowindex, colname] = newValue
        lg.debug("updated column %s of uniqueId %s with %s" % (colname, uniqueId, newValue))
        self.save_db()

    def get_reference_image(self, empId):
        ref_dir = os.path.join(self.img_path_prefix, empId)
        if not os.path.exists(ref_dir):
            return None
        else:
            image_paths = glob(ref_dir + "/*.jpg")
            return cv2.imread(image_paths[0])

    def save_recognized_crops(self, imgList, trackId="", empId="", tree=True):
        try:
            timestamp = time.strftime("%d%b%Y_%H%M%S", time.localtime())
            if tree:
                savedir = os.path.join(self.recognized_faces_img_path_prefix, empId)
            else:
                savedir = self.recognized_faces_img_path_prefix

            if not os.path.exists(savedir):
                os.makedirs(savedir)

            ref_image = self.get_reference_image(empId)
            curr_image = imgList[0]

            if ref_image is not None:
                sh, sw, _ = curr_image.shape
                ref_image = cv2.resize(ref_image, (sw, sh))
                rh, rw, _ = ref_image.shape

                h = max(rh, sh)
                w = rw + sw

                sImg = np.zeros([h, w, 3], dtype=np.uint8)
                sImg[:rh, 0:rw] = ref_image
                sImg[:sh, rw:rw + sw] = curr_image

            else:
                sImg = curr_image

            savepath = "%s/%s_%d.jpg" % (savedir, timestamp, trackId)
            cv2.imwrite(savepath, sImg)
        except Exception as e:
            lg.error(e)

    def save_crops(self, imgList, trackId="", tree=True, savedir=None, refImage=None):
        if trackId == "":
            trackId = str(uuid.uuid1()).split("-")[0]

        timestamp = time.strftime("%d%b%Y_%H%M%S", time.localtime())
        if savedir is None:
            if tree:
                savedir = os.path.join(self.detected_faces_img_path_prefix, timestamp, "%d" % trackId)
            else:
                savedir = self.detected_faces_img_path_prefix

        if not os.path.exists(savedir):
            os.makedirs(savedir)

        lg.debug("Saving %d crops to %s" % (len(imgList), savedir))

        for i, img in enumerate(imgList):
            if tree:
                savepath = os.path.join(savedir, "%d_%d.jpg" % (trackId, i))
            else:
                savepath = os.path.join(savedir, "%s_%s_%d.jpg" % (timestamp, trackId, i))

            if refImage is not None:
                rh, rw, _ = refImage.shape
                sh, sw, _ = img.shape
                h = max(rh, sh)
                w = rw + sw

                sImg = cv2.zeros([h, w, 3], dtype=np.uint8)
                sImg[:, 0:rw] = refImage
                sImg[:, rw:sw] = img

            else:
                sImg = img

            cv2.imwrite(savepath, sImg)

    def enroll_face(self, imgList, prefix="", saveDB=False):
        newid = str(uuid.uuid1())
        savedir = os.path.join(self.img_path_prefix, "%s_%s" % (newid, prefix))
        if not os.path.exists(savedir):
            os.makedirs(savedir)

        for i, img in enumerate(imgList):
            savepath = os.path.join(savedir, "%s_%d.jpg" % (newid, i))
            cv2.imwrite(savepath, img)

        dbentry = DBEntry(
            clientId=self.client_id,
            uniqueId=newid,
            employeeId="",
            cameraId=self.cam_id,
            imagesPathPrefix=self.img_path_prefix
        )
        self.enrollment_df = self.enrollment_df.append(dbentry.as_dict, ignore_index=True)
        if saveDB:
            self.save_db()

    def get_name(self, uniqueId):
        if self.lookup_df is not None:
            return self.get_name_from_lookup(uniqueId)

        if uniqueId == UNKNOWN:
            return UNKNOWN

        rows = self.enrollment_df.loc[self.enrollment_df['uniqueId'] == uniqueId]
        if len(rows.index) == 0:
            return 'unknown'

        fname, lname = rows['firstName'].values[0], rows['lastName'].values[0]
        if str(fname) == "nan":
            fname = ""
        if str(lname) == "nan":
            lname = ""

        name = fname + ' ' + lname
        return name

    def get_name_from_lookup(self, uniqueId):
        if uniqueId == UNKNOWN:
            return UNKNOWN

        rows = self.lookup_df.loc[self.lookup_df['uniqueId'] == uniqueId]
        if len(rows.index) == 0:
            return 'unknown'

        fname, lname = rows['firstName'].values[0], rows['lastName'].values[0]
        if str(fname) == "nan":
            fname = ""
        if str(lname) == "nan":
            lname = ""

        name = fname + ' ' + lname
        return name

    def is_known_approved(self, uniqueId):  # returns in_db, is_approved
        if self.lookup_df is not None:
            return self.is_known_approved_from_lookup(uniqueId)

        rows = self.enrollment_df.loc[self.enrollment_df['uniqueId'] == uniqueId]
        if len(rows.index) == 1:
            return True, (rows['approved'] == 'Y').bool()
        else:
            return False, False

    def is_known_approved_from_lookup(self, uniqueId):
        rows = self.lookup_df.loc[self.lookup_df['uniqueId'] == uniqueId]
        if len(rows.index) == 1:
            return True, (rows['approved'] == 'Y').bool()
        else:
            return False, False

    def load_db(self):
        ok = False
        if os.path.exists(self.enrollment_csv_path):
            self.enrollment_df = pd.read_csv(self.enrollment_csv_path, dtype=object, keep_default_na=False)
            ok = True
        else:
            lg.warning("enrollment csv doens't exist")

        if os.path.exists(self.lookup_csv_path):
            self.lookup_df = pd.read_csv(self.lookup_csv_path, dtype=object, keep_default_na=False)
        else:
            self.lookup_df = None
            lg.warning("lookup csv doesn't exists; will use enrollment csv")

        if os.path.exists(self.logging_csv_path):
            self.logging_df = pd.read_csv(self.logging_csv_path, dtype=object, keep_default_na=False)

        return ok

    def save_db(self):
        self.enrollment_df.to_csv(self.enrollment_csv_path, index=False)
        self.logging_df.to_csv(self.logging_csv_path, index=False)

    # def train(self):
    #     train_set_path = self.img_path_prefix
    #     model_path = datetime.datetime.now().strftime("%Y%m%d")
    #     faceTrainer = FaceTrainer(train_set_path, model_name=model_path)
    #     faceTrainer.extract_train_embeddings()

    def clear_train_set(self):
        train_set_path = self.img_path_prefix
        shutil.rmtree(train_set_path, ignore_errors=True)

    def clear_detected_faces(self):
        lg.warning("Deleting detected faces directory %s" % self.detected_faces_img_path_prefix)
        shutil.rmtree(self.detected_faces_img_path_prefix, ignore_errors=True)


if __name__ == "__main__":
    bi = BackendAPI()
    bi.load_dump()
    # bi.train()

    # for k in range(10):
    #     bi.addLog(str(uuid.uuid1()))

    # bi.dump()

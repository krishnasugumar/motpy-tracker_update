import base64
import logging
import time
import traceback
from concurrent.futures import ThreadPoolExecutor
import json
import cv2
import numpy as np
import requests
from concurrent.futures import ThreadPoolExecutor
import traceback
import json

import logging

lg = logging.getLogger(__name__)
import ast

LOG_POST = False

class BackendAPI(object):
    def __init__(self,
                 host="localhost",
                 port=8082,
                 base_api="kamerAI-api/home-api/kamerai-service/"
                 ):
        super().__init__()

        self.enable_thread_pool = False
        self.host = host
        self.port = port
        self.base_api = base_api
        self.thread_pool = ThreadPoolExecutor(max_workers=2)
        self.username = "admin"
        self.password = "admin123$"
        userAndPass = base64.b64encode(
            bytes("{username}:{password}".format(username=self.username, password=self.password), 'ascii')).decode(
            "ascii")
        self.headers = {'Authorization': 'Basic %s' % userAndPass, "Content-Type": 'application/json'}
        self.sess = requests.Session()
        self.data30 = {}
        self.count_save_recognized_success = 0
        self.count_save_recognized_fail = 0
        self.count_save_unrecognized_success = 0
        self.count_save_unrecognized_fail = 0

    @property
    def url(self):
        return "http://{host}:{port}/{base_api}".format(host=self.host, port=self.port, base_api=self.base_api)

    def post(self, endpoint, data, timeout=(1,30)):
        request_url = "{url}/{endpoint}".format(url=self.url, endpoint=endpoint)
        try:
            if LOG_POST:
                self.data30[data[0]["refTrackNo"]] = data
                with open("payloads.json", "w") as pf:
                    pf.write(json.dumps(self.data30))
                    lg.debug("written payload to json %d"%len(self.data30.keys()))
            r = self.sess.post(request_url, json=data, headers=self.headers, timeout=timeout)

            if r.status_code == 404:
                lg.debug([d['confidence'] for d in data])
                if "recognized" in endpoint:
                    self.count_save_recognized_fail +=1
                else:
                    self.count_save_unrecognized_fail +=1
            else:
                if "recognized" in endpoint:
                    self.count_save_recognized_success +=1
                else:
                    self.count_save_unrecognized_success +=1
            r = r.json()
            lg.debug(r)
            
        except Exception as e:
            lg.error(e)
            traceback.print_exc()
            if "recognized" in endpoint:
                self.count_save_recognized_fail +=1
            else:
                self.count_save_unrecognized_fail +=1

    def get(self, endpoint, params=None, debug=True, asjson=True):
        request_url = "{url}/{endpoint}".format(url=self.url, endpoint=endpoint)
        try:
            r = self.sess.get(request_url, headers=self.headers, params=params)
            if asjson:
                r = r.json()
            # if debug:
            #     lg.debug(r)
            return r
        except Exception as e:
            traceback.print_exc()
            lg.error(e)

    def is_alive(self):
        return True

    def get_v1(self, endpoint, params=None, debug=True, asjson=True):
        request_url = "{url}/{endpoint}".format(url="http://localhost:8082/kamerai-api", endpoint=endpoint)
        try:
            r = self.sess.get(request_url, headers=self.headers, params=params)
            if asjson:
                r = json.loads(r.text)
            if debug:
                lg.debug(r)
            return r
        except Exception as e:
            traceback.print_exc()
            lg.error(e)

    def save_crops(self, imgList, embeddingList, camId, trackNo=None, predictions=None):
        lg.debug("Submitted SaveCrops")
        if self.enable_thread_pool:
            self.thread_pool.submit(self.async_save_crops, lg, imgList, embeddingList, camId, trackNo, predictions)
        else:
            self.async_save_crops(lg, imgList, embeddingList, camId, trackNo, predictions)

    def async_save_crops(self, lg, imgList, embeddingList, camId, trackNo=None, predictions=None):
        payload = [{
            "refImage": base64.b64encode(cv2.imencode('.jpg', img)[1].tostring()).decode(),
            "embeded": str(np.squeeze(embedding).tolist()),
            "cameraId": str(camId),
            "refTrackNo": str(trackNo),
            "prediction": prediction[0],
            "confidence": float("{0:.3f}".format(prediction[1]))
        } for img, embedding, prediction in zip(imgList, embeddingList, predictions)]

        # lg.debug(payload)
        savet = time.time()
        lg.info("TrkId-%d Pushing to DB" % trackNo)
        response = self.post("track-data/save-crops", payload)
        fullt = time.time() - savet
        lg.info("TrkId-%d Pushed to DB Pushing ThreadPool; Time taken by save crops %f" % (trackNo, fullt))
        self.print_stats()

    def save_recognized_crops(self, uniqueId, imgList, embeddingList, camId, trackNo=None, predictions=None):
        lg.debug("Submitted RecognizedCrops")
        if self.enable_thread_pool:
            self.thread_pool.submit(self.async_save_recognized_crops, lg, uniqueId, imgList, embeddingList, camId, trackNo,
                                    predictions)
        else:
            self.async_save_recognized_crops(lg, uniqueId, imgList, embeddingList, camId, trackNo, predictions)

    def async_save_recognized_crops(self, lg, uniqueId, imgList, embeddingList, camId, trackNo=None, predictions=None):
        payload = [{
            "refImage": base64.b64encode(cv2.imencode('.jpg', img)[1].tostring()).decode(),
            "embeded": str(np.squeeze(embedding).tolist()),
            "cameraId": str(camId),
            "refTrackNo": str(trackNo),
            "empid": uniqueId,
            "prediction": prediction[0],
            "confidence": float("{0:.3f}".format(prediction[1]))
        } for img, embedding, prediction in zip(imgList, embeddingList, predictions)]
        savet = time.time()
        lg.info("TrkId-%d Pushing to DB" % trackNo)
        response = self.post("track-data/save-recognized-crops", payload)
        fullt = time.time() - savet
        lg.info("TrkId-%d Pushed to DB Pushing ThreadPool; Time taken by save crops %f" % (trackNo, fullt))
        self.print_stats()

    def get_employee_info(self, uniqueId):
        url = "employee-info/get-employee-by-id/%s" % uniqueId
        response = self.get(url, {})
        return response

    def get_training_data(self):
        if self.enable_thread_pool:
            #TODO: test this
            response = list(self.thread_pool.map(self.get,[("track-data/training-data/false", None, False, False)]))[0]
        else:
            response = self.get("track-data/training-data/false", debug=False, asjson=False)

        lg.debug(response)
        response_content = ast.literal_eval(response.content.decode('utf-8'))
        return response_content
        # X, y = [], []
        # for label, embeddings in response.items():
        #     for embed in embeddings:
        #         if len(ast.literal_eval(embed)) > 10:
        #             X.append(np.array(ast.literal_eval(embed)))
        #             y.append(label)
        # X = np.array(X).reshape(len(X), 128)
        # y = np.array(y)

        # return X, y

    def get_employee_id_cluster_id(self):
        response = self.get_v1("track-data/get-emp-cluster-ids", debug=False, asjson=True)
        # lg.debug(response)
        return response

    def get_name(self, uniqueId):
        try:
            if self.enable_thread_pool:
                result = list(self.thread_pool.map(self.get_employee_info, [uniqueId]))[0]
                return result["fullName"]
            else:
                return self.get_employee_info(uniqueId)["fullName"]
        except Exception as e:
            lg.error(e)
            traceback.print_exc()
            return None

    def is_known_approved(self, uniqueId):
        return False, False

    def print_stats(self):
        msg = "Succesfully Saved:: Recognized:%d Unrecognized:%d;  Failed:: Recognized:%d Unrecognized:%d;"%(
                self.count_save_recognized_success,
                self.count_save_unrecognized_success,
                self.count_save_recognized_fail,
                self.count_save_unrecognized_fail
            )
        lg.debug(msg)


if __name__ == "__main__":
    bi = BackendAPI()

import datetime
import logging
import os
import socket
import subprocess
import time
from contextlib import closing
from glob import glob
from threading import Timer

lg = logging.getLogger(__name__)


def is_env_production():
    return os.path.exists("ui/flag-production-machine")


def most_recent_file(prefix, suffix):
    files = glob(prefix + "*" + suffix)
    files.sort(key=os.path.getmtime)

    if len(files) == 0:
        return None

    lg.debug("Most recent file: %s" % files[-1])
    return files[-1]


def is_time_between(begin_time, end_time, check_time=None):
    # credit: https://stackoverflow.com/questions/10048249/how-do-i-determine-if-current-time-is-within-a-specified-range-using-pythons-da
    # If check time is not given, default to current system time
    check_time = check_time or datetime.datetime.now().time()
    if begin_time < end_time:
        return check_time >= begin_time and check_time <= end_time
    else:  # crosses midnight
        return check_time >= begin_time or check_time <= end_time


class RepeatTimer(Timer):
    # credit: https://stackoverflow.com/questions/12435211/python-threading-timer-repeat-function-every-n-seconds
    # run timer every X seconds
    def run(self):
        while not self.finished.wait(self.interval):
            self.function(*self.args, **self.kwargs)


def is_env_production():
    return os.path.exists("ui/flag-production-machine")


def is_ramdisk_mounted(ramdisk_path):
    cmd = "df -Th | grep %s" % ramdisk_path
    try:
        ramdisk_mount_status = subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True).decode()
        lg.debug(ramdisk_mount_status)
        if "tmpfs" in ramdisk_mount_status:
            lg.debug("is_ramdisk_mounted = Yes")
            return True
        else:
            return False
    except Exception as e:
        lg.error(e)


def sudopwd():
    return ["recode", "admin@123"]


def run_cmd(cmd):
    lg.debug(subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True).decode())


def run_sudo_cmd(cmd):
    for passwd in sudopwd():
        try:
            scmd = "echo %s | sudo -S " % (passwd) + cmd
            result = subprocess.check_output(scmd, stderr=subprocess.STDOUT, shell=True).decode()
            lg.debug(result)
            return result
        except subprocess.CalledProcessError as e:
            lg.error(e.output)
            lg.error("Failed with %s" % passwd)


def unmount(path):
    cmd = "umount -l -v %s" % (path)
    if len(run_sudo_cmd(cmd)) == "":
        lg.debug("Successfully unmounted %s" % path)


def delpath(path):
    cmd = "rm -rv %s" % path
    run_cmd(cmd)


def prepare_ramdisk_path(ramdisk_path):
    clear_ramdisk(ramdisk_path)
    os.makedirs(ramdisk_path)
    return ramdisk_path


def clear_ramdisk(ramdisk_path):
    lg.debug("check if mount point exists")
    if os.path.exists(ramdisk_path):
        lg.debug("mount point exists; is it mounted?")
        if is_ramdisk_mounted(ramdisk_path):
            lg.debug("its mounted, unmounting")
            unmount(ramdisk_path)
        else:
            lg.debug("its not mounted")
            # unmount it
        lg.debug("clearing mount point")
        delpath(ramdisk_path)
    else:
        lg.debug("mount point %s doesnt exist" % ramdisk_path)

    lg.debug("cleared ramdisk")


def mount_ramdisk(size, ramdisk_path):
    lg.debug("attempting to mount ramdisk")
    cmd = "mount -t tmpfs -o size=%dM tmpfs %s" % (size, ramdisk_path)
    run_sudo_cmd(cmd)


def get_ramdisk(prefix, size=125, ramdisk_path="/tmp/ramdisk/face-rec"):
    ramdisk_path = os.path.join(ramdisk_path, prefix)

    prepare_ramdisk_path(ramdisk_path)
    mount_ramdisk(size, ramdisk_path)
    if is_ramdisk_mounted(ramdisk_path):
        return ramdisk_path
    else:
        raise RuntimeError("Failed to mount ramdisk at %s" % ramdisk_path)


def get_free_port():
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as s:
        s.bind(('', 0))
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        return s.getsockname()[1]


def run_and_wait(service):
    service.start()
    try:
        while True:
            time.sleep(100)
    except KeyboardInterrupt:
        pass
    finally:
        service.join()

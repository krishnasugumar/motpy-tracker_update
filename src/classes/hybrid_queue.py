#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Hybrid Queue stores first N items in RAM and rest in Disk to minimize Memory Usage"""

__author__ = "Azmath Moosa"

import queue
import random
import string
import threading
from multiprocessing import Queue

import persistqueue


class HybridQueue(object):
    def __init__(
            self,
            ram_maxsize=100,
            disk_maxsize=500,
            disk_prefix='/tmp/kai-'
    ):
        self.diskq_prefix = ''.join(random.choices(string.ascii_uppercase + string.digits, k=9))
        self.ram_maxsize = ram_maxsize
        self.disk_maxsize = disk_maxsize
        self.ramq = Queue(maxsize=ram_maxsize)
        self.diskq = persistqueue.Queue(path="/%s-%s" % (disk_prefix, self.diskq_prefix), maxsize=disk_maxsize)
        self.last_cur = 0
        self.last_cur_lock = threading.Lock()

    def put(self, item, block=True, timeout=None):
        if self.last_cur < self.ram_maxsize:
            # print("ram")
            self.ramq.put(item, block, timeout)
        elif (self.disk_maxsize + self.ram_maxsize) > self.last_cur >= self.ram_maxsize:
            # print("disk")
            self.diskq.put(item, block, timeout)
        elif self.last_cur >= (self.disk_maxsize + self.ram_maxsize):
            raise queue.Full

        with self.last_cur_lock:
            self.last_cur += 1

    def get(self, block=True, timeout=None):
        if self.last_cur == 0:
            raise queue.Empty
        elif (self.disk_maxsize + self.ram_maxsize) >= self.last_cur > self.ram_maxsize:
            # print("disk %d; ram %d" % (self.diskq.qsize(), self.ramq.qsize()))
            item = self.ramq.get()
            disk_item = self.diskq.get()
            self.ramq.put(disk_item)
        elif 0 < self.last_cur <= self.ram_maxsize:
            # print("ram %d" % self.ramq.qsize())
            item = self.ramq.get(block, timeout)
        with self.last_cur_lock:
            self.last_cur -= 1

        return item

    def get_nowait(self):
        return self.get(block=False)

    def put_nowait(self, item):
        self.put(item, block=False)

    def qsize(self):
        return self.last_cur

    def empty(self):
        return self.qsize() == 0


if __name__ == "__main__":
    q = HybridQueue(8, 25)
    # for tot in range(19,20):
    if 1:
        tot = 20
        try:
            for i in range(tot):
                q.put_nowait(("%d" % i) * 10)
                print("%d <--" % i, q.qsize())
        except:
            pass
        print("---")
        for i in range(tot):
            s = q.qsize()
            x = q.get_nowait()
            print(x, "<--", s, "-->", q.qsize())

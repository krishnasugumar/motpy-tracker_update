from enum import Enum

import numpy as np
from PIL import Image

from vap import base


class STANDARD_COLORS(Enum):
    AliceBlue=0;Chartreuse=1;Aqua=2;Aquamarine=3;Azure=4;Beige=5;Bisque=6;BlanchedAlmond=7;BlueViolet=8;BurlyWood=9;
    CadetBlue=10;AntiqueWhite=11;Chocolate=12;Coral=13;CornflowerBlue=14;Cornsilk=15;Crimson=16;Cyan=17;DarkCyan=18;
    DarkGoldenRod=19;DarkGrey=20;DarkKhaki=21;DarkOrange=22;DarkOrchid=23;DarkSalmon=24;DarkSeaGreen=25;DarkTurquoise=26;
    DarkViolet=27;DeepPink=28;DeepSkyBlue=29;DodgerBlue=30;FireBrick=31;FloralWhite=32;ForestGreen=33;Fuchsia=34;
    Gainsboro=35;GhostWhite=36;Gold=37;GoldenRod=38;Salmon=39;Tan=40;HoneyDew=41;HotPink=42;IndianRed=43;Ivory=44;
    Khaki=45;Lavender=46;LavenderBlush=47;LawnGreen=48;LemonChiffon=49;LightBlue=50;LightCoral=51;LightCyan=52;
    LightGoldenRodYellow=53;LightGray=54;LightGrey=55;LightGreen=56;LightPink=57;LightSalmon=58;LightSeaGreen=59;
    LightSkyBlue=60;LightSlateGray=61;LightSlateGrey=62;LightSteelBlue=63;LightYellow=64;Lime=65;LimeGreen=66;Linen=67;
    Magenta=68;MediumAquaMarine=69;MediumOrchid=70;MediumPurple=71;MediumSeaGreen=72;MediumSlateBlue=73;MediumSpringGreen=74;
    MediumTurquoise=75;MediumVioletRed=76;MintCream=77;MistyRose=78;Moccasin=79;NavajoWhite=80;OldLace=81;Olive=82;OliveDrab=83;
    Orange=84;OrangeRed=85;Orchid=86;PaleGoldenRod=87;PaleGreen=88;PaleTurquoise=89;PaleVioletRed=90;PapayaWhip=91;PeachPuff=92;
    Peru=93;Pink=94;Plum=95;PowderBlue=96;Purple=97;Red=98;RosyBrown=99;RoyalBlue=100;SaddleBrown=101;Green=102;SandyBrown=103;
    SeaGreen=104;SeaShell=105;Sienna=106;Silver=107;SkyBlue=108;SlateBlue=109;SlateGray=110;SlateGrey=111;Snow=112;
    SpringGreen=113;SteelBlue=114;GreenYellow=115;Teal=116;Thistle=117;Tomato=118;Turquoise=119;Violet=120;Wheat=121;White=122;
    WhiteSmoke=123;Yellow=124;YellowGreen=125;Black=126;


class TextAlign(Enum):
    TopLeft=0;TopCenter=1;TopRight=2;
    MiddleLeft=3;MiddleCenter=4;MiddleRight=5;
    BottomLeft=6;BottomCenter=7;BottomRight=8;


class DisplayText(object):
    def __init__(self,
            text: str = "",
            align: TextAlign = TextAlign.TopLeft,
            fontname: str = "",
            size: int = 10,
            color: STANDARD_COLORS = STANDARD_COLORS.Black,
            bgcolor: STANDARD_COLORS = None
            ):
        super(DisplayText, self).__init__()
        self.text = text
        self.align = align
        self.fontname = fontname
        self.size = size
        self.color = color
        self.bgcolor = bgcolor


class DisplayFrame(object):
    def __init__(self, plcvimage):
        if isinstance(plcvimage, Image):
            self.plimage = plcvimage
        elif isinstance(plcvimage, np.ndarray):
            self.plimage = Image.fromarray(plcvimage)

    def __enter__(self):
        return self.plimage

    def __exit__(self, exc_type, exc_val, exc_tb):
        return np.array(self.plimage)


class AttrDict(dict, base.UtilBase):
    """ allows accessing dict[key] using dict.key notation """
    def __init__(self, *args, **kwargs):
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self

    @classmethod
    def from_dict(cls, dict):
        obj = cls()
        obj.__dict__.update(dict)
        return obj
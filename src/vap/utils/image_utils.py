""" Image Manipulation Utility Functions """

import cv2
import numpy as np
from PIL import Image

import vap.predictions
from vap import base


class ImageUtil(base.UtilBase):
    """ Image Manipulation Utility Functions """
    def __init__(self):
        super().__init__()

    @staticmethod
    def load_cv_image(image_path):
        """ return cv2.imread """
        return cv2.imread(image_path)

    @staticmethod
    def load_pil_image(image_path):
        """ return pillow image """
        return Image.open(image_path)

    @staticmethod
    def to_pil(plcvimage):
        """ returns pil image """
        try:
            return ImageUtil.cv_to_pil(plcvimage)
        except:
            return plcvimage

    @staticmethod
    def to_cv(plcvimage):
        """ return cv image """
        try:
            return ImageUtil.pil_to_cv(plcvimage)
        except:
            return plcvimage

    @staticmethod
    def pil_to_cv(pilimage):
        """ converts pil image to opencv image; rgb to bgr """
        return np.array(pilimage.convert("RGB"))[:,:,::-1]

    @staticmethod
    def cv_to_pil(cvimage):
        """ converts opencv image to pil image; bgr to rgb """
        try:
            cvimage = cv2.cvtColor(cvimage, cv2.COLOR_BGRA2RGBA)
        except:
            cvimage = cv2.cvtColor(cvimage, cv2.COLOR_BGR2RGBA)

        return Image.fromarray(np.uint8(cvimage))

    @staticmethod
    def resize_keep_aspratio(cvimage, width=None, height=None, downsize_only=False, interpolation=cv2.INTER_CUBIC):
        """ resizes opencv image preserving aspect ratio

         Parameters
         ----------
         cvimage : ndarray
            OpenCV image array
         width : int
            set width of new image
         height: int
            set height of new image
         downsize_only : boolean
            if true, the resized image must be smaller than original, else
         """
        if width is None and height is None:
            return cvimage

        h, w = cvimage.shape[:2]
        ap = w/h
        if width is None and height is not None:
            width = ap*height
        elif width is not None and height is None:
            height = width/ap

        if downsize_only:
            if h < height and w < width:
                return cvimage

        return cv2.resize(cvimage, (int(width), int(height)), interpolation=interpolation)

    @staticmethod
    def resize_image(cvimage, width=None, height=None):
        "Resizes the opencv image based on height and width :AD:"
        if width is None or height is None:
            return cvimage
        else:
            return cv2.resize(cvimage, (width, height))

    @staticmethod
    def extract_image_patch(cvimage, bbox: vap.predictions.primitives.BBox, patch_shape=None):
        """Extract image patch from bounding box.

        Parameters
        ----------
        image : ndarray
            The full image.
        bbox : vap.predictions.primitives.BBox
        patch_shape : Optional[array_like]
            This parameter can be used to enforce a desired patch shape
            (height, width). First, the `bbox` is adapted to the aspect ratio
            of the patch shape, then it is clipped at the image boundaries.
            If None, the shape is computed from :arg:`bbox`.

        Returns
        -------
        ndarray | NoneType
            An image patch showing the :arg:`bbox`, optionally reshaped to
            :arg:`patch_shape`.
            Returns None if the bounding box is empty or fully outside of the image
            boundaries.

        """
        if not bbox.is_normalized:
            bbox = np.array(bbox.tlbr)
        else:
            ch, cw = cvimage.shape[:2]
            bbox = np.array(bbox.tlbr) * np.array([ch, cw, ch, cw])

        if patch_shape is not None:
            # correct aspect ratio to patch shape
            target_aspect = float(patch_shape[1]) / patch_shape[0]
            old_width = bbox[3] - bbox[1]
            old_height = bbox[2] - bbox[0]
            if 0:
                new_width = target_aspect * old_height
                bbox[1] -= (new_width - old_width) / 2
                bbox[3] += (new_width - old_width) / 2
            else:
                new_ht = old_width / target_aspect
                bbox[0] -= (new_ht - old_height) / 2
                bbox[2] += (new_ht - old_height) / 2

        # convert to top left, bottom right
        #bbox[2:] += bbox[:2]
        bbox = bbox.astype(np.int)

        # clip at image boundaries
        bbox[:2] = np.maximum(0, bbox[:2])
        bbox[2:] = np.minimum(np.asarray(cvimage.shape[:2][::-1]) - 1, bbox[2:])
        if np.any(bbox[:2] >= bbox[2:]):
            return None
        sy, sx, ey, ex = bbox
        image = cvimage[sy:ey, sx:ex]

        if patch_shape is not None:
            image = cv2.resize(image, tuple(patch_shape[::-1]))

        return image

    @staticmethod
    def is_blurry(img, threshold = 700):
        """
        returns if image is blurry

        Parameters
        ----------
        img : ndarray
            OpenCV compatible image
        threshold : int

        Returns
        -------
        Boolean indicating weather the image is blurry
        """
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        blur_val = cv2.Laplacian(gray, cv2.CV_64F).var()
        if blur_val > threshold:
            return False
        else:
            # cv2.putText(gray,"%f"%blur_val, (5,30), cv2.FONT_HERSHEY_COMPLEX, 0.5, (255,255,255),1,8)
            # cv2.imshow("isblur", gray)
            # cv2.waitKey(1000)
            return True
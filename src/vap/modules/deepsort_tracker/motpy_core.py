from typing import List
from ...utils.image_utils import ImageUtil
from ...predictions import BBoxPrediction
# from  ... base import SDBBox as sdbox
from ... import base
from ... import predictions
from .motpy import Detection, MultiObjectTracker, NpImage, Box
import numpy as np
# from motpy.core import setup_logger
# from motpy.detector import BaseObjectDetector
# from motpy.testing_viz import draw_detection, draw_track

class MotPyTrackerModuleBase(base.VAPModuleBase):
    """ Ultimate base class for all MotpyTracker modules """
    pass
class MotpyTracker(MotPyTrackerModuleBase):
    """ Tracker based on DeepSORT Algorithm """

    def __init__(
            self,
            dt= 1/10,
            track_deletion_callback=None,
            track_confirmation_callback=None,
            start_id=1,
            model_spec=None
    ):
        """

        Parameters
        ----------
        dt : int
            frame per second details
        track_deletion_callback: function
            A callback function with signature `def del_callback(track)` that is called every time a track is deleted
        track_confirmation_callback: function
            A callback function with signature `def conf_callback(track)` that is called every time a track is confirmed stable
        start_id: int
            Track Ids are assigned starting from this number
        model_spec: dict
            model specification details and config
        """
        super(MotpyTracker, self).__init__()

        self.tracker = MultiObjectTracker(dt=dt, model_spec=model_spec,
            # track_deletion_callback=track_deletion_callback,
            # track_confirmation_callback=track_confirmation_callback,
            start_id=start_id
        )

    def update(self, cvimage, detections: List[BBoxPrediction]):
        """
        Updates tracking state with newer detections

        Parameters
        ----------
        cvimage : ndarray
            The OpenCV image on which detection was performed
        detections : List
            List of detections

        Returns
        -------
        List [ `BBoxPrediction` ] with each detections `info` attribute set to a `Track` object
        """
        # # print("encoder predict")
        # features = self.encoder.predict(cvimage, detections)
        # # print("cropping")
        for d in detections:
            d.info.feature = []
        #
        # # print("cropped done")
        # self.tracker.predict()
        # # print("tracker.predict returned")
        # tracked_detections = self.tracker.update(detections)
        # # print("tracker.update returned")
        tracked_detections = []
        # # preview the boxes on frame
        height = cvimage.shape[0]
        width = cvimage.shape[1]
        if detections:
            for face_list in detections:
                confidence = face_list.score
                # det = {}
                # det['box'] = []
                # print(det)
                bbox_det = face_list.bbox_asis
                feature = face_list.info.feature
                norml_box = np.array([bbox_det.xmin, bbox_det.ymin, bbox_det.xmax, bbox_det.ymax])
                print("before", norml_box)
                tracked_detections.append(Detection(box=norml_box, score=confidence,feature=feature))

        self.tracker.step(tracked_detections)
        tracker_detections = self.tracker.active_tracks(min_steps_alive=8)
        # conversion into vap bbox format
        bbx_tracked_detections = []
        for track_mot in tracker_detections:
            # draw_track(motpy_frame, track_mot)
            label = "Face"
            bbox_det = track_mot.box
            print('after',bbox_det)
            bbox_det = np.array(bbox_det, dtype='float32')
            print(bbox_det)
            # bbox = track_mot.bbox.scaled(*cvimage.shape[:2])
            ymax = bbox_det[2]/width
            if ymax > 1.0:
                ymax = 1.0
            xmax = bbox_det[3]/height
            if xmax > 1.0:
                xmax = 1.0
            bbox_norm = (bbox_det[1]/height, bbox_det[0]/width,xmax, ymax)
            # bbox_norm[bbox_norm > 1.0] = 1.0
            print('after norm', bbox_norm)
            bbox_norm = np.array(bbox_norm, dtype='float32')
            # print(bbox_norm)
            # print(track_mot.box)
            # track_id = track_mot.id
            score = track_mot.score
            feature = []
            # ih = bbox_det[3]*height - bbox_det[1]*height
            # iw = bbox_det[2]*width - bbox_det[0]*width
            track_modified = track_mot
            # track_modified.is_confirmed= True
            # track_modified.track_since_update = 0
            # track_modified.confirmed_age = 1
            info = {}
            info['captions'] = {}
            info['crop'] = {}
            info['feature'] = []
            info['track'] = track_modified
            print(bbox_det[0] , bbox_det[1] , bbox_det[3] , width , bbox_det[2] , height)
            if bbox_det[0] > 0 and bbox_det[1] > 0 and bbox_det[3] <= width and bbox_det[2] <= height:
                detection = predictions.BBoxPrediction.from_TFModel(bbox_norm,
                    label,
                    score,
                    height,
                    width,
                )
                print(detection)
                detection.info.track = track_modified
                bbx_tracked_detections.append(detection)
        return bbx_tracked_detections

    def clear(self):
        """ clear all tracks """
        return self.tracker.cleanup_trackers()

    @property
    def tracks(self):
        return self.tracker.active_tracks()
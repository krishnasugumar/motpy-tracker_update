import logging
import os
import sys
from logging.handlers import RotatingFileHandler

from . import multiprocessing_logging


def get_root_logger(
        log_name="kamerai",
        log_dir="logs/",
        log_size_megabytes=50,
        log_rotation_count=15,
        log_format=\
                "%(asctime)s %(module)s-%(lineno)d [PID:%(process)d %(processName)s] [%(levelname)-5.5s]  %(message)s"\
        ):
    """
    Initialize logger with multiprocessing support, file-rotation, max-size and logging format

    Parameters
    ----------
    log_name : str
        Name of log file
    log_dir : str
        Path to log file direcotry
    log_size_megabytes : int
        Max size of log file in MB
    log_rotation_count : int
        After log_size is reached, the current file is renamed with an integer suffix like .1,.2 etc. and writehead
        moved to a new file.
        Max number of files that should be created after which the last file is discarded.
    log_format : str
        Specify logging format;
        Default is `%(asctime)s %(module)s-%(lineno)d [PID:%(process)d %(processName)s] [%(levelname)-5.5s] %(message)s`

        Refer [Log-Record-Attributes](https://docs.python.org/3/library/logging.html#logrecord-attributes)

    Returns
    -------
    A handle to a logger object whose `.info` `.warning` `.debug` `.error` `.critical` methods can be invoked to log
    """
    lg = logging.getLogger()

    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    log_formatter = logging.Formatter(
        log_format)

    file_handler = RotatingFileHandler("{0}/{1}.log".format(log_dir, log_name), maxBytes=(1024 * 1024 * log_size_megabytes),
                                       backupCount=log_rotation_count)
    file_handler.setFormatter(log_formatter)
    lg.addHandler(file_handler)

    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(log_formatter)
    lg.addHandler(console_handler)

    lg.setLevel(logging.DEBUG)
    multiprocessing_logging.install_mp_handler(logger=lg)

    return lg

def getLogger(scope):
    """
    Shadows python `logging.getLogger` for convenience

    Usage
    ------
    `lg = vap.modules.logging.getLogger(__name__)`

    Parameters
    ----------
    scope: str
        It should be the scope of the calling module - Pass `__name__`

    Returns
    -------
    python logging object initialized with passed scope
    """
    return logging.getLogger(scope)

""" Convenient wrapper for python logging module with preconfigured rotation+file+console handlers to get going faster.
  Support for multiprocessing.  Check `logging_init` for documentation

Usage
-----

In `root.py`:

```
from vap.modules import logging

lg = logging.get_root_logger(
        log_name="kamerai",
        log_dir="logs/"
        )

lg.info("logger initialized")
```

In `submodule.py`:

```
from vap.modules import logging

lg = logging.getLogger(__name__)

lg.info("logger initialized")
```
"""
from . logging_init import *

"""
Standard CNN Output primitive definitions ex, Bbox, Poly etc.
"""
import numpy as np

from vap import base


class BBox(base.VAPBase):
    """ Bounding Box """
    def __init__(self, xmin, ymin, xmax, ymax, normalized=True):

        self._is_normalized = normalized
        self._xmin = xmin
        self._xmax = xmax
        self._ymin = ymin
        self._ymax = ymax


    def __repr__(self):
        return "BBox {btype}<xmin:{xmin},xmax:{xmax},ymin:{ymin},ymax:{ymax}>".format(btype=self.btype,
            xmin=self._xmin,xmax=self._xmax,ymin=self._ymin,ymax=self._ymax)

    @classmethod
    def from_tfbox(cls, tfbox):
        """
        Initialize new BBox from TFModel output

        Parameters
        ----------
        tfbox :  tuple
            (ymin, xmin, ymax, xmax)

        Returns
        -------
        New Bbox instance
        """
        ymin, xmin, ymax, xmax = tfbox
        return cls(xmin, ymin, xmax, ymax)

    @classmethod
    def as_absolute(cls, xmin, ymin, xmax, ymax):
        """
        Initialize new BBox with absolute values

        Parameters
        ----------
        xmin : int
            top-left x
        ymin : int 
            top-left y 
        xmax : int 
            bottom-right x
        ymax : int 
            bottom-right y 

        Returns
        -------
        New Bbox instance

        """
        xmin, ymin, xmax, ymax = map(int, [xmin, ymin, xmax, ymax])
        return cls(xmin, ymin, xmax, ymax, normalized=False)

    @property
    def is_normalized(self):
        """ returns True/False if bbox coordinates are normalized """
        return self._is_normalized

    @property
    def btype(self):
        """ returns string indicating bbox coordinates type - Normalized or Absolute"""
        return "Normalized" if self._is_normalized else "Absolute"

    @property
    def xmin(self):
        return self._xmin

    @property
    def xmax(self):
        return self._xmax

    @property
    def ymin(self):
        return self._ymin

    @property
    def ymax(self):
        return self._ymax

    @property
    def tlbr(self):
        return self.ymin, self.xmin, self.ymax, self.xmax


    @property
    def tlwh(self):
        return self.ymin, self.xmin, self.width, self.height

    @property
    def ltwh(self):
        return self.xmin, self.ymin, self.width, self.height

    @property
    def xyah(self):
        return ((self.xmin+self.xmax)/2), ((self.ymin+self.ymax)/2), self.width/self.height, self.height

    @property
    def width(self):
        return self._xmax - self._xmin

    @property
    def height(self):
        return self._ymax - self._ymin

    @property
    def area(self):
        return self.height*self.width

    @property
    def diag(self):
        return ((self.height**2 + self.width**2)**0.5)

    @property
    def asp_ratio(self):
        return (self.width/self.height)

    def scaled(self, h, w):
        return BBox.as_absolute(self.xmin*w, self.ymin*h, self.xmax*w, self.ymax*h)


class RBBox(base.VAPBase):
    """ Rotated Bounding Box """
    def __init__(self, ptopleft, ptopright, pbottomright, pbottomleft):
        self._ptopleft = ptopleft
        self._ptopright = ptopright
        self._pbottomright = pbottomright
        self._pbottomleft = pbottomleft

    @property
    def ptopleft(self):
        return self._ptopleft

    @property
    def ptopright(self):
        return self._ptopright

    @property
    def pbottomright(self):
        return self._pbottomright

    @property
    def pbottomleft(self):
        return self._pbottomleft

    @property
    def cv2boxpoints(self):
        return np.array([self.pbottomright, self.pbottomleft, self.ptopleft, self.ptopright])


    @property
    def tlbr(self):
        raise NotImplementedError

    @property
    def tlwh(self):
        raise NotImplementedError

    @property
    def ltwh(self):
        raise NotImplementedError

    @property
    def xyah(self):
        raise NotImplementedError

    @property
    def width(self):
        raise NotImplementedError

    @property
    def height(self):
        raise NotImplementedError

    @property
    def area(self):
        raise NotImplementedError

    @property
    def diag(self):
        raise NotImplementedError

    @property
    def asp_ratio(self):
        raise NotImplementedError
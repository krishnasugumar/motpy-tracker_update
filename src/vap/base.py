"""
Base Classes for All VAP Modules
"""

class VAPBase(object):
    """ Ultimate base class for all VAP modules """
    def __init__(self):
        pass 

class EstimatorConfigBase(VAPBase):
    """ Base Class representing ML/DL model configuration"""
    def __init__(self):
        super(EstimatorConfigBase, self).__init__()

class CNNEstimatorConfigBase(EstimatorConfigBase):
    """ Base Class representing CNN model configuration """
    def __init__(self):
        super(CNNEstimatorConfigBase, self).__init__()

class MLEstimatorConfigBase(EstimatorConfigBase):
    """ Base Class representing ML model configuration :AD:"""
    def __init__(self):
        super(MLEstimatorConfigBase, self).__init__()

class EstimatorBase(VAPBase):
    """ Base Class representing an ML/DL model """
    def __init__(self, config: EstimatorConfigBase):
        super(EstimatorBase, self).__init__()
        self._config = config

class CNNEstimatorBase(EstimatorBase):
    """ Base Class representing a CNN model """
    def __init__(self, config: CNNEstimatorConfigBase):
        super(CNNEstimatorBase, self).__init__(config)
        self._config = config

class MLEstimatorBase(EstimatorBase):
    """ Base Class representing a ML model :AD:"""
    def __init__(self, config: MLEstimatorConfigBase):
        super(MLEstimatorBase, self).__init__(config)
        self._config = config


class EstimatorOutputBase(VAPBase):
    """ Base class representing a ML/DL model output """
    def __init__(self):
        super(EstimatorOutputBase, self).__init__()

class CNNEstimatorOutputBase(EstimatorOutputBase):
    """ Base class representing a CNN model output """
    def __init__(self):
        super(CNNEstimatorOutputBase, self).__init__()

class MLEstimatorOutputBase(EstimatorOutputBase):
    """ Base class representing a CNN model output :AD:"""
    def __init__(self):
        super(MLEstimatorOutputBase, self).__init__()

class ExceptionBase(Exception, VAPBase):
    """ Base class for throwing exceptions """
    pass

class UtilBase(VAPBase):
    """ Base class for all utility functions """
    pass

class SrcFeedBase(VAPBase):
    """ Base class for all feed sources """
    pass

class VAPModuleBase(VAPBase):
    """ Base class for all VAPModules """
    pass
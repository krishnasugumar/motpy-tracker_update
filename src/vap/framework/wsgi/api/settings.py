
import json
import logging
import os
import shutil

from flask import Blueprint, render_template
from flask_socketio import emit

from .base import WSGIBaseAPI

lg = logging.getLogger(__name__)


class AppSettingsAPI(WSGIBaseAPI):

    def __init__(self, config_json_path, config_schema_path=None):
        """ Setting API that offers a web interface for editing json configuration files
        
        Parameters
        ----------
        config_json_path: str
            path to config json
        config_schema_path: str
            path to config json schema - a special file that describes config parameters; needed for validation
        """

        super().__init__(websocket_enabled=True)
        self.json_path = config_json_path
        self.json_schema_path = config_schema_path

    def get_blueprint(self, websocket_handle, apiroot=""):
        """ returns flask.Blueprint object """

        settings_api = Blueprint(
            apiroot,
            __name__,
            template_folder="templates/app_settings",
            static_folder="templates/app_settings"
        )

        expose = websocket_handle.on

        @settings_api.route("/")
        def index():
            if not (self.json_schema_path is None):
                with open(self.json_schema_path) as jsschema:
                    json_schema_templates = json.load(jsschema)
                    json_templates = json_schema_templates["templates"]
                    json_schema = json_schema_templates["schema"]
            else:
                json_schema = {}
                json_schema_templates = {}
            with open(self.json_path) as jsconf:
                return render_template(
                    "settings.html",
                    apiroot=apiroot,
                    json_config=jsconf.read(),
                    json_schema=json.dumps(json_schema),
                    json_templates=json.dumps(json_templates)
                )

        @settings_api.route("/dep/<path:path>")
        def serve_file(path):
            return settings_api.send_static_file(path)

        @expose('save_settings', namespace=apiroot)
        def save_settings(data):
            try:
                json_config = data['json']

                path = self.json_path
                if os.path.exists(path):
                    backup_path = path[:-4] + "bk.json"
                    shutil.copy(path, backup_path)

                with open(path, "w") as jsf:
                    jsf.write(json.dumps(json_config, indent=4))

                emit('saveCompleted', {'status': 'ok'})
                lg.debug("Settings saved to %s" % path)
            except:
                lg.error(e)

        return settings_api

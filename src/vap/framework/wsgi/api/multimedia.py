import logging
import os
import re

from flask import Blueprint, Response, request, send_from_directory, redirect

from .base import WSGIBaseAPI

logging.getLogger('socketio').setLevel(logging.ERROR)

lg = logging.getLogger(__name__)


class MultiMediaAPI(WSGIBaseAPI):
    """ API Collection for Media Streaming Routes """

    KEY_FEED_URL = "feed_url"

    def __init__(self, image_feed=False, video_feed=False, live_feed=True, live_feed_dict={}):
        """ Multimedia API serves images, videos, live streams 

        Parameters
        ----------
        image_feed: boolean
            Enable /image_feed route to serve static images
        video_feed: boolean
            Enable /video_feed route to serve recorded videos
        live_feed: boolean
            Enable /live_feed route to serve live mjpg streams
        live_feed_dict: dict
            Dictionary with camera id and corresponding /path to retrieve mjpg streams        
        """
        super().__init__()
        self.image_feed = image_feed
        self.video_feed = video_feed
        self.live_feed = live_feed
        self.live_feed_dict = live_feed_dict

    def get_blueprint(self):
        """ returns a flask.Blueprint object initialized with configured routes """

        image_feed = self.image_feed
        video_feed = self.video_feed
        live_feed = self.live_feed
        live_feed_dict = self.live_feed_dict

        media_api = Blueprint('multimedia', __name__)

        if live_feed:
            @media_api.route("/live_feed/<camid>", methods=['GET'])
            def get_live_feed(camid):
                try:
                    if camid in live_feed_dict:
                        feed_url = live_feed_dict[camid]
                        return redirect(feed_url)
                    else:
                        return Response("Camera not found", 404)
                except:
                    return Response("No cameras configured; Please add %s key \
                        to live_feed_dict class init param with dictionary of camid:'%s' mappings" % (live_feed_dict, MultiMediaAPI.KEY_FEED_URL), 404)

        if video_feed:
            @media_api.route('/video_feed/', methods=['GET'])
            def get_video_file():
                range_header = request.headers.get('Range', None)
                fullpath = request.args.get("videofile")
                byte1, byte2 = 0, None
                if range_header:
                    match = re.search(r'(\d+)-(\d*)', range_header)
                    groups = match.groups()

                    if groups[0]:
                        byte1 = int(groups[0])
                    if groups[1]:
                        byte2 = int(groups[1])

                chunk, start, length, file_size = self._get_chunk(
                    fullpath, byte1, byte2)
                resp = Response(chunk, 206, mimetype='video/mp4',
                                content_type='video/mp4', direct_passthrough=True)
                resp.headers.add(
                    'Content-Range', 'bytes {0}-{1}/{2}'.format(start, start + length - 1, file_size))
                print(resp)
                return resp

        if image_feed:
            @media_api.route('/image_feed/', methods=['GET'])
            def get_image_file():
                range_header = request.headers.get('Range', None)
                fullpath = request.args.get("imagefile")
                directory = os.path.dirname(fullpath)
                image = os.path.basename(fullpath)

                return send_from_directory(directory, image)

        return media_api

    def _get_chunk(self, fullpath="", byte1=None, byte2=None):
        try:
            file_size = os.stat(fullpath).st_size
            start = 0
            length = 1024000

            if byte1 < file_size:
                start = byte1
            if byte2:
                length = byte2 + 1 - byte1
            else:
                length = file_size - start

            with open(fullpath, 'rb') as f:
                f.seek(start)
                chunk = f.read(length)
            return chunk, start, length, file_size
        except:
            return 0, 0, 0, 0

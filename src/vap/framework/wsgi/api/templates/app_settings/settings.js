

var container = document.getElementById("jsoneditor");

const templates_keys = {{json_templates}};


const schema = {{json_schema}};

const options = {
    schema: schema,
    name: "Settings",
    mode: 'tree',
    modes: ['code', 'text', 'tree', 'preview'],
    templates: [
        {
            text: 'Camera',
            title: 'Add a new Camera',
            className: 'jsoneditor-type-object',
            field: 'camera-unique-id',
            value: {
                'src': 'rtsp://',
                'calibration': {
                "perspective_points": [
                    [397, 62],
                    [528, 72],
                    [299, 309],
                    [100, 247]
                ],
                "flattened_points": [
                    [100, 200],
                    [200, 200],
                    [200, 600],
                    [100, 600]
                ],
                "px_distance_threshold": 1000,
                "topview": {
                    "width": 150,
                    "height": 150,
                    "offset_x": 70,
                    "offset_y": 10,
                    "zoom": 0.8          
                }
            },
            }
        }
    ],
    onCreateMenu: function (items, node) {
        const path = node.path
  
        // log the current items and node for inspection
        // console.log('items:', items, 'node:', node)

        InsertItem = Object.keys(templates_keys); //["inputs"]
        RemoveMenuSubItems = ["Transform", "Type", "Extract", "Auto", "Array", "Object", "String"]

                
        function filterMenu(items){
            items = items.filter((item) => {
                return (!RemoveMenuSubItems.includes(item.text) && item.type !== 'separator')
            })
            return items;
        }
        items = filterMenu(items);
        items.forEach((item, index, items) => {
            if ("submenu" in item){
                items[index].submenu = filterMenu(item.submenu);
            }            
        })

        items = items.filter((item)=> {
            if (node.path.length == 2){
                if (InsertItem.includes(node.path[node.path.length - 2])){
                    console.log(item.text);
                    return true;
                }
                }
            return (item.text != "Insert") && (item.text != "Append");
        })
        if (!(InsertItem.includes(node.path[node.path.length - 2]))){
            
        }
  
        // console.log('items:', items, 'node:', node)
  

        return items
      }    
  }

var editor = new JSONEditor(container, options);
var json = {{json_config}} 
editor.set(json);

// socket stuff
var socket = io.connect('http://' + document.domain + ':' + location.port + '{{apiroot}}');
// verify our websocket connection is established
socket.on('connect', function () {
    console.log('Websocket connected!');    
});
socket.on('saveCompleted', function (msg) {
    if (msg['status'] == 'ok') {
        status = "save was successul"
    } else {
        status = "save failed! check log for details"
    }
    M.toast({ html: status });
})


function saveSettings(){
    data = editor.get();
    socket.emit('save_settings', {'json': data});
}
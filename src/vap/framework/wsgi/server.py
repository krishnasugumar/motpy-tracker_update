from flask import Flask
from flask_socketio import SocketIO
import logging

logging.getLogger('socketio').setLevel(logging.ERROR)
logging.getLogger('engineio').setLevel(logging.ERROR)

class WSGIApiServer(object):

    def __init__(
            self,
            host="localhost",
            port=5000,
            context=__name__,
            blueprints={}
    ):
        """ This class initializes a Flask application and manages it
        
        Parameters
        ----------
        host: str
            hostname (default:localhost)
        port: int
            port number
        context: str
            a string passed to Flask() constructor (default:`__name__`)
        blueprints: dict
            a dictionary with path:flask.Blueprint api instances. The blueprint apps are mounted
            at the path.
            Ex. MultimediaAPI, SettingsAPI etc.
        """

        self.host = host
        self.port = port
        self.app = Flask(context)
        self.app.config['TEMPLATES_AUTO_RELOAD'] = True

        self.websocket_app = None

        for path, routes in blueprints.items():
            print("registering route: %s" % path)
            if type(routes) is list:
                for route in routes:
                    self._register_blueprint(route, path)
            else:
                self._register_blueprint(routes, path)

    def _register_blueprint(self, bprint, path):
        if bprint.websocket_enabled:
            if self.websocket_app is None:
                self.websocket_app = SocketIO()
            print("registering socketed route: %s" % path)
            self.app.register_blueprint(
                bprint.get_blueprint(
                    self.websocket_app,
                    apiroot=path
                ),
                url_prefix=path
            )
        else:
            print("registering nonsocket route: %s" % path)
            self.app.register_blueprint(
                bprint.get_blueprint(),
                url_prefix=path
            )

    def route(self, rule, **options):
        """ forwards to app.route decorator """

        def decorator(f):
            endpoint = options.pop("endpoint", None)
            self.app.add_url_rule(rule, endpoint, f, **options)
            return f

        return decorator

    def run(self):
        """ Runs the flask/socketio app (blocks)"""
        if self.websocket_app is not None:
            self.websocket_app.init_app(self.app)
            self.websocket_app.run(self.app, host=self.host, port=self.port)
        else:
            self.app.run(host=self.host, port=self.port)

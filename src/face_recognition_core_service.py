import vap
from classes.handlers.camera_process import CameraProcess
from classes.handlers.feed_display_process import FeedDisplayProcess
from classes.handlers.recognition_process import RecognitionProcess
from classes.handlers.recognition_result_handler_process import \
    RecognitionResultHandlerProcess
from classes.handlers.save_data_process import SaveDataProcess
from vap.config import AppConfig
from vap.framework.kai_process import KaiProcess, run_and_wait, MultiprocessingProxyManager
from vap.framework.wsgi.api import MultiMediaAPI
from vap.framework.wsgi.server_process import WSGIApiServerProcess
from vap.modules import logging

lg = logging.getLogger(__name__)


class VisionService(KaiProcess):

    def __init__(self):
        self.config = vap.config.AppConfig("config.json")
        super().__init__(
            health_status_port=self.config.management.face_recognition_core_health_status_port,
            stop_subprocs_on_stop=True,
            join_subprocs_on_sigstop=True
        )

    def unique_name(self) -> str:
        return "FaceRecognitionCoreService"

    def monitored_status_params(self) -> dict:
        return {"status": "OK"}

    def run(self):
        super().run()

        lg.debug("Initializing Main Process")
        app_config = AppConfig("config.json")

        mp_manager = MultiprocessingProxyManager()

        recognition_results_dict = mp_manager.dict()
        cam_count = len(app_config.input)

        cam_buffer_qs = [mp_manager.Queue(maxsize=100) for i in range(cam_count)]
        feed_display_qs = [mp_manager.Queue(maxsize=120) for i in range(cam_count)]
        backround_recognition_crops_to_recognize_q = mp_manager.Queue(maxsize=1000)
        backround_recognition_results_q = mp_manager.Queue(maxsize=100)
        save_q = mp_manager.Queue(maxsize=3000)
        track_state_update_q = mp_manager.Queue(maxsize=100)
        recognition_state_update_qs = [mp_manager.Queue(maxsize=100) for i in range(cam_count)]

        self.q_handles = cam_buffer_qs + feed_display_qs + [backround_recognition_crops_to_recognize_q] + \
                         [backround_recognition_results_q] + [save_q] + [track_state_update_q] + \
                         recognition_state_update_qs

        cam_handlers = [
            CameraProcess(
                app_config,
                cam_idx,
                cam_id,
                app_config.input[cam_id],
                cam_buffer_qs[cam_idx],
                track_state_update_q,
                backround_recognition_crops_to_recognize_q,
                feed_display_qs[cam_idx]
            ) for cam_idx, cam_id in enumerate(app_config.input)
        ]

        feed_display_handlers = [
            FeedDisplayProcess(
                app_config,
                cam_idx,
                cam_id,
                app_config.input[cam_id],
                feed_display_qs[cam_idx],
                recognition_results_dict,
                recognition_state_update_qs[cam_idx]
            ) for cam_idx, cam_id in enumerate(app_config.input)
        ]

        recognition_handlers = [
            RecognitionProcess(
                app_config,
                backround_recognition_crops_to_recognize_q,
                backround_recognition_results_q
            ) for i in range(0, 2)
        ]

        recognition_result_handlers = [
            RecognitionResultHandlerProcess(
                app_config,
                backround_recognition_results_q,
                track_state_update_q,
                save_q,
                recognition_state_update_qs
            )
        ]

        save_handlers = [
            SaveDataProcess(
                app_config,
                save_q
            )
        ]

        feed_route_handler = [WSGIApiServerProcess(
            port=app_config.management.live_feed_router_port,
            blueprints={
                '': MultiMediaAPI(live_feed=True, live_feed_dict={
                    k: "http://localhost:%d/video_feed" % v["port_no"] for k, v in app_config.input.items()
                })
            }
        )]

        self.subproc_handles = cam_handlers + \
                               recognition_handlers + \
                               recognition_result_handlers + \
                               save_handlers + \
                               feed_display_handlers + feed_route_handler

        lg.info("Health Status Port:%s" % self.health_status_port)
        self.run_subprocs()
        self.wait_for_shutdown_event()

        lg.info("Shutdown Queue Manager")
        mp_manager.shutdown()
        lg.info("Exiting Main")


if __name__ == "__main__":
    lg = logging.get_root_logger(log_name="ayeAccess.FR")
    vision_service = VisionService()
    run_and_wait(vision_service)

    print("main_multi exited")

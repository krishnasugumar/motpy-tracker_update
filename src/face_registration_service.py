import os
import ast
import pickle
from collections import Counter
import cv2
from imutils import paths
import pandas as pd 
import numpy as np
from clf_utils.db_utils import DbUtils
from classes.face_recogniser import FaceRecogniser
from vap.framework.kai_process import KaiProcess, run_and_wait
from flask import Flask, request, Response
import vap
import traceback
import json
from vap.modules import logging
from datetime import datetime, timedelta
from vap.config import AppConfig

lg = logging.getLogger(__name__)

appconfig = {}
try:
    config = AppConfig("config.json")
    ENTRY_CAMS = config.recognition.registration.entry_cams #["ppk-0"]
    DIST_THRESHOLD = config.recognition.registration.matching_dist_threshold # 0.48
    CLOSEST_LIMIT = config.recognition.registration.closest_limit #15
    POP_APPROACH = True
    TIME_OFFSET = config.recognition.registration.time_offset #30
except Exception as e:
    lg.debug(e)

VALID_EMBED = 'valid_embed'
VALID_CAMS = "valid_cams"
VALID_TIME = "valid_time"
EMP_PROFILE = 'registrationemployeeprofile' #'registrationemployeeprofile' #"employeeprofile"
CREATED_DATE = "createddate"
EMBEDING = "embeded"
path = "/media/ahamad/work/projects/tvs/face-rec/face-data/matching-smapleip/Input.txt"



def load_data(path):
    with open(path) as f:
        data = json.load(f)
    return data

def dist(a, b):
	return np.linalg.norm(a-b, axis=1)

def get_embedding(faceprofile, fr):
    face_embedding = fr.get_embedding(faceprofile)
    return face_embedding

def get_timeobj(sample_time):
    return datetime.strptime(sample_time, '%Y-%m-%d %H:%M:%S')

def view_faceprofile(df_mobile):
    for emp_id in df_mobile["emp_id"].to_list():
        face = df_mobile[df_mobile["emp_id"] == emp_id][EMP_PROFILE][0]
        embed = df_mobile[df_mobile["emp_id"] == emp_id][EMBEDING]
        status = df_mobile[df_mobile["emp_id"] == emp_id][VALID_EMBED]
        print(face.shape, face.shape)
        cv2.imshow("face", face)
        cv2.waitKey(0)


def matching(df_mobile, df_system):
    match_list = []

    if len(df_mobile) == 0 or len(df_system) == 0:
        lg.debug("Insufficient data to process")
        return match_list

    mobile_embeds = df_mobile[EMBEDING].tolist()
    mobile_names = df_mobile['emp_id'].tolist()

    system_embeds = df_system[EMBEDING].tolist() 
    system_names = df_system["cluster_id"].tolist()

    for i in range(len(mobile_embeds)):
        temp_dict = {}
        if len(system_embeds) > 0:
            dists = dist(mobile_embeds[i], system_embeds)

            idxs = np.argsort(dists)[:CLOSEST_LIMIT]
            close_ids = np.array(system_names)[idxs]
            final_id = Counter(close_ids).most_common()[0][0]
            confidence = Counter(close_ids).most_common()[0][1]/float(CLOSEST_LIMIT)

            temp_dict["employeeId"] = mobile_names[i]
            temp_dict["cluster_id"] = final_id
            temp_dict["confidence"] = confidence
            temp_dict["trackdataId"] = df_system[df_system["cluster_id"] == final_id]["trackdata_id"].to_list()

            if min(dists) < DIST_THRESHOLD:
                temp_dict["status"] = "Matched"

            else:             
                temp_dict["status"] = "Recommended"

            if POP_APPROACH:
                indexes = np.where(np.array(system_names) == final_id)[0]
                system_names = [name for i, name in enumerate(system_names) if i not in indexes]
                system_embeds = [name for i, name in enumerate(system_embeds) if i not in indexes]

        else:
            temp_dict["employeeId"] = mobile_names[i]
            temp_dict["cluster_id"] = None
            temp_dict["confidence"] = 0.
            temp_dict["trackdataId"] = None
            temp_dict["status"] = "NoSytemData"

        lg.debug("Matching_pair%d(%d)-%s:: dist-%0.2f(%0.2f)"%
                    (i+1, len(mobile_embeds), str(temp_dict), min(dists), DIST_THRESHOLD))

        match_list.append(temp_dict)
    return match_list


def df_system_process(df_system):
    lg.debug("Recieved DB_records-%d clusters-%d"%(len(df_system), len(Counter(df_system["cluster_id"])))) 
    df_system[EMBEDING] = df_system[EMBEDING].apply(lambda r: np.array(ast.literal_eval(r)))
    df_system[VALID_EMBED] = df_system[EMBEDING].apply(lambda r: r.shape[0] > 1)
    df_system_invalidembd = df_system[df_system[VALID_EMBED] == False]
    df_system = df_system[df_system[VALID_EMBED] == True]
    lg.debug("valid embeddings-%d Invalid-%d"%(len(df_system), len(df_system_invalidembd)))

    df_system[VALID_CAMS] = df_system["camera_id"].apply(lambda r: r in ENTRY_CAMS)
    df_system_othercams = df_system[df_system[VALID_CAMS] == False]
    df_system = df_system[df_system[VALID_CAMS] == True]
    lg.debug("valid cam records-%d Invalid-%d selected_cams %s recieved cams %s"%
            (len(df_system), len(df_system_invalidembd), str(ENTRY_CAMS), str(Counter(df_system["camera_id"]))))
    
    df_system[CREATED_DATE] = df_system[CREATED_DATE].apply(lambda r: get_timeobj(r[:19]))
    df_system.sort_values(by=[CREATED_DATE]) # df_system.sort_values(CREATED_DATE)

    return df_system


def df_mobile_process(df_mobile, fr):
    lg.debug("Recieved Mobile records-%d"%(len(df_mobile)))

    df_mobile[EMP_PROFILE] = df_mobile[EMP_PROFILE].apply(lambda r: DbUtils.stringToRGB(r))
    df_mobile[EMBEDING] = df_mobile[EMP_PROFILE].apply(lambda r: get_embedding(r, fr))
    df_mobile[VALID_EMBED] = df_mobile[EMBEDING].apply(lambda r: r.shape[0] > 1)
    df_mobile_invalidembd = df_mobile[df_mobile[VALID_EMBED] == False]
    df_mobile = df_mobile[df_mobile[VALID_EMBED] == True]
    lg.debug("valid mobile records-%d Invalid-%d"%(len(df_mobile), len(df_mobile_invalidembd)))
    lg.debug("Invalid profile pictures employee_ids: %s"%str(df_mobile_invalidembd["emp_id"]))

    df_mobile[CREATED_DATE] = df_mobile[CREATED_DATE].apply(lambda r: get_timeobj(r[:19]))
    df_mobile.sort_values(by=[CREATED_DATE]) # df_mobile.sort_values(CREATED_DATE)
    
    return df_mobile

def time_matching(df_mobile, df_system):
    # Todo
    mobile_date = df_mobile[CREATED_DATE].to_list()
    system_date = df_system[CREATED_DATE].to_list()
    min_time = min(mobile_date)
    max_time = max(mobile_date) 
    max_time = max_time + timedelta(minutes = TIME_OFFSET)

    df_system[VALID_TIME] = df_system[CREATED_DATE].apply(lambda r: r > min_time and r < max_time)
    df_system_othertimes = df_system[df_system[VALID_TIME] == False]
    df_system = df_system[df_system[VALID_TIME] == True]
    
    lg.debug("time_range %s to %s offset-%d in_rnage- %d out_range-%d"%
            (str(min_time), str(max_time), TIME_OFFSET, len(df_system), len(df_system_othertimes)))

    return df_mobile, df_system

def registration_matching(data, fr):
    try:
        lg.debug("Registration Process Parameters: %s"%config.recognition.registration)
        df_mobile = pd.DataFrame(data["mobiledata"])
        df_system = pd.DataFrame(data["systemdata"])
        

        df_mobile = df_mobile_process(df_mobile, fr)
        df_system = df_system_process(df_system)
        lg.debug("Valid mobile records-%d system_records (clusters)-%d(%d)"%
                (len(df_mobile), len(df_system), len(Counter(df_system["cluster_id"]))))

        df_mobile, df_system = time_matching(df_mobile, df_system)

        match_list = matching(df_mobile, df_system)

    except Exception as e:
        lg.debug(e)
        lg.debug("Falied to complete the Matching, Look above to get the error")
        traceback.print_exc()
        match_list = []
   
    return match_list

def get_response_json(match_list):
    response = []
    lg.debug("matching list length- %d"%len(match_list))
    if len(match_list) == 0:
        lg.debug("Response:%s"%str(response))
        return response

    for item in match_list:
        if item["trackdataId"] is not None:
            temp_dict = {}
            temp_dict["employeeId"] = item["employeeId"]
            temp_dict["trackdataId"] = item["trackdataId"]
            temp_dict["clusterId"] = item["cluster_id"]
            temp_dict["status"] = item["status"]
            response.append(temp_dict)
    
    lg.debug("Response:%s"%str(response))
    return response

class RegistrationService(KaiProcess):

    def __init__(self):
        super().__init__()

    def unique_name(self) -> str:
        return "RegistrationService"

    def monitored_status_params(self) -> dict:
        return {}

    def run(self):
        super().run()
        self.fr = FaceRecogniser(appconfig)
        app = Flask(__name__)

        @app.route('/matching', methods=["POST"])
        # @app.route('/matching')
        def reg_matching():
            lg.debug("Matching Process Initiated")
            data = request.json
            # lg.debug(data)
            # data = load_data(path)

            lg.debug("Registration Matching Initiated")
            match_list = registration_matching(data, self.fr)

            lg.debug("Response Generation Initiated")
            response_json = get_response_json(match_list)

            # print(response_json)
            return Response(json.dumps(response_json), headers={'Content-type': "application/json"})


        self.run_in_and_wait_for_subthread(app.run, host="0.0.0.0", debug=False)


if __name__ == "__main__":
    from vap.modules import logging

    lg = logging.get_root_logger(log_name="ayeAccess.FR")
    ms = RegistrationService()
    run_and_wait(ms)

    if 0:
        app_config = AppConfig("/media/ahamad/work/projects/tvs/face-rec/face-rec/src/config.json")
        fr = FaceRecogniser(app_config)
        path = "/media/ahamad/work/projects/tvs/face-rec/face-data/matching-smapleip/Input.txt"
        data = load_data(path)
        match_list = registration_matching(data, fr)
        response_json = get_response_json(match_list)

        # print(match_list)
        # print(response_json)
        # view_faceprofile(df_mobile)
        # print(df_system.columns)

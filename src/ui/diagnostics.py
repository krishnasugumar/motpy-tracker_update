import sys
sys.path.append("/data/work/face-rec/face-rec/src/")
import os, json

import psycopg2
from psycopg2.extras import RealDictCursor

import flask
from flask import Flask, render_template, render_template_string, url_for, request, jsonify, Response 

DB_NAME="testdb2"
DB_USER="kamerai"
DB_PWD="kamerai"
DB_HOST="127.0.0.1"
DB_PORT="5432"

app = Flask(__name__)

SAVED_QUERIES = {}

class UI(object):

    def __init__(self):
        self.hi = "It Works!"        
        self.pages = {
            "header": """<!DOCTYPE html>
<html>
<header>
<style>
.cards {
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(150px, 1fr)); 
    grid-auto-rows: auto;
    grid-gap: 1rem; 
}

.cards-small {
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(100px, 1fr)); 
    grid-auto-rows: auto;
    grid-gap: 1rem; 
}
 
header, main, footer {
    padding-right: 40%;
}

@media only screen and (max-width : 992px) {
    header, body, footer {
    padding-right: 0;
    }
}

  body {
    display: flex;
    min-height: 100vh;
    flex-direction: column;
  }

  main {
    flex: 1 0 auto;
  }

      
</style>
<!-- Include Required Prerequisites -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<!-- Compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
<!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<style>
#clustering-slide-out.sidenav {
    width:40% !important
}
#training-slide-out.sidenav {
    width:50% !important
}
</style>

</header>
<main>
    <ul id="clustering-slide-out" class="sidenav sidenav-fixed">
      <div id="clusterDetails">
      </div>
    </ul>

    <ul id="training-slide-out" class="sidenav">    
    <div id="trainingDetails">
    </div>
  </ul>

          
       
""",
            "home": """
  <div class="row">
    <div class="col s12">
      <div class="row">

        <div class="input-field col s2">
          <i class="material-icons prefix">date_range</i>
          <input type="text" id="startdate" class="datepicker" value="12/20/2019">
          <label for="startdate">Start Date</label>
        </div>

        <div class="input-field col s2">
            <i class="material-icons prefix">access_time</i>
            <input type="text" id="startime" class="timepicker" value="06:50">
            <label for="starttime">Start Time</label>
        </div>

        <div class="input-field col s2">
            <i class="material-icons prefix">access_time</i>
            <input type="text" id="endtime" class="timepicker" value="09:00">
            <label for="endtime">End Time</label>
        </div>
        <div class="input-field col s2">
            <select id='status'>
                <option value="All">All</option>
                <option value="Recognized">Recognized</option>
                <option value="Un Recognized">UnRecognized</option>
            </select>
            <label>Status</label>
        </div>
        <div class="input-field col s2">
            <select id='camId'>
                <option value="All">All</option>
                <option value="0">0 Entry</option>
                <option value="1">1 Exit</option>
                <option value="ppk-0">ppk-0 Entry</option>
                <option value="ppk-1">ppk-1 Exit</option>                
            </select>
            <label>CamId</label>
        </div>
        <div class="input-field col s2">
          <a id='load' class="waves-effect waves-light btn">Load</a>
        </div>

        <div class="input-field col s2">
            <input type="text" id="recordCount" class="active" value="">
            <label for="recordCount">Record Count</label>
        </div>

      </div>
    </div>
  </div>
<div class="row">
<div class="col s12">
<!-- Photo Grid -->
<div id="photo-grid" class="cards"> 
</div>
</div>
</div>

<script>
$('#load').click(function(){
  var startdate = $('#startdate')[0].value;
  var starttime = $('#startime')[0].value;
  var enddate = $('#startdate')[0].value;
  var endtime = $('#endtime')[0].value;
  var daterange = startdate + ' @ ' + starttime + " --- " + enddate + ' @ ' + endtime
  console.log(daterange)

  var status = $('#status')[0].value;
  var camId = $('#camId')[0].value;
  payload = {'date-range': daterange, 'status': status, 'camId': camId};

  $("#photo-grid").load("/grid", payload, function(response,status,xhr){
    console.log(status);
    });
});

function loadClusterDetails(clusterId){
    console.log(clusterId + " clicked!");
    $('#clusterDetails').load("/cluster-details",{"clusterId":clusterId}, function(respopnse,status,xhr){
        console.log(status);
    })
}

function loadTrainingDetails(empId){
    console.log(empId + " clicked!");
    $('#trainingDetails').load("/training-details",{"empId":empId}, function(response, status, xhr){
        console.log(status);
    })
}

$("#photo-grid").load("/grid",{"date-range":"", "status":"All", "camId":"All"}, function(response,status,xhr){
    console.log(status);
    //console.log(response);
});


</script>

            """,
        "custom": """
        <div class="row" style="margin-bottom:0px">
            <form class="col s12" >
              <div class="row" style="margin-bottom:0px">
                <div class="input-field col s6">
                  <input placeholder="select ref_image from track_data td" id="query_text" type="text" class="validate" value="select ref_image from track_data td">
                  <label for="first_name">Query (select ref_image to see images)</label>
                </div>
                <div class="input-field col s1">
                  <input placeholder="limit 100" id="query_text_suffix" type="text" class="validate" value="limit 100;">
                  <label for="first_name">limit</label>
                </div>
                  <div  class="input-field col s2">
                    <select id="saved-qry">
                      {% for savedqry in savedqrys %}
                      <option value="{{ savedqry[0] }}">{{ savedqry[1] }}</option>
                      {% endfor %}                      
                    </select>
                    <label>Saved Queries (saved-qry.json)</label>
                  </div>                                        
                <div class="switch col s2">
                    <label>
                      Grid
                      <input type="checkbox" id="qry-result-table">
                      <span class="lever"></span>
                      Table
                    </label>
                </div>
                <div class="input-field col s1">
                    <a id="query" class="waves-effect waves-light btn">Query</a>
                </div>                                        
            </form>
        </div>
        <div class="row">
            <div id="qry-results" class="col s12"> 
            </div>
        </div>
        
        
        <script>
        $('#clustering-slide-out').removeClass('sidenav-fixed');
        $('#clustering-slide-out').sidenav(options={'edge':'right'});
        $('main').css('padding-right','0px');
        $('#query').click(function () {
            val = $('#query_text')[0].value + ' ' + $('#query_text_suffix')[0].value;
            tabulate = $('#qry-result-table')[0].checked;            
            execQuery(val, tabulate);
        });
        $('#saved-qry').change(function () {
            $("#query_text").val( $('#saved-qry')[0].value );
        });
        
        function execQuery(query, tabulate){
            console.log(query);
            $('#qry-results').load("/exec-query",{"query": query, "tabulate": tabulate}, function(response,status,xhr){
                console.log(status);
            });
        }
        </script>
        """,
        "query-results": """
        <div>{{ query }}</div>
        {% if tabulate == "true" %}
        <table class="striped">
            <thead>
              <tr>
              {%for colname in heads%}            
                  <th>{{ colname }}</th>                  
              {%endfor%} 
              </tr>
            </thead>
            
            <tbody>
            {%for r in range(0,rows)%}
              <tr>
                {%for colname in heads%}
                {% if colname == 'ref_image' %}
                    <td><img style="height:100px;width:100px;" src="data:image/jpeg;base64,  {{ table[r][colname] }}" /></td>
                {% else %}
                    <td>{{ table[r][colname] }}</td>
                {% endif %}
                {%endfor%}
              </tr>
            {%endfor%}
            </tbody>
        </table>
        {% else %}
        <div class="cards">
        {%for r in range(0,rows)%}
            <div class="card">
              <div class="card-image">
                <img src="data:image/jpg;base64, {{ table[r]['ref_image'] }}" style="height:200px" >
              </div>
              <div class="card-content">
                {%for colname in heads%}
                {%if colname != 'ref_image'%}
                <p>{{ colname }} : {{ table[r][colname] }}</p>
                {%endif%}
                {%endfor%}         
              </div>
            </div> 
        {% endfor %}
        </div>
        
        {% endif %}
        
        """,
        "footer": """
<script>        
$(document).ready(function(){
$('#clustering-slide-out').sidenav(options={'edge':'right'});
$('#training-slide-out').sidenav(options={'edge':'left'});

$('.datepicker').datepicker(options={'format':'mm/dd/yyyy', 'defaultDate':'now', 'setDefaultDate':true });
$('#startime').timepicker(options={'twelveHour':false,'defaultTime':'06:50'});
$('#endtime').timepicker(options={'twelveHour':false,'defaultTime':'09:00'});
$('select').formSelect();
});
</script>
</main>

</html>
""",
        "photo-grid": """{%for i in range(0, len)%}
<div class="card">
  <div class="card-image">
    <img src="data:image/jpg;base64, {{ images[i] }}" style="height:200px" >
  </div>
  <div class="card-content">
    <a href='#' onclick="loadClusterDetails('{{ cluserIds[i] }}'); return false;"><b>{{ names[i] }}</b></a>
    <p>{{ dates[i] }}</p>
    <p>Images:{{ imgCounts[i] }}</p>
    <p>CamId:{{ camIds[i] }}</p>
    <p>EmpType: {{ empTypes[i] }}</p>

  </div>
</div> 
     {%endfor%} 

   <script>
    $('#recordCount').val('{{len}}');
    M.updateTextFields();
   </script>
  """,

        "cluster-details": """
    <li><a>Cluster Id: {{clusterId}}</a></li>
    <li><a>Employee Id: {{empId}}</a></lil>
    <li><a>Full Name: {{fullName}}</a></lil>
    <li><a>Employee Type: {{empType}}</a></lil>
    <li><a>Updated by: {{updatedBy}}</a></lil>
    <li><a>Images Count: {{imagesCount}}</a></lil>     
    {% if fullName is not none %}    
    <li><a href="#" onclick="loadTrainingDetails('{{ empId }}');" data-target="training-slide-out" class="sidenav-trigger">Show Training Images</a></li>
    {%endif %}
    <li><a class="subheader">Cluster Images</a></li>
    <div id="cluster-photo-grid" class="cards"> 
    {%for i in range(0, len)%}
    <div class="card">
        <div class="card-image">
            <img src="data:image/jpg;base64, {{ images[i] }}" style="height:150px" >
        </div>
        <div class="card-content">
            <p>{{ dates[i] }}</p>
            <p>TrackNo: {{ trackNos[i] }}</p>
            <p>CamId: {{ camIds[i] }}</p>
        </div>
        </div> 
    {%endfor%} 

    </div>
""",
        "training-details":"""
    <li><a>Employee Id: {{empId}}</a></lil>
    <li><a>Verification Id: {{verifyId}}</a></lil>
    <li><a>ValidImagesCount: {{imagesCount}}</a></lil>     
    <li><a class="subheader">Valid Training Images</a></li>
    <div id="training-photo-grid" class="cards"> 
    {%for i in range(0, len)%}
    <div class="card">
        <div class="card-image">
            <img src="data:image/jpg;base64, {{ images[i] }}" style="height:150px" >
        </div>
        <div class="card-content">
            <p>Created:{{ createdDates[i] }}</p>
            <p>Updated:{{ updatedDates[i] }} By {{ updatedBys[i] }}</p>
            <p>TrackNo:{{ trackNos[i] }}</p>
        </div>
        </div> 
    {%endfor%} 
"""        
        }

    def render_fullbody(self, page, **kwargs):
        return  self.pages["header"] + render_template_string(self.pages[page], **kwargs) + self.pages["footer"]
        
    def render(self, page, **kwargs):
        return  render_template_string(self.pages[page], **kwargs)

    



class DBHandler(object):

    def __init__(self):
        self.con = psycopg2.connect(database=DB_NAME, user=DB_USER, password=DB_PWD, host=DB_HOST, port=DB_PORT)
        print("Database opened successfully")

    def querydb(self, query, asdict=False, fetch=True):
        if asdict:
            cur = self.con.cursor(cursor_factory=RealDictCursor)
        else:
            cur = self.con.cursor()

        cur.execute(query)
        if fetch:
            res = cur.fetchall()
        else:
            res = None
        if asdict:
            return [i[0] for i in cur.description], res
        return res

    def commit(self, query):
        # print(query)
        # return None

        cur = self.con.cursor()
        cur.execute(query)
        return self.con.commit()

    def fetch_from(self, table, columns):
        query = "select %s from %s"%(",".join(columns), table)
        return self.querydb(query)

    def fetch_unique_from(self, table, column):
        query = "select distinct(%s) from %s"%(column, table)
        return self.querydb(query)

    def insert_into(self, table, headers, rows):
        rowsq =   ",\n".join(["(%s)"%(",".join(['\'%s\''%str(col) for col in row])) for row in rows]  )
        #rowsq = ",\n".join(["(%s"%(",".join(col)) for row in rows for col in row ])
        query = "insert into {table} ({headers}) values {rows}".format(
            table=table,
            headers="{headers}".format(headers=",".join(headers)),
            rows=rowsq
        )
        print(self.commit(query))

    def insert_single_into(self, table, headers, row):
        query = "insert into {table} ({headers}) values ({row})".format(
            table=table,
            headers="{h}".format(h=",".join(headers)),
            row=row
        )
        self.commit(query)

    def insert_multi_into(self, table, headers, rowsstr):
        query = "insert into {table} ({headers}) values {rowsstr}".format(
            table=table,
            headers="{h}".format(h=",".join(headers)),
            rowsstr=rowsstr
        )
        self.commit(query)

    def insert_obj(self, table, obj):
        self.insert_single_into(table, obj.headers(), str(obj))

    def insert_multi_obj(self, table, multiobj):
        self.insert_multi_into(table, multiobj.headers(), str(multiobj))


    def get_todays_clusters(self):
        query = """SELECT t.ref_image, t.created_date, t.cluster_id, t.status, v.emp_id, ei.full_name, cc.img_count, t.camera_id  FROM (
  SELECT DISTINCT ON (td.cluster_id) *
  FROM track_data td 
  WHERE td.created_date >= now() - interval '3 day'
  ORDER BY td.cluster_id, td.created_date DESC
) t
inner join employee_verification v on t .verify_id = v.verify_id
inner join employee_info ei on v.emp_id = ei.emp_id
inner join (select cluster_id, count(cluster_id) as img_count from track_data group by cluster_id) cc on cc.cluster_id = t.cluster_id 
ORDER BY t.created_date DESC"""
        return self.querydb(query)

    def get_clusters(self, start, end, status, camid):
        if status == 'All':
            status_str = ''
        else:
            status_str = " and status = '%s' "%(status)

        if camid == 'All':
            camid_str = ''
        else:
            camid_str = " and camera_id = '%s' "%(camid)

        query = """SELECT t.ref_image, t.created_date, t.cluster_id, t.status, v.emp_id, ei.full_name, cc.img_count, t.camera_id, ei."type" FROM (
  SELECT DISTINCT ON (td.cluster_id) *
  FROM track_data td 
  WHERE td.created_date >= '{start}' and td.created_date <= '{end}' {status_str} {camid_str}
  ORDER BY td.cluster_id, td.created_date DESC
) t
left outer join employee_verification v on t .verify_id = v.verify_id
left outer join employee_info ei on v.emp_id = ei.emp_id
inner join (select cluster_id, count(cluster_id) as img_count from track_data group by cluster_id) cc on cc.cluster_id = t.cluster_id 
ORDER BY t.created_date DESC""".format(start=start, end=end, status_str=status_str, camid_str=camid_str)
        return self.querydb(query)

    def get_cluster_details(self, cluster_id):
        query = """select td.ref_image, td.ref_track_no, td.created_date, td.camera_id from track_data td where cluster_id = '{cluster_id}'
        """.format(cluster_id=cluster_id)
        return self.querydb(query)

    def get_recognized_cluster_details(self, cluster_id):
        query = """select distinct on (ei.emp_id) ei.emp_id, ei.full_name, ei."type", t.updated_by from (select * from track_data td where cluster_id = '{cluster_id}') t 
inner join employee_verification v on t.verify_id = v.verify_id  
inner join employee_info ei on v.emp_id = ei.emp_id""".format(cluster_id=cluster_id)
        return self.querydb(query)

    def get_training_details(self, emp_id):
        query = """select td.ref_image, td.ref_track_no, td.created_date, td.updated_date, td.updated_by, t.verify_id 
from (select verify_id from employee_verification ev where emp_id = '{emp_id}') t
join track_data td on td.verify_id = t.verify_id where td.embeded != ''  and updated_by = 'Admin';
""".format(emp_id=emp_id)
        return self.querydb(query)

ui = UI()
db = DBHandler()

@app.route('/old')
def home():   
    return ui.render_fullbody("home")

@app.route('/')
def custom():
    return ui.render_fullbody("custom", savedqrys=[(v,k) for k,v in SAVED_QUERIES.items()])

@app.route('/exec-query', methods=['POST'])
def exec_query():
    query = request.form['query']
    tabulate = request.form['tabulate']
    try:
        colnames, table = db.querydb(query, asdict=True)
        return ui.render("query-results", tabulate=tabulate, query=query, heads=colnames, table=table, rows=len(table))
    except:
        db.querydb("rollback;", fetch=False)
        return "SQL error"

@app.route('/cluster-details', methods=['POST'])
def cluster_details():
    cluster_id = request.form['clusterId']
    print(cluster_id)
    data = db.get_cluster_details(cluster_id)
    images = [d[0] for d in data ]
    trackNos = [d[1] for d in data]
    dates = [d[2] for d in data]
    camIds = [d[3] for d in data]
    imagesCount = len(images)
    print(len(data))

    recognized_data = db.get_recognized_cluster_details(cluster_id)
    if len(recognized_data) > 0:
        empId, fullName, empType, updatedBy = recognized_data[0]
    else:
        empId, fullName, empType, updatedBy = None, None, None, None  

    return ui.render("cluster-details",clusterId=cluster_id, len=len(data), 
    imagesCount=imagesCount, dates=dates, trackNos=trackNos, images=images,
    empId=empId, fullName=fullName, empType=empType, updatedBy=updatedBy, camIds=camIds
    )

@app.route('/training-details', methods=['POST'])
def training_details():
    emp_id = request.form['empId']
    print(emp_id)
    data = db.get_training_details(emp_id)

    images = [d[0] for d in data]
    trackNos = [d[1] for d in data]
    createdDates = [d[2] for d in data]
    updatedDates = [d[3] for d in data]
    updatedBys = [d[4] for d in data]
    verifyId = data[0][5]

    return ui.render('training-details',empId=emp_id,verifyId=verifyId,len=len(data),images=images,imagesCount=len(images),
    trackNos=trackNos,createdDates=createdDates,updatedDates=updatedDates,updatedBys=updatedBys)

@app.route('/grid', methods=['POST'])
def grid():
    daterange = request.form['date-range']
    status = request.form['status']
    camid = request.form['camId']
    print(daterange)
    try:
        start, end = daterange.split(' --- ')
        data = db.get_clusters(start, end, status, camid)
    except:
        data = db.get_todays_clusters()
    images = [d[0] for d in data]
    names = [d[5] for d in data]
    cluserIds = [d[2] for d in data]
    dates = [d[1] for d in data]
    img_counts = [d[6] for d in data]
    camIds = [d[7] for d in data]
    empTypes = [d[8] for d in data]

    return ui.render("photo-grid", images=images, names=names, cluserIds=cluserIds,
     dates=dates, imgCounts=img_counts, camIds=camIds, len=len(data),
     empTypes=empTypes
     )


def main():
    global SAVED_QUERIES
    if os.path.exists('saved-qry.json'):
        with open('saved-qry.json') as sq:
            SAVED_QUERIES = json.load(sq)

    app.run("0.0.0.0", port=5001, debug=True)

if __name__ == "__main__":
    main()

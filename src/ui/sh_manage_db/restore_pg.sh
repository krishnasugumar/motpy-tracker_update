#!/bin/bash

source ../vars.sh

. ../start_postgres.sh

BACKUP_NAME=backup20end.dbdump
#DB_NAME=$DB_NAME  //to use DB_NAME defined in vars.sh
DB_NAME=testdb2

sleep 2

echo "### Starting PostgreSQL Backup ###"

sudo -u $( logname ) /usr/lib/postgresql/10/bin/createdb $DB_NAME
sudo -u $( logname ) /usr/lib/postgresql/10/bin/psql $DB_NAME < $BACKUP_NAME

echo "### $BACKUP_NAME restored to Database $DB_NAME ###"
echo "### Advice: To delete use command \"dropdb $DB_NAME\" to delete this database ###"

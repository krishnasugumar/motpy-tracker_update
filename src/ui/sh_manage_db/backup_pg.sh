#!/bin/bash

source ../vars.sh

. ../start_postgres.sh

BACKUP_NAME=backup.dbdump
DB_NAME=$DB_NAME

sleep 2

echo "### Starting PostgreSQL Backup ###"

sudo -u $( logname ) /usr/lib/postgresql/10/bin/pg_dump $DB_NAME > $BACKUP_NAME

echo "### Saved to $BACKUP_NAME ###"

#!/bin/bash

source vars.sh

. start_postgres.sh

echo "### Setting Env Variables ###"
export CLUSTER_URL=http://localhost:5000/cluster
export CRON_EXPRESSION='0 0/5 * ? * *'
export db_password=$DB_PASSWORD
export db_url=jdbc:postgresql://localhost:5432/$DB_NAME
export db_username=$DB_USER
export LOG_PATH=$SPRING_LOG_PATH
export LIVE_STREAM_URL=$LIVE_STREAM_URLS
export AUTH_USER_NAME=kamerai #admin
export AUTH_USER_PASSWORD=1recode@PPK #admin
export AUTH_URL_1=http://localhost:8080/auth/realms/master/protocol/openid-connect/token
export AUTH_URL_2=http://localhost:8080/auth/realms/master/protocol/openid-connect/token/introspect
export MATCH_ALG_URL=http://localhost:5000/matching
export MATCH_CRON_EXPRESSION='0 0/14 * 1/1 * ?'
export MATCH_UN_RECOGNIZED_DURATION=1
export CLUSTER_RECOGNIZED_DURATION=6
if [ "$FLAG_DEBUG" = true ] ; then
    echo "### Running on Debug Machine ###"
    # export TVS_ATTEND_DATA_URL=http://testing.tvslsl.in/lnt/api/RecodeAttendData
    # export TVS_AUTHENTICATION_URL=http://testing.tvslsl.in/lnt/token
else
    echo "### Running on Production Machine ###"
    export TVS_ATTEND_DATA_URL=https://tvslmsgboard.tvslsl.in/lnt/api/RecodeAttendData
    export TVS_AUTHENTICATION_URL=https://tvslmsgboard.tvslsl.in/lnt/token
fi

echo "### Launching Registration UI ###"
reg_server_port=9091 java -jar Registration-0.0.1-SNAPSHOT.jar &

echo "### Launching Springboot Backend+UI ###"
server_port=8082 java -jar kentAI-0.0.1-SNAPSHOT.jar 


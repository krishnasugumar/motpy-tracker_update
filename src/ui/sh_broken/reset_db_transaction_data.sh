source ../vars.sh
#clear any roll dbs

. ../start_postgres.sh

sleep 2

echo "### Setting Permissions PostgreSQL ###"

sudo -u $( logname ) /usr/lib/postgresql/10/bin/psql $DB_NAME < "truncate_all.sql"

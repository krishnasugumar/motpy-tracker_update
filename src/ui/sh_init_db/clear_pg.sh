#!/bin/bash

sudo systemctl stop postgresql
sudo apt --purge remove -y postgresql postgresql-10 postgresql-client-10 postgresql-client-common postgresql-common postgresql-contrib
sudo rm -rf /var/lib/postgresql/
sudo rm -rf /var/log/postgresql/
sudo rm -rf /etc/postgresql/

sudo deluser postgres
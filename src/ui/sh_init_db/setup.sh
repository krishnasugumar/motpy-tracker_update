#!/bin/bash
set -x
echo "##### Checking sudo #####"

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

echo "OK"

echo "##### Installing PostgreSQL #####"

#sudo apt update
sudo apt install -y postgresql postgresql-contrib

echo "##### Configuring Postgres #####"
source ../vars.sh
#sudo -u postgres bash -c "psql -c \"CREATE USER $DB_USER WITH PASSWORD '$DB_PASSWORD';\nCREATE DATABASE $DB_NAME; \""
sudo systemctl stop postgresql

sudo rm -rf $DB_PATH
mkdir -p $DB_PATH
#sudo chown postgres $DB_PATH  # doesn't work on NTFS partitions
#sudo chgrp postgres $DB_PATH
#sudo -u postgres /usr/lib/postgresql/10/bin/initdb -D $DB_PATH --auth-local peer --auth-host md5
#sudo -u postgres /usr/lib/postgresql/10/bin/pg_ctl -D $DB_PATH -l $DB_PG_LOG_PATH start

sudo -u $( logname ) /usr/lib/postgresql/10/bin/initdb -D $DB_PATH --auth-local peer --auth-host md5
sudo chown -R $( logname ):$( logname ) /var/run/postgresql/
sudo -u $( logname ) /usr/lib/postgresql/10/bin/pg_ctl -D $DB_PATH -l $DB_PG_LOG_PATH start
sleep 2
#sudo sed -i -e "/$TARGET_KEY =/ s#= .*#= \'$REPLACEMENT_VALUE\'#" $CONFIG_FILE
#sudo systemctl start postgresql
sudo -u $( logname ) createdb -p $DB_PORT
sudo -u $( logname ) createdb $DB_NAME -p $DB_PORT
sudo -u $( logname ) psql -p $DB_PORT -c "create role $DB_USER with login password '$DB_PASSWORD'"

sudo -u $( logname ) psql -p $DB_PORT -c "grant all privileges on database \"$DB_NAME\" to $DB_USER"

sudo systemctl start postgresql
#sudo -u postgres bash -c "createdb $DB_NAME"
#sudo -u postgres psql -c "create role $DB_USER with login password '$DB_PASSWORD'"
echo "##### Finished Setup #####"

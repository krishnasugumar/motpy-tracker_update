#!/bin/bash

source ../vars.sh

. ../start_postgres.sh

sudo -u $( logname ) createdb $DB_NAME -p $DB_PORT
sudo -u $( logname ) psql -p $DB_PORT -c "create role $DB_USER with login password '$DB_PASSWORD'"

sudo -u $( logname ) psql -p $DB_PORT -c "grant all privileges on database \"$DB_NAME\" to $DB_USER"

sudo systemctl start postgresql

/*delete from permission;
delete from roles;
delete from user_role_group_roles_groups;
delete from roles_group;
delete from user_role_group;
delete from users;
delete from default_permission;
delete from default_roles;
*/
DO $$ 
DECLARE
   counter bigint := 0;
BEGIN 
	counter = nextval('DEFAULT_ROLE_ID_SEQ');
	INSERT INTO DEFAULT_ROLES(ROLE_ID, ROLE_NAME, DESCRIPTION, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE) VALUES
	(counter, 'Employee', 'Employee', 'Administrative', current_date, 'Administrative', current_date);

	INSERT INTO DEFAULT_PERMISSION(PERMISSION_ID, ROLE_ID, PERMISSION_NAME, DESCRIPTION, READ_PERMISSION, WRITE_PERMISSION, DELETE_PERMISSION,  CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE)
	VALUES(
	nextval('DEFAULT_PERMISSION_ID_SEQ'), counter, 'Employee_Landing_Read', 'Employee Landing Read', true, false, false, 'Administrative', current_date, 'Administrative', current_date
	),(
	nextval('DEFAULT_PERMISSION_ID_SEQ'), counter, 'Employee_Edit_Read', 'Employee Edit Read', true, false, false, 'Administrative', current_date, 'Administrative', current_date
	),(
	nextval('DEFAULT_PERMISSION_ID_SEQ'), counter, 'Employee_Edit_Write', 'Employee Edit Write', false, true, false, 'Administrative', current_date, 'Administrative', current_date
	),(
	nextval('DEFAULT_PERMISSION_ID_SEQ'), counter, 'Employee_Tag_Write', 'Employee Tag Write', false, true, false, 'Administrative', current_date, 'Administrative', current_date
	),(
	nextval('DEFAULT_PERMISSION_ID_SEQ'), counter, 'Employee_Tag_Update_Write', 'Employee Tag Update Write', false, true, false, 'Administrative', current_date, 'Administrative', current_date
	),(
	nextval('DEFAULT_PERMISSION_ID_SEQ'), counter, 'Employee_Create_Write', 'Employee Create Write', false, true, false, 'Administrative', current_date, 'Administrative', current_date
	),(
	nextval('DEFAULT_PERMISSION_ID_SEQ'), counter, 'Employee_Delete_Delete', 'Employee Delete Delete', false, false, true, 'Administrative', current_date, 'Administrative',current_date
	);

	counter = 0;
	counter = nextval('DEFAULT_ROLE_ID_SEQ');
	INSERT INTO DEFAULT_ROLES(ROLE_ID, ROLE_NAME, DESCRIPTION, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE) VALUES
	(counter, 'Recording', 'Recording', 'Administrative', current_date, 'Administrative', current_date);

	INSERT INTO DEFAULT_PERMISSION(PERMISSION_ID, ROLE_ID, PERMISSION_NAME, DESCRIPTION, READ_PERMISSION, WRITE_PERMISSION, DELETE_PERMISSION,  CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE)
	VALUES(
	nextval('DEFAULT_PERMISSION_ID_SEQ'), counter, 'Recording_Landing_Read', 'Recording Landing Read', true, false, false, 'Administrative', current_date, 'Administrative', current_date
	);
	
	counter = 0;
	counter = nextval('DEFAULT_ROLE_ID_SEQ');
	INSERT INTO DEFAULT_ROLES(ROLE_ID, ROLE_NAME, DESCRIPTION, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE) VALUES
	(counter, 'Training Date', 'Training Date', 'Administrative', current_date, 'Administrative', current_date);

	INSERT INTO DEFAULT_PERMISSION(PERMISSION_ID, ROLE_ID, PERMISSION_NAME, DESCRIPTION, READ_PERMISSION, WRITE_PERMISSION, DELETE_PERMISSION,  CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE)
	VALUES(
	nextval('DEFAULT_PERMISSION_ID_SEQ'), counter, 'Training_Date_Read', 'Training Date Read', true, false, false, 'Administrative', current_date, 'Administrative', current_date
	);

	counter = 0;
	counter = nextval('DEFAULT_ROLE_ID_SEQ');
	INSERT INTO DEFAULT_ROLES(ROLE_ID, ROLE_NAME, DESCRIPTION, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE)
	VALUES(
	counter, 'User', 'User', 'Administrative', current_date, 'Administrative', current_date
	);

	INSERT INTO DEFAULT_PERMISSION(PERMISSION_ID, ROLE_ID, PERMISSION_NAME, DESCRIPTION, READ_PERMISSION, WRITE_PERMISSION, DELETE_PERMISSION,  CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE)
	VALUES(
	nextval('DEFAULT_PERMISSION_ID_SEQ'),counter, 'User_Create_Read', 'User Create Read', true, false, false, 'Administrative', current_date, 'Administrative', current_date
	),(
	nextval('DEFAULT_PERMISSION_ID_SEQ'),counter, 'User_Create_Write', 'User Create Write', false, true, false, 'Administrative', current_date, 'Administrative', current_date
	),(
	nextval('DEFAULT_PERMISSION_ID_SEQ'),counter, 'User_Edit_Read', 'User Edit Read', true, false, false, 'Administrative', current_date, 'Administrative', current_date
	),(
	nextval('DEFAULT_PERMISSION_ID_SEQ'),counter, 'User_Edit_Write', 'User Edit Write', false, true, false, 'Administrative', current_date, 'Administrative', current_date
	),(
	nextval('DEFAULT_PERMISSION_ID_SEQ'),counter, 'User_Landing_Read', 'User Landing Read', true, false, false, 'Administrative', current_date, 'Administrative', current_date
	),(
	nextval('DEFAULT_PERMISSION_ID_SEQ'),counter, 'User_Delete_Delete', 'User Delete Delete', false, false, true, 'Administrative', current_date, 'Administrative', current_date
	),(
	nextval('DEFAULT_PERMISSION_ID_SEQ'),counter, 'User_Status_Write', 'User Status Write', false, true, false, 'Administrative', current_date, 'Administrative', current_date
	),(
	nextval('DEFAULT_PERMISSION_ID_SEQ'),counter, 'Role_Group_Read', 'Role Group Read', true, false, false, 'Administrative', current_date, 'Administrative', current_date
	),(
	nextval('DEFAULT_PERMISSION_ID_SEQ'),counter, 'Role_Group_Write', 'Role Group Write', false, true, false, 'Administrative', current_date, 'Administrative', current_date
	);
END $$;

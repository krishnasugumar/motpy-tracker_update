#!/bin/bash

source vars.sh

echo "### Stopping any Postgres Services  ###"

sudo systemctl stop postgresql
sudo -u $( logname ) /usr/lib/postgresql/10/bin/pg_ctl -D $DB_PATH -l $DB_PG_LOG_PATH stop

sleep 2

echo "### Starting PostgreSQL ###"
sudo mkdir  -p /var/run/postgresql
sudo chown -R $( logname ):$( logname ) /var/run/postgresql
sudo chown -R $( logname ):$( logname ) /var/lib/postgresql
sudo -u $( logname ) /usr/lib/postgresql/10/bin/pg_ctl -D $DB_PATH -l $DB_PG_LOG_PATH start
#!/bin/bash

# postgres config

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )";
DEBUG=${DIR}/flag-debug-machine
PRODUCTION=${DIR}/flag-production-machine
if [ ! -f "$PRODUCTION" ]; then
## if debug machine ##
    echo "$PRODUCTION does not exist"
    FLAG_DEBUG=true
    SPRING_LOG_PATH=/data/work/face-rec/face-rec/src/ui/kameraAI_log
    DB_USER=kamerai
    DB_PASSWORD=kamerai
    DB_NAME=testdb2
    #DB_NAME=waycool
    DB_PATH=/data/work/face-rec/face-rec/data/db/
    DB_PG_LOG_PATH=/data/work/face-rec/face-rec/data/db/pgdb.log
    DB_PORT=5432
    LIVE_STREAM_URLS=http://217.7.233.140/cgi-bin/faststream.jpg?stream=full&fps=0,http://217.7.233.140/cgi-bin/faststream.jpg?stream=full&fps=0

else
## if production machine ##
    echo "$PRODUCTION exists"
    FLAG_DEBUG=false
    SPRING_LOG_PATH=/mnt/storage/fr/springlog
    DB_USER=kamerai
    DB_PASSWORD=kamerai
    DB_NAME=kameraidb
    DB_PATH=/mnt/storage/fr/db/
    DB_PG_LOG_PATH=/mnt/storage/fr/dblog
    DB_PORT=5432

fi

#SPRING_LOG_PATH=/mnt/storage/fr/springlog
#DB_USER=kamerai
#DB_PASSWORD=kamerai
#DB_NAME=kameraidb
#DB_PATH=/mnt/storage/fr/db
#DB_PG_LOG_PATH=/mnt/storage/fr/dblog
#DB_PORT=5432


# DB_NAME=kameraiDB
# DB_PATH=/data/work/face-rec/ui_builds/database/db
# DB_PG_LOG_PATH=/data/work/face-rec/ui_builds/database/db_log/pgdb.log
# DB_PORT=5432



#!/bin/bash

echo "Launching Backend and UI"
bash -c "cd ui/ && ./run.sh" &

echo "Launching Vision Service"
source ~/anaconda3/etc/profile.d/conda.sh
conda activate /data/work/face-rec/face-env
python app_mgr.py &

Face Recognition

Setting up Dev Env

We recommend Anaconda
* To recreate environment use this command:
`conda env create -f conda_env.yml`
* To save changes made to the environment use this command:
`conda env export -p /path/to/face-env > conda_env.yml`
